<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED429  -->
  <xsl:template match="ed:ED429">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>��������� �� ������� ������ ����� �� ������� ������������ ������ ������&#13;&#10;</xsl:text>
      <xsl:text>    � �������� ������ ��� ������ ������ ������������ ������ ������&#13;&#10;</xsl:text>
      <xsl:text>    ����� ���� ��������� �����������&#13;&#10;</xsl:text>
      <xsl:text>    ������ �� ����� ������� �� ������ �� �������&#13;&#10;</xsl:text>
      <xsl:text>    ������ �� ������� ������� (�� ������� ������)  /ED429/ &#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:AssetWithdrawal">
        <xsl:text>����� ���������                           : </xsl:text><xsl:value-of select="@AssetReqNum"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>���� � ����� ����������� ���������        : </xsl:text><xsl:value-of select="@AssetReqDateTime"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>��������������� ����� ����.�����������    : </xsl:text><xsl:value-of select="@OrgRegNum"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>����������� ��������� ������������ ��     : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="ed:OrgName"/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                          : ')"/>
        </xsl:call-template>
        <xsl:for-each select="ed:SecurityDepoPartitionsList">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>������� ������ ���� ��� ������ �����&#13;&#10;</xsl:text>
          <xsl:text>======================================&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:SecurityDepoPartitions">
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:value-of select="concat(position(),'.')"/>
            <xsl:text>��� �����������                         : </xsl:text><xsl:value-of select="@DepoCode"/>
            <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            <xsl:text>  � ����� ���� � �����������              : </xsl:text><xsl:value-of select="@DepoAccount"/>
            <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            <xsl:text>  � ������� ����� ����, �� ��������       :&#13;&#10;</xsl:text>
            <xsl:text>  ����������� �����������                 : </xsl:text><xsl:value-of select="@DepoPartitionFrom"/>
            <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            <xsl:text>  ������������ �������, �� ��������       :&#13;&#10;</xsl:text>
            <xsl:text>  ����������� �����������                 : </xsl:text>
            <xsl:call-template name="text_wrapper">
              <xsl:with-param name="text" select="@DepoPartitionFromName"/>
              <xsl:with-param name="width" select="50"/>
              <xsl:with-param name="before" select="string('                                          : ')"/>
            </xsl:call-template>
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:text>  � ������� ����� ����, � ������� ������� &#13;&#10;</xsl:text>
            <xsl:text>  ��������� �����������                   : </xsl:text><xsl:value-of select="@DepoPartitionTo"/>
            <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            <xsl:text>  ������������ �������, � ������� ������� :&#13;&#10;</xsl:text>
            <xsl:text>  ��������� �����������                   : </xsl:text>
            <xsl:call-template name="text_wrapper">
              <xsl:with-param name="text" select="@DepoPartitionToName"/>
              <xsl:with-param name="width" select="50"/>
              <xsl:with-param name="before" select="string('                                          : ')"/>
            </xsl:call-template>
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:text>  ������������ �����������                : </xsl:text>
            <xsl:call-template name="text_wrapper">
              <xsl:with-param name="text" select="ed:DepoName"/>
              <xsl:with-param name="width" select="50"/>
              <xsl:with-param name="before" select="string('                                          : ')"/>
            </xsl:call-template>
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:text>                            ������ ������&#13;&#10;</xsl:text>
            <xsl:text>  ---------------------------------------------------------------------&#13;&#10;</xsl:text>
            <xsl:text>    � �/�     ���������������             ISIN      ���-�� ������ �����&#13;&#10;</xsl:text>
            <xsl:text>           ��������������� �����                    �����.�������&#13;&#10;</xsl:text>
            <xsl:text>  ---------------------------------------------------------------------&#13;&#10;</xsl:text>
            <xsl:apply-templates select="ed:Security"/>
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:text>  ---------------------------------------------------------------------&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:for-each>

        <xsl:for-each select="ed:CreditNonmarketAssetList">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>������, ������ � �������&#13;&#10;</xsl:text>
          <xsl:text>=========================&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:CreditNonmarketAsset">
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:value-of select="concat(position(),'.')"/>
            <xsl:for-each select="@AssetCode">

              <xsl:text>���������� ����� ������&#13;&#10;</xsl:text>
              <xsl:text>  � ������� ����� ������                : </xsl:text><xsl:value-of select="."/>
              <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="@AssetNo">
              <xsl:text>  � ������ (����� ���������� ��������,&#13;&#10;</xsl:text>
              <xsl:text>  �������, ������ ������)               : </xsl:text>
              <xsl:call-template name="text_wrapper">
                <xsl:with-param name="text" select="."/>
                <xsl:with-param name="width" select="50"/>
                <xsl:with-param name="before" select="string('                               ')"/>
              </xsl:call-template>
              <xsl:text>&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="@AssetDate">
              <xsl:text>  ���� ������                           : </xsl:text>
              <xsl:call-template name="date"/>
              <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="@IsResident">
              <xsl:text>  �������� ���������� ��                : </xsl:text>
              <xsl:if test=". = 1">���</xsl:if>
              <xsl:if test=". = 2">��</xsl:if>
              <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="@INN">
              <xsl:text>  ��� �����������                       : </xsl:text><xsl:value-of select="."/>
              <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="@EGRUL">
              <xsl:text>  ���.����� ����������� � ����� (����)  : </xsl:text><xsl:value-of select="."/>
              <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="@TUHolder">
              <xsl:text>  ��� ��, � ������� �������� �����      : </xsl:text><xsl:value-of select="."/>
              <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="@PartitionTypeTo">
              <xsl:text>  ��� �������, � ������� ���������&#13;&#10;</xsl:text>
              <xsl:text>  �����������                           : </xsl:text>
              <xsl:if test=". = '01'">�������� ������</xsl:if>
              <xsl:if test=". = '02'">������ �����</xsl:if>
              <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="ed:IssuerName">
              <xsl:text>  ������������ �����������, �����������&#13;&#10;</xsl:text>
              <xsl:text>  � ��������� ������ (��� ���� �����&#13;&#10;</xsl:text>
              <xsl:text>  �������, ����� ������ � �������)      : </xsl:text>
              <xsl:call-template name="text_wrapper">
                <xsl:with-param name="text" select="."/>
                <xsl:with-param name="width" select="30"/>
                <xsl:with-param name="before" select="string('                                          ')"/>
              </xsl:call-template>
              <xsl:text>&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:for-each>

        <xsl:for-each select="ed:GuarantorReplacementList">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>������ �����������&#13;&#10;</xsl:text>
          <xsl:text>====================&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:GuarantorReplacement">
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:value-of select="concat(position(),'.')"/>
            <xsl:for-each select="@CreditNum">
              <xsl:text>����� ������� : </xsl:text><xsl:value-of select="."/>
              <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:text>  ��������������� ������ ��-����������� &#13;&#10;</xsl:text>
            <xsl:text>  =======================================&#13;&#10;</xsl:text>
            <xsl:for-each select="ed:IssuerName/@GuarantorRegNum">
              <xsl:value-of select="concat('    ',.,'&#13;&#10;')"/>
            </xsl:for-each>
            <xsl:text>&#13;&#10;</xsl:text>


            <xsl:text>  ���������� ��������������&#13;&#10;</xsl:text>
            <xsl:text>  ==========================&#13;&#10;</xsl:text>
            <xsl:for-each select="ed:NewGuarantee">
              <xsl:text>&#13;&#10;</xsl:text>
              <xsl:text>    ����� ���������� �� �����������&#13;&#10;</xsl:text>
              <xsl:text>    ����������� �������� ����� ������&#13;&#10;</xsl:text>
              <xsl:text>    ����������������                  : </xsl:text>
              <xsl:value-of select="@AgreementNumber"/>
              <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

              <xsl:text>    ����� �������� ��������������     : </xsl:text>
              <xsl:call-template name="text_wrapper">
                <xsl:with-param name="text" select="@NewGuaranteeNumber"/>
                <xsl:with-param name="width" select="30"/>
                <xsl:with-param name="before" select="string('                                        ')"/>
              </xsl:call-template>
              <xsl:text>&#13;&#10;</xsl:text>

              <xsl:for-each select="@NewGuaranteeDate">
                <xsl:text>    ���� �������� ��������������      : </xsl:text>
                <xsl:call-template name="date"/>
                <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
              </xsl:for-each>

              <xsl:for-each select="@NewGuarantorRegNum">
                <xsl:text>    ���.����� ��-����������           : </xsl:text><xsl:value-of select="."/>
                <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
              </xsl:for-each>

              <xsl:for-each select="@GuaranteeAmount">
                <xsl:text>    �����, ��������� � ��������&#13;&#10;</xsl:text>
                <xsl:text>    ��������������                    : </xsl:text>
                <xsl:call-template name="ffsum"/>
                <xsl:text>&#13;&#10;</xsl:text>
              </xsl:for-each>
              <xsl:text>&#13;&#10;</xsl:text>
              <xsl:text>    ========================&#13;&#10;</xsl:text>
            </xsl:for-each>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:for-each>
  </xsl:template>

  <!--  Security  -->
  <xsl:template match="ed:Security">
      <xsl:call-template name="LeftPad">
        <xsl:with-param name="input" select="position()"/>
        <xsl:with-param name="n" select="9"/>
      </xsl:call-template>
      <xsl:call-template name="Center">
        <xsl:with-param name="input" select="@SecurityRegNum"/>
        <xsl:with-param name="n" select="28"/>
      </xsl:call-template>
      <xsl:call-template name="Center">
        <xsl:with-param name="input" select="@SecurityCode"/>
        <xsl:with-param name="n" select="14"/>
      </xsl:call-template>
      <xsl:call-template name="Center">
        <xsl:with-param name="input" select="@SecurityAmount"/>
        <xsl:with-param name="n" select="24"/>
      </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>
</xsl:stylesheet>
