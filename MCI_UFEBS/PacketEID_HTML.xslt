<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������). -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="html" encoding="WINDOWS-1251"/>


  <!-- =============================================================================================================
      ����� �������������� ��
  ================================================================================================================== -->
  <xsl:template match="ed:PacketEID">
<!--    <h1 style="font-size:4mm; width:180mm; align:left;">����� �������������� �� /PacketEID/</h1>
    <p style="font-size:4mm;"><xsl:call-template name="PriznGr"/></p>
-->
    <xsl:if test="count(ed:ED113) + count(ed:ED114)>0">
      <xsl:call-template name="ED273"/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>

    <xsl:apply-templates select="*[starts-with(local-name(.),'ED') and local-name(.) != 'ED113' and local-name(.) != 'ED114']"/>

  </xsl:template>

</xsl:stylesheet>