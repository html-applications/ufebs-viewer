<?xml version='1.0' encoding="Windows-1251" ?>
<!--  ������������ ����� �������� �� ���������  -->
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
  xmlns:SInWs="Sum-In_Words.uri" version='1.0' exclude-result-prefixes='SInWs'>

 <xsl:output method='html'/>

 <xsl:variable name='Self' select='document("")'/>

 <xsl:template name='SumInWords'>
  <xsl:param name="rub" select="'0'"/>
  <xsl:param name="kop" select="'00'"/>
  <xsl:choose>
    <xsl:when test='$rub = 0'>
      <xsl:value-of select="'���� '"/>
     </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name='RecurseSumWords'>
       <xsl:with-param name='Value' select='$rub'/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>

  <xsl:variable name='lrub' select='substring($rub,string-length($rub),1)'/>
  <xsl:variable name='lkop' select='substring($kop,string-length($kop),1)'/>

  <xsl:choose>
    <xsl:when test="$lrub = 1">
      <xsl:value-of select="'����� '"/>
    </xsl:when>
    <xsl:when test="$lrub > 1 and $lrub &lt; 5">
      <xsl:value-of select="'����� '"/>
    </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="'������ '"/>
   </xsl:otherwise>
  </xsl:choose>


  <xsl:choose>
    <xsl:when test="$lkop=1">
      <xsl:value-of select="concat($kop,' �������')"/>
    </xsl:when>
    <xsl:when test="$lkop > 1 and $lkop &lt; 5">
      <xsl:value-of select="concat($kop,' �������')"/>
    </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="concat($kop,' ������')"/>
   </xsl:otherwise>
  </xsl:choose>

 </xsl:template>


 <xsl:template name='RecurseSumWords'>
  <xsl:param name="Value" select="''"/>

  <xsl:choose>
   <xsl:when test='($Value div 1000000) - (($Value mod 1000000) div 1000000) > 0'>

    <xsl:call-template name='DecodeInteger'>
     <xsl:with-param name='Value' select='($Value div 1000000) - (($Value mod 1000000) div 1000000)'/>
     <xsl:with-param name='controlbad' select='0'/>
    </xsl:call-template>

    <xsl:call-template name='GetWordType'>
     <xsl:with-param name='Value' select='($Value div 1000000) - (($Value mod 1000000) div 1000000)'/>
     <xsl:with-param name='tagName' select="'SInWs:Million'"/>
    </xsl:call-template>

    <xsl:call-template name='RecurseSumWords'>
     <xsl:with-param name='Value' select='$Value mod 1000000'/>
    </xsl:call-template>

   </xsl:when>

   <xsl:when test='($Value div 1000) - (($Value mod 1000) div 1000) > 0'>

    <xsl:call-template name='DecodeInteger'>
     <xsl:with-param name='Value' select='($Value div 1000) - (($Value mod 1000) div 1000)'/>
     <xsl:with-param name='controlbad' select='1'/>
    </xsl:call-template>

    <xsl:call-template name='GetWordType'>
     <xsl:with-param name='Value' select='($Value div 1000) - (($Value mod 1000) div 1000)'/>
     <xsl:with-param name='tagName' select="'SInWs:Thousand'"/>
    </xsl:call-template>

    <xsl:call-template name='RecurseSumWords'>
     <xsl:with-param name='Value' select='$Value mod 1000'/>
    </xsl:call-template>

   </xsl:when>

   <xsl:when test='$Value != 0'>

    <xsl:call-template name='DecodeInteger'>
     <xsl:with-param name='Value' select='$Value'/>
     <xsl:with-param name='controlbad' select='0'/>
    </xsl:call-template>

   </xsl:when>
   <xsl:otherwise/>
  </xsl:choose>

 </xsl:template>


 <xsl:template name='DecodeInteger'>
  <xsl:param name='Value' select="''"/>
  <xsl:param name='controlbad' select="''"/>
  <xsl:variable name='Value100' select='$Value mod 100'/>

  <xsl:if test='($Value div 100) - ($Value100 div 100) != 0'>
   <xsl:call-template name='GetWordData'>
    <xsl:with-param name='Pos' select='27 + (($Value div 100) - ($Value100 div 100))'/>
    <xsl:with-param name='tagName' select="'SInWs:Words'"/>
   </xsl:call-template>
  </xsl:if>

  <xsl:choose>
   <xsl:when test='$Value100 > 20'>
    <xsl:call-template name='GetWordData'>
     <xsl:with-param name='Pos' select='18 + (($Value100 div 10) - (($Value100 mod 10) div 10))'/>
     <xsl:with-param name='tagName' select="'SInWs:Words'"/>
    </xsl:call-template>
    <xsl:if test='$Value100 mod 10 != 0'>
     <xsl:call-template name='GetSmallWord'>
      <xsl:with-param name='Value' select='$Value100 mod 10'/>
      <xsl:with-param name='controlbad' select='$controlbad'/>
     </xsl:call-template>
    </xsl:if>
   </xsl:when>
   <xsl:when test='$Value100 != 0'>
    <xsl:call-template name='GetSmallWord'>
     <xsl:with-param name='Value' select='$Value100'/>
     <xsl:with-param name='controlbad' select='$controlbad'/>
    </xsl:call-template>
   </xsl:when>
  </xsl:choose>
 </xsl:template>

 <xsl:template name='GetSmallWord'>
  <xsl:param name='Value' select="''"/>
  <xsl:param name='controlbad' select="''"/>
  <xsl:choose>
   <xsl:when test='($controlbad = 1) and ($Value &lt; 3)'>
    <xsl:call-template name='GetWordData'>
     <xsl:with-param name='Pos' select='$Value'/>
     <xsl:with-param name='tagName' select="'SInWs:BadWord'"/>
    </xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
    <xsl:call-template name='GetWordData'>
     <xsl:with-param name='Pos' select='$Value'/>
     <xsl:with-param name='tagName' select="'SInWs:Words'"/>
    </xsl:call-template>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>

 <xsl:template name='GetWordType'>
  <xsl:param name='Value' select="''"/>
  <xsl:param name='tagName' select="''"/>
  <xsl:choose>
   <xsl:when test='($Value mod 100) > 20'>
    <xsl:call-template name='GetInnerType'>
     <xsl:with-param name='Value' select='($Value mod 100) mod 10'/>
     <xsl:with-param name='tagName' select='$tagName'/>
    </xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
    <xsl:call-template name='GetInnerType'>
     <xsl:with-param name='Value' select='$Value mod 100'/>
     <xsl:with-param name='tagName' select='$tagName'/>
    </xsl:call-template>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>

 <xsl:template name='GetInnerType'>
  <xsl:param name='Value' select="''"/>
  <xsl:param name='tagName' select="''"/>
  <xsl:choose>
   <xsl:when test='$Value = 1'>
    <xsl:call-template name='GetWordData'>
     <xsl:with-param name='Pos' select='1'/>
     <xsl:with-param name='tagName' select='$tagName'/>
    </xsl:call-template>
   </xsl:when>
   <xsl:when test='$Value > 1 and $Value &lt; 5'>
    <xsl:call-template name='GetWordData'>
     <xsl:with-param name='Pos' select='2'/>
     <xsl:with-param name='tagName' select='$tagName'/>
    </xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
    <xsl:call-template name='GetWordData'>
     <xsl:with-param name='Pos' select='3'/>
     <xsl:with-param name='tagName' select='$tagName'/>
    </xsl:call-template>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>

 <xsl:template name='GetWordData'>
  <xsl:param name='Pos' select="''"/>
  <xsl:param name='tagName' select="''"/>
  <xsl:for-each select='$Self//SInWs:*[name() = $tagName]'>
   <xsl:if test='position() = $Pos'>

    <xsl:value-of select="./@word"/>
    <xsl:text> </xsl:text>

   </xsl:if>
  </xsl:for-each>
 </xsl:template>

 <SInWs:Million word='�������'/>
 <SInWs:Million word='��������'/>
 <SInWs:Million word='���������'/>
 <SInWs:Thousand word='������'/>
 <SInWs:Thousand word='������'/>
 <SInWs:Thousand word='�����'/>
 <SInWs:BadWord word='����'/>
 <SInWs:BadWord word='���'/>
 <SInWs:Words word='����'/>
 <SInWs:Words word='���'/>
 <SInWs:Words word='���'/>
 <SInWs:Words word='������'/>
 <SInWs:Words word='����'/>
 <SInWs:Words word='�����'/>
 <SInWs:Words word='����'/>
 <SInWs:Words word='������'/>
 <SInWs:Words word='������'/>
 <SInWs:Words word='������'/>
 <SInWs:Words word='�����������'/>
 <SInWs:Words word='����������'/>
 <SInWs:Words word='����������'/>
 <SInWs:Words word='������������'/>
 <SInWs:Words word='����������'/>
 <SInWs:Words word='�����������'/>
 <SInWs:Words word='����������'/>
 <SInWs:Words word='������������'/>
 <SInWs:Words word='������������'/>
 <SInWs:Words word='��������'/>
 <SInWs:Words word='��������'/>
 <SInWs:Words word='�����'/>
 <SInWs:Words word='���������'/>
 <SInWs:Words word='����������'/>
 <SInWs:Words word='���������'/>
 <SInWs:Words word='�����������'/>
 <SInWs:Words word='���������'/>
 <SInWs:Words word='���'/>
 <SInWs:Words word='������'/>
 <SInWs:Words word='������'/>
 <SInWs:Words word='���������'/>
 <SInWs:Words word='�������'/>
 <SInWs:Words word='��������'/>
 <SInWs:Words word='�������'/>
 <SInWs:Words word='���������'/>
 <SInWs:Words word='���������'/>

</xsl:stylesheet>

