<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED574  -->
  <xsl:template match="ed:ED574">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>���������� ������������� ������� ��������&#13;&#10;</xsl:text>
      <xsl:text>    ���������� ��������� ����� ������ /ED574/&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="@DictionMode">
        <xsl:text>��� ����������� : </xsl:text> <xsl:value-of select="."/>
        <xsl:if test=". = 1"> (�� ����������)</xsl:if>
        <xsl:if test=". = 2"> (�� �������)</xsl:if>
        <xsl:if test=". = 3"> (����� ������������� )</xsl:if>
        <xsl:if test=". = 4">
          <xsl:text> (����������  � �����������, ����������� �� ������ ��&#13;&#10;</xsl:text>
          <xsl:text>                    ������������� ���� �/��� � �������������,&#13;&#10;</xsl:text>
          <xsl:text>                    ��� ������� ����� �� ������������� ��� ������������)&#13;&#10;</xsl:text>
        </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:if test="ed:InitialED/@EDNo != ''">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:apply-templates select="ed:InitialED"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:for-each select="ed:COSCustomerInfo">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��������� �������&#13;&#10;</xsl:text>
        <xsl:text>=================&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>  ���                  : </xsl:text> <xsl:value-of select="@UIS"/> <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>  ��� ���              : </xsl:text> <xsl:value-of select="@BIC"/> <xsl:text>&#13;&#10;</xsl:text>
        <xsl:for-each select="@SWIFTBIC">
          <xsl:text>  ��� SWIFT            : </xsl:text> <xsl:value-of select="."/> <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@IsFreeServices">
          <xsl:text>  ������� ������������ : </xsl:text><xsl:value-of select="."/>
          <xsl:if test=". = 1"> (������ ��������������� ��������� )</xsl:if>
          <xsl:if test=". = 2"> (������ ��������������� ������ )</xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@IsSuspensedExchange">
          <xsl:text>  ����� �� ����� ���   : </xsl:text><xsl:value-of select="."/>
          <xsl:if test=". = 1"> (��� �����������)</xsl:if>
          <xsl:if test=". = 2"> (��������������� ������ )</xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@IsDailyDispatchES">
          <xsl:text>  �� �� ������� �� ����: </xsl:text><xsl:value-of select="."/>
          <xsl:if test=". = 1"> (�� ����������)</xsl:if>
          <xsl:if test=". = 2"> (����������)</xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@ChangeType">
          <xsl:text>  ��� ���������        : </xsl:text><xsl:value-of select="."/>
          <xsl:if test=". = 1"> (���������� ������������)</xsl:if>
          <xsl:if test=". = 2"> (���������� ������������ �� ������)</xsl:if>
          <xsl:if test=". = 3">
            <xsl:text> (� ������������ ������� �������&#13;&#10;                           ��������������� ������ �� ����� ���)</xsl:text>
          </xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>  ������������ ������� : </xsl:text>
        <xsl:for-each select="ed:Name">
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="."/>
            <xsl:with-param name="width" select="60"/>
            <xsl:with-param name="before"
             select="string('                       : ')"/>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>  ���������� �� ������� � ������ ��&#13;&#10;</xsl:text>
        <xsl:text>  =================================&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:ESExchangeInfo">
          <xsl:text>    ��� ������  : </xsl:text> <xsl:value-of select="@ServicesCode"/>
          <xsl:if test="@ServicesCode = 1"><xsl:text> (���������� ��������� � ���� �� )</xsl:text></xsl:if>
          <xsl:if test="@ServicesCode = 2"><xsl:text> (��������� ������������ ������� )</xsl:text></xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:for-each select="@BeginDate">
            <xsl:text>    ���� ������ ������ �����������        : </xsl:text> <xsl:call-template name="date"/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:for-each select="@EndDate">
            <xsl:text>    ���� ���������� �� ������ ����������� : </xsl:text> <xsl:call-template name="date"/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:for-each select="@ESSize">
            <xsl:text>    ������������ ������ �� : </xsl:text> <xsl:value-of select="."/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:for-each select="@ARMNo">
            <xsl:text>    ����� ���   : </xsl:text> <xsl:value-of select="."/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:if test="count(ed:COSSenderInfo) != 0">
            <xsl:if test="@ServicesCode = 2">
              <xsl:text>    ����������� : </xsl:text>
              <xsl:for-each select="ed:COSSenderInfo/@UIS">
                <xsl:if test="position() !=1 and position() mod 5 = 1">
                  <xsl:text>&#13;&#10;</xsl:text>
                  <xsl:value-of select = "string('                  ')"/>
                </xsl:if>
                <xsl:value-of select="concat(.,string(' '))"/>
              </xsl:for-each>
              <xsl:text>&#13;&#10;</xsl:text>
            </xsl:if>
            <xsl:if test="@ServicesCode = 1">
              <xsl:text>    ����������� : </xsl:text>
              <xsl:for-each select="ed:COSSenderInfo">
                <xsl:text>��� : </xsl:text> <xsl:value-of select="@UIS"/>
                <xsl:text>  ��� SWIFT : </xsl:text>
                <xsl:choose>
                  <xsl:when test="@SWIFTBIC !=''">
                     <xsl:call-template name="Pad">
                       <xsl:with-param name="input" select="@SWIFTBIC"/>
                       <xsl:with-param name="n" select="11"/>
                     </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select = "string('           ')"/>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:text>  ��������� SWIFT : </xsl:text>
                <xsl:for-each select="ed:SWIFTTypeList/@SWIFTTypeNo">
                  <xsl:if test="position() !=1 and position() mod 5 = 1">
                    <xsl:text>&#13;&#10;</xsl:text>
                    <xsl:value-of select = "string('                                                                               ')"/>
                  </xsl:if>
                  <xsl:value-of select="concat(.,string(' '))"/>
                </xsl:for-each>
                <xsl:text>&#13;&#10;</xsl:text>
                <xsl:text>            </xsl:text>
              </xsl:for-each>
              <xsl:text>&#13;&#10;</xsl:text>
            </xsl:if>
          </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
  </xsl:template>


</xsl:stylesheet>