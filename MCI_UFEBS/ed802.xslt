<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������). -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED802 -->
  <xsl:template match="ed:ED802">
    <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
    <xsl:text>���������� � ����������� /ED802/&#13;&#10;&#13;&#10;</xsl:text>
    <xsl:call-template name="PriznGr"/>

    <xsl:for-each select="@InfoTypeCode">
      <xsl:call-template name="InfoTypeCode"/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:text>���� � ����� �������� ��      : </xsl:text><xsl:value-of select="@CreationDateTime"/>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:for-each select="@PoolInfoFlag">
      <xsl:call-template name="LongText">
        <xsl:with-param name="text_f"
          select="'���������� ��������������� �� �����,������������������� �� ���� �������� BIC � Account'"/>
        <xsl:with-param name="text_t"
          select="'���������� ��������������� �� ���� ������ ���� �����������'"/>
      </xsl:call-template>
    </xsl:for-each>


    <xsl:for-each select="ed:LPInfo">
      <xsl:text>���������� � ���� �����������&#13;&#10;</xsl:text>
      <xsl:text>=============================&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:call-template name="ED802Account"/>
    </xsl:for-each>

    <xsl:text>���������� � ����������� ������ ���������&#13;&#10;</xsl:text>
    <xsl:text>=========================================&#13;&#10;&#13;&#10;</xsl:text>
    <xsl:for-each select="ed:Account">
      <xsl:call-template name="ED802Account"/>

      <xsl:for-each select="ed:DebitQueuedPayments">
        <xsl:text>���������� � ����������� � ������� ���������&#13;&#10;</xsl:text>
        <xsl:text>������������� � �������� ������� �� �����&#13;&#10;</xsl:text>
        <xsl:text>----------------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:DebitQueuedGroup">
          <xsl:call-template name="QueuedGroup">
            <xsl:with-param name="DC" select="1"/>
            <xsl:with-param name="ByAccount" select="ed:ByCreditAccount"/>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:for-each>

      <xsl:for-each select="ed:CreditQueuedPayments">
        <xsl:text>���������� � ����������� �� ������������� ��������&#13;&#10;</xsl:text>
        <xsl:text>��������� ������������� � ���������� ������� �� ����&#13;&#10;</xsl:text>
        <xsl:text>-----------------------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:CreditQueuedGroup">
          <xsl:call-template name="QueuedGroup">
            <xsl:with-param name="DC" select="2"/>
            <xsl:with-param name="ByAccount" select="ed:ByDebitAccount"/>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

    </xsl:for-each>
    <xsl:if test="ed:EDRefID/@EDNo != ''">
        <xsl:apply-templates select="ed:EDRefID"/>
    </xsl:if>
  </xsl:template>

<!-- GroupCode -->
  <xsl:template name="GroupCode">
    <xsl:text>��� ������              : </xsl:text><xsl:value-of select="."/>
    <xsl:if test=".='INTR'"><xsl:text> - ������������� �������&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".='QWRH'"><xsl:text> - ������� ������������ ��� ���������� �� ��������� ����&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".='QWAP'"><xsl:text> - ������� ������������, ��������� ���������� �� ���������� ��������&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".='QSPC'"><xsl:text> - ������� �� ����������� � ���� ������������&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".='QWCH'"><xsl:text> - ������� ������������, ��������� ��������&#13;&#10;</xsl:text></xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>


<!-- LiquidityInfo -->

  <xsl:template name="LiquidityInfo">
    <xsl:text>���������� � �����������&#13;&#10;</xsl:text>
    <xsl:text>-------------------------&#13;&#10;&#13;&#10;</xsl:text>
    <xsl:for-each select="@Balance">
      <xsl:text>������� �� �����             : </xsl:text>
      <xsl:call-template name="ffsum"/><xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
    <xsl:for-each select="@NetAvailableLiquidity">
      <xsl:text>������ ��������� ����������� : </xsl:text>
      <xsl:call-template name="ffsum"/><xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>

<!-- ED802Account -->

  <xsl:template name="ED802Account">
    <xsl:text>��� �� ��� ��� : </xsl:text><xsl:value-of select="@BIC"/>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>���� ��������� : </xsl:text><xsl:value-of select="@Account"/>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:for-each select="@SpecialModeIndicator">
      <xsl:call-template name="LongText">
        <xsl:with-param name="text_t" select="'� ����� ������� ������� �� ����������� � ���� ������������'"/>
      </xsl:call-template>
    </xsl:for-each>

    <xsl:if test="count(ed:AccountModeChanges) != 0">
      <xsl:text>��������� � ������ ������ �����&#13;&#10;</xsl:text><xsl:value-of select="@GroupCode"/>
      <xsl:text>--------------------------------&#13;&#10;</xsl:text><xsl:value-of select="@GroupCode"/>
      <xsl:for-each select="ed:AccountModeChanges/@AccountChangeType">
        <xsl:text>��� ��������� : </xsl:text><xsl:value-of select="."/>
        <xsl:if test=".= 'ICLS'"><xsl:text> - ����� ��� ��� ����� ��� ����������</xsl:text></xsl:if>
        <xsl:if test=".= 'ARRS'"><xsl:text> - ����� (�����������) �� ���� ��� ����������</xsl:text></xsl:if>
        <xsl:if test=".= 'ARRM'"><xsl:text> - ����� (�����������) �� ���� ��� �������</xsl:text></xsl:if>
        <xsl:if test=".= 'ARRD'"><xsl:text> - ����� (�����������) �� ���� ��� ����</xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="count(ed:LimitInfo) !=0">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>�������� � ������������� �������&#13;&#10;</xsl:text>
      <xsl:text>---------------------------------&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:LimitInfo">
        <xsl:call-template name="LimitInfo"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>

    <xsl:for-each select="ed:LiquidityInfo">
      <xsl:call-template name="LiquidityInfo"/>
    </xsl:for-each>

    <xsl:for-each select="ed:ReservationInfo">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>���������� � ���������������&#13;&#10;</xsl:text>
      <xsl:text>------------------------------&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:for-each select="@NonCashReservedSum">
        <xsl:text>����� ����� ������.����������� �� �����, �� �����������&#13;&#10;</xsl:text>
        <xsl:text>��������� �� ������� �������� �������� �������          : </xsl:text>
        <xsl:call-template name="ffsum"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@CashReservedSum">
        <xsl:text>����� ����� ������.����������� �� ����� &#13;&#10;</xsl:text>
        <xsl:text>� ����� �� ������� �������� �������� �������            : </xsl:text>
        <xsl:call-template name="ffsum"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      </xsl:for-each>
    </xsl:for-each>

    <xsl:for-each select="ed:ArrestInfo">
      <xsl:for-each select="@TotalAmount">
        <xsl:text>����� ����� ������� �/��� ����������� �� �����          : </xsl:text>
        <xsl:call-template name="ffsum"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@ArrestedAmount">
        <xsl:text>����� ����� �������, ����������������� ��� ����������&#13;&#10;</xsl:text>
        <xsl:text>���������� �� ������ (������������) �� �����            : </xsl:text>
        <xsl:call-template name="ffsum"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@OutstandingAmount">
        <xsl:text>����� ����� ������������� ������� (�����������)&#13;&#10;</xsl:text>
        <xsl:text>�� �����, �� ����������� ���� ������������,&#13;&#10;</xsl:text>
        <xsl:text>����������� �� ���� ������������ �������                : </xsl:text>
        <xsl:call-template name="ffsum"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@IndeterminateAmountFlag">
        <xsl:call-template name="IndeterminateAmountFlag"/>
      </xsl:for-each>

      <xsl:for-each select="ed:ArrestDetailedInfo">
        <xsl:call-template name="ArrestDetailedInfo"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

    </xsl:for-each>
  </xsl:template>

<!-- QueuedGroup -->

  <xsl:template name="QueuedGroup">
    <xsl:param name="DC" select="1"/>
    <xsl:param name="ByAccount" select="."/>
    <xsl:for-each select="@GroupCode">
        <xsl:text>��� ������              : </xsl:text><xsl:value-of select="."/>
        <xsl:if test=".='INTR'"><xsl:text> - ������������� �������&#13;&#10;</xsl:text></xsl:if>
        <xsl:if test=".='QWRH'"><xsl:text> - ������� ������������ ��� ���������� �� ��������� ����&#13;&#10;</xsl:text></xsl:if>
        <xsl:if test=".='QWAP'"><xsl:text> - ������� ��������., ��������� ������. �� ���������� ��������&#13;&#10;</xsl:text></xsl:if>
        <xsl:if test=".='QSPC'"><xsl:text> - ������� �� ����������� � ���� ������������&#13;&#10;</xsl:text></xsl:if>
        <xsl:if test=".='QWCH'"><xsl:text> - ������� ������������, ��������� ��������&#13;&#10;</xsl:text></xsl:if>
    </xsl:for-each>

    <xsl:for-each select="@Cnt">
        <xsl:text>���������� ������������ : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@Amount">
        <xsl:text>�����                   : </xsl:text>
      <xsl:call-template name="ffsum"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:if test="count($ByAccount) != 0">
      <xsl:choose>
        <xsl:when test="$DC=1">
          <xsl:text>� ��������� �� ������ ����������  :&#13;&#10;</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>� ��������� �� ������ ��������  :&#13;&#10;</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:text>----------------------------------------------------------------------------------&#13;&#10;</xsl:text>
      <xsl:text>��� �� ��� ��� |    ����� �����       | ���-�� ������������ :       �����&#13;&#10;</xsl:text>
      <xsl:text>----------------------------------------------------------------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="$ByAccount">
        <xsl:call-template name="Center">
          <xsl:with-param name="input" select="@BIC"/>
          <xsl:with-param name="n" select="16"/>
        </xsl:call-template>
        <xsl:call-template name="Center">
          <xsl:with-param name="input" select="@Account"/>
          <xsl:with-param name="n" select="23"/>
        </xsl:call-template>
        <xsl:call-template name="Center">
          <xsl:with-param name="input" select="@Cnt"/>
          <xsl:with-param name="n" select="22"/>
        </xsl:call-template>
        <xsl:for-each select="@Amount">
          <xsl:call-template name="fsum"/>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>----------------------------------------------------------------------------------&#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>
  </xsl:template>


</xsl:stylesheet>