<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ����� ����������� ���������� �� �������  -->

  <!--  LiqInfo  -->
  <xsl:template name="LiqInfo">
      <xsl:text>��� ��� : </xsl:text>
      <xsl:value-of select="@BIC"/>
      <xsl:text>   ����� ������������ ����������� ���������� : </xsl:text><xsl:value-of select="@PKOITUTime"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:Liquidity">
        <xsl:for-each select="@LiquiditySum">
          <xsl:text>   1.����� �����&#13;&#10;</xsl:text>
          <xsl:text>   -----------------&#13;&#10;</xsl:text>
          <xsl:text>   ����� ��������� ������� ���   : </xsl:text><xsl:call-template name="fsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@Debt">
          <xsl:text>   �������������� ������ ���/��� : </xsl:text><xsl:call-template name="fsum"/>
          <xsl:text>&#13;&#10;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>

      <xsl:for-each select="ed:CorrespAccBal">
        <xsl:for-each select="@OutBal">
          <xsl:text>   2.������� ������� �� �������� �� � ���&#13;&#10;</xsl:text>
          <xsl:text>   ------------------------------------------&#13;&#10;</xsl:text>
          <xsl:text>   ����� ������� � ������� �������� �� : </xsl:text><xsl:call-template name="fsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@CreditLimitSum">
          <xsl:text>   C���� �������������� ������ ���/��� : </xsl:text><xsl:call-template name="fsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@ReservedSum">
          <xsl:text>   ����� ��������������� �������       : </xsl:text><xsl:call-template name="fsum"/>
          <xsl:text>&#13;&#10;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>

      <xsl:for-each select="ed:RTGSLiquidity">
        <xsl:for-each select="@RTGSOutBal">
          <xsl:text>   3.��������� ������� �� ����������� ��� � ����&#13;&#10;</xsl:text>
          <xsl:text>   ---------------------------------------------&#13;&#10;</xsl:text>
          <xsl:text>   ����� ������� � ������� ����� ��� � �� : </xsl:text><xsl:call-template name="fsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@RTGSReservedSum">
          <xsl:text>   ����� ��������������� ������� �� ����� : </xsl:text><xsl:call-template name="fsum"/>
          <xsl:text>&#13;&#10;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>

      <xsl:for-each select="ed:PaytTurnState">
        <xsl:for-each select="@TUTurnPayment">
          <xsl:text>   4.��������� ������� ���������� ��������&#13;&#10;</xsl:text>
          <xsl:text>   ----------------------------------------&#13;&#10;</xsl:text>
          <xsl:text>   ��������� ������� ���������� �������� � ��� ��       : </xsl:text><xsl:call-template name="fsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@RTGSTurnPayment">
          <xsl:text>   ��������� ������� ���������� �������� � ������� ���� : </xsl:text><xsl:call-template name="fsum"/>
          <xsl:text>&#13;&#10;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>

      <xsl:for-each select="ed:EstimatedIncome">
        <xsl:for-each select="@EstimatedIncomeSum">
          <xsl:text>   5.��������� ����������� �� ������ ���������� ������� ����&#13;&#10;</xsl:text>
          <xsl:text>   ----------------------------------------------------------&#13;&#10;</xsl:text>
          <xsl:text>   ����� ��������� �����������  : </xsl:text><xsl:call-template name="fsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>
  </xsl:template>

  <!--  LimitTransKind  -->
  <xsl:template name="LimitTransKind">
      <xsl:text>��� ������      : </xsl:text>
      <xsl:value-of select="@LimitTransKind"/>
      <xsl:if test="@LimitTransKind=0"><xsl:text> (���)</xsl:text></xsl:if>
      <xsl:if test="@LimitTransKind=1"><xsl:text> (�����)</xsl:text></xsl:if>
      <xsl:if test="@LimitTransKind=2"><xsl:text> (������������)</xsl:text></xsl:if>
      <xsl:if test="@LimitTransKind=3"><xsl:text> (��������������)</xsl:text></xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--  LimitInfo  -->
  <xsl:template name="LimitInfo">
      <xsl:if test="@PURBIC!=''"><xsl:text>��� ���         : </xsl:text><xsl:value-of select="@PURBIC"/></xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>����� ������    : </xsl:text><xsl:for-each select="@LimitSum"><xsl:call-template name="fsum"/></xsl:for-each>
      <xsl:text>&#13;&#10;&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>