<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED511  -->
  <xsl:template match="ed:ED511">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>���� �� ������, ��������������� ������ ������ ������������ ���� /ED511/&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>�������� ������              : </xsl:text>
      <xsl:for-each select="@ServicesBeginDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text> - </xsl:text>
      <xsl:for-each select="@ServicesEndDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>��� ���                      : </xsl:text>
      <xsl:value-of select="@BIC"/> <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>��� ����� ������             : </xsl:text>
      <xsl:value-of select="@INN"/> <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>��� ���                      : </xsl:text>
      <xsl:value-of select="@KPP"/> <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>���� � ����� ������ ���      : </xsl:text>
      <xsl:value-of select="@PayeePersonalAcc"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>������������ ����� �� ������&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>����������� �����            : </xsl:text>
      <xsl:for-each select="@ChargedSum"><xsl:call-template name="ffsum"/></xsl:for-each>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>����� ���������� �������     : </xsl:text>
      <xsl:for-each select="@PrepaymentSum"><xsl:call-template name="ffsum"/></xsl:for-each>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>����� ���������              : </xsl:text>
      <xsl:for-each select="@OverpaymentSum"><xsl:call-template name="ffsum"/></xsl:for-each>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>����� ������������           : </xsl:text>
      <xsl:for-each select="@ExtraChargeSum"><xsl:call-template name="ffsum"/></xsl:for-each>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>����� � ������               : </xsl:text>
      <xsl:for-each select="@Sum"><xsl:call-template name="ffsum"/></xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
