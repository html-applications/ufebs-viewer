<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED462  -->
  <xsl:template match="ed:ED462">
      <xsl:param name="pos" select="position()-$modif_pos"/>
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat($pos,'.  ')"/>
      <xsl:text>������ �� �����/��������� �������� ����������  � </xsl:text>
      <xsl:value-of select="concat(ed:Request/@DocNo,'  �� ')"/>
      <xsl:for-each select="ed:Request/@DocDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:Request">
        <xsl:text>��� �����������          : </xsl:text><xsl:value-of select="@OrgBIC"/>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>������������ ����������� : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="@NameClient"/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                         : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��� ����������           : </xsl:text><xsl:value-of select="@BicPBR"/>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>������������ ����������  : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="@NamePBR"/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                         : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:text>��� ��������             : </xsl:text>
        <xsl:if test="@OperationType = 1">����� �������� ����������</xsl:if>
        <xsl:if test="@OperationType = 2">��������� �������� ����������</xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:text>����������� ���� ���������� ��������   : </xsl:text>
        <xsl:for-each select="@OperationDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>������� ���� ��� ���������� / �������� : </xsl:text><xsl:value-of select="@Acc"/>
        <xsl:text>     ����� : </xsl:text>
        <xsl:for-each select="@Sum">
          <xsl:call-template name="ffsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>

      <xsl:if test="count(ed:Cash) > 0">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>�������� ����������  &#13;&#10;</xsl:text>
        <xsl:text>==================== &#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>��� ����������          �������             �����                ��� ��������&#13;&#10;</xsl:text>
        <xsl:text>---------------------------------------------------------------------------------&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:Cash">
          <xsl:if test="@CashType = 0">
            <xsl:call-template name="Center">
              <xsl:with-param name="input" select="string('������')"/>
              <xsl:with-param name="n" select="14"/>
            </xsl:call-template>
          </xsl:if>
          <xsl:if test="@CashType = 1">
            <xsl:call-template name="Center">
              <xsl:with-param name="input" select="string('��������')"/>
              <xsl:with-param name="n" select="14"/>
            </xsl:call-template>
          </xsl:if>
          <xsl:text> </xsl:text>
          <xsl:for-each select="@Nominal"><xsl:call-template name="fsum"/></xsl:for-each>
          <xsl:text> </xsl:text>
          <xsl:for-each select="@Sum"><xsl:call-template name="fsum"/></xsl:for-each>
          <xsl:text> </xsl:text>
          <xsl:for-each select="@ValsPackType">
            <xsl:if test=".  = 1">      ������� ��� �������</xsl:if>
            <xsl:if test=". = 10">      ����� �������</xsl:if>
            <xsl:if test=". = 11">      ����� � ������� �������</xsl:if>
            <xsl:if test=". = 12">      ����� � �������</xsl:if>
            <xsl:if test=". = 15">      ����� � ������� �������</xsl:if>
            <xsl:if test=". = 19">      ����� � �������</xsl:if>
            <xsl:if test=". = 22">      ������� �������</xsl:if>
            <xsl:if test=". = 23">      ����� 100</xsl:if>
            <xsl:if test=". = 24">      �������������� �����</xsl:if>
            <xsl:if test=". = 25">      ����� � ��������������� ��������</xsl:if>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:if>

      <xsl:text>� ��� ����� �� ��������  &#13;&#10;</xsl:text>
      <xsl:text>=========================&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>������            �����  &#13;&#10;</xsl:text>
      <xsl:text>-------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:CashInfo">
        <xsl:value-of select="concat(@CashCode,'     ')"/>
        <xsl:for-each select="@CashSum"><xsl:call-template name="fsum"/></xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��������� / ���������� : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="ed:CustomerData/@CustomerName"/>
        <xsl:with-param name="width" select="50"/>
        <xsl:with-param name="before" select="string('                       : ')"/>
      </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:Annotation">
        <xsl:text>����������             : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="."/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                       : ')"/>
        </xsl:call-template>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>����������� &#13;&#10;</xsl:text>
      <xsl:text>===============&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:CreatorInfo">
        <xsl:value-of select="position()"/>
        <xsl:value-of select="concat('.',@Position,' ',@FIO)"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
