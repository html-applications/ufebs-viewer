<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

<!--  ED221  -->
  <xsl:template match="ed:ED221">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>����� �� ��������� �� ����� ��� ������� ���������� ��� ���������� /ED221/</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:if test="ed:InitialED/@EDNo != ''">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:apply-templates select="ed:InitialED"/>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>


      <xsl:if test="@ExcludedEDQuantity > 0">
          <xsl:text>���������� ������/�������.���-��� �� ����� ����������  : </xsl:text>
          <xsl:value-of select="@ExcludedEDQuantity"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@ExcludedEDSum > 0">
          <xsl:text>����� ����� ������/�������.���-��� �� ����� ���������� : </xsl:text>
          <xsl:for-each select="@ExcludedEDSum"><xsl:call-template name="summa"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="@CreditQuantity > 0">
          <xsl:text>���������� ���������� �� ������� ����� ����������      : </xsl:text>
          <xsl:value-of select="@CreditQuantity"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="@CreditSum > 0">
          <xsl:text>����� ����� ���������� �� ������� ����� ����������     : </xsl:text>
          <xsl:for-each select="@CreditSum"><xsl:call-template name="summa"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="@DebetQuantity > 0">
          <xsl:text>���������� ���������� �� ������ ����� ����������       : </xsl:text>
          <xsl:value-of select="@DebetQuantity"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="@DebetSum > 0">
          <xsl:text>����� ����� ���������� �� ������ ����� ����������      : </xsl:text>
          <xsl:for-each select="@DebetSum"><xsl:call-template name="summa"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="@CreditLimitSum > 0">
          <xsl:text>����� ������ �������������� �������    : </xsl:text>
          <xsl:for-each select="@CreditLimitSum"><xsl:call-template name="summa"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="@ReservedSum > 0">
          <xsl:text>����� ��������������� �������          : </xsl:text>
          <xsl:for-each select="@ReservedSum"><xsl:call-template name="summa"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>����� �����, �� �������� ����������� ���� : </xsl:text>
      <xsl:value-of select="@Acc"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>��������� ������� �� ����� ���������   : </xsl:text>
      <xsl:for-each select="@OutBal"><xsl:call-template name="summa"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>�������� ������� �� ����� ���������    : </xsl:text>
      <xsl:for-each select="@EnterBal"><xsl:call-template name="summa"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>����, �� ������� ����������� �������   : </xsl:text>
      <xsl:for-each select="@AbstractDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>����� ������� ������������ �������     : </xsl:text>
      <xsl:value-of select="@EndTime"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>���� ���������� �������� �� �����      : </xsl:text>
      <xsl:for-each select="@TransDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:choose> <xsl:when test="ed:PartInfo">
          <xsl:apply-templates select="ed:PartInfo"/>
      </xsl:when> </xsl:choose>

      <xsl:if test="count(ed:EDCopyBrf) != 0">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>���������� (����� �������� �����) �� ���&#13;&#10;</xsl:text>
        <xsl:text>-----------------------------------------&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:EDCopyBrf">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:if test="ed:EDRefID/@EDNo != ''">
            <xsl:apply-templates select="ed:EDRefID"/>
          </xsl:if>
          <xsl:if test="@CtrlCode != ''">
            <xsl:text>��� ���������� �������� : </xsl:text><xsl:value-of select="@CtrlCode"/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:if>
          <xsl:text>�����    : </xsl:text>
          <xsl:for-each select="@Sum"><xsl:call-template name="summa"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>

        </xsl:for-each>
      </xsl:if>

      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:choose>
          <xsl:when test="ed:EDCopy">

              <xsl:text>  �     ���    ���  ��       �     ����        ���         �     ����                  � � � � � � � � � �                                                      � � � � � � � � � �</xsl:text>
              <xsl:text>&#13;&#10;</xsl:text>
              <xsl:text> �/�   ������   ��          ��      ��         ��          ��     ��        ���             ��                   ��                   �����         ���             ��                   ��</xsl:text>
              <xsl:text>&#13;&#10;</xsl:text>

              <xsl:for-each select="ed:EDCopy">
                <xsl:call-template name="LeftPad">
                  <xsl:with-param name="input" select="position()"/>
                  <xsl:with-param name="n" select="6"/>
                </xsl:call-template>
                <xsl:for-each select="*[starts-with(local-name(),'ED')]">
                    <xsl:call-template name="ED_Tab">
                      <xsl:with-param name="ed" select="."/>
                      <xsl:with-param name="pos" select="position()"/>
                    </xsl:call-template>
                </xsl:for-each>
              </xsl:for-each>
          </xsl:when>
      </xsl:choose>

  </xsl:template>

</xsl:stylesheet>