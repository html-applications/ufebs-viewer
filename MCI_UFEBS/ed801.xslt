<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������). -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED801  -->
  <xsl:template match="ed:ED801">
    <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
    <xsl:text>������ ���������� � ����������� /ED801/&#13;&#10;</xsl:text>
    <xsl:call-template name="PriznGr"/>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:for-each select="@InfoTypeCode">
      <xsl:call-template name="InfoTypeCode">
        <xsl:with-param name="str" select="'��� ������� : '"/>
      </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
    <xsl:for-each select="@PoolInfoFlag">
      <xsl:call-template name="LongText">
        <xsl:with-param name="text_f"
          select="'���������� ������������� �� �����,������������������� �� ���� �������� BIC � Account'"/>
        <xsl:with-param name="text_t"
          select="'���������� ������������� �� ���� ������ ���� �����������'"/>
      </xsl:call-template>
    </xsl:for-each>
    <xsl:for-each select="@BIC">
      <xsl:text>��� ���������, �� ����� �������� ������������ ������ : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
    <xsl:for-each select="@Account">
      <xsl:text>����, �� �������� ������������ ������                : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#13;&#10;</xsl:text>

  </xsl:template>

</xsl:stylesheet>