<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED421  -->
  <xsl:template match="ed:ED421">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>������ �� ������� � ��������� �������� ����� ������ /&#13;&#10;</xsl:text>
      <xsl:text>    ��������� �� ��������� ������� ����� ������ �� ������������� ���������� ������ /ED421/ </xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>

      <xsl:text>����� ����������� ��                      : </xsl:text><xsl:value-of select="@EDTime"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>�������� � ��������� ��������&#13;&#10;</xsl:text>
      <xsl:text>=============================&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:CreditOpTerms">
        <xsl:text>����� ������/���������                    : </xsl:text><xsl:value-of select="@ReqNum"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>���� � ����� ����������� ������/��������� : </xsl:text><xsl:value-of select="@ReqDateTime"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="@CreditConsDate">
          <xsl:text>���� ������������ ���������� ��������     : </xsl:text><xsl:call-template name="date"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>����� ������������ ���������� ��������    : </xsl:text><xsl:value-of select="@CreditConsNo"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="@CreditOpDate">
          <xsl:text>���� ���������� ���������� �������� ���   :&#13;&#10;</xsl:text>
          <xsl:text>��������� �������� �� �������������       :&#13;&#10;</xsl:text>
          <xsl:text>���������� ������                         : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>

        <xsl:text>������ ���������� ��������� ��������      : </xsl:text>
        <xsl:value-of select="@CreditOpMethod"/>
        <xsl:if test="@CreditOpMethod = 0"><xsl:text> (����.�������� �� ����.������, ������ ���������� � ������������)</xsl:text></xsl:if>
        <xsl:if test="@CreditOpMethod = 1"><xsl:text> (����.�������� �� ����.������, ������ ���������� � �����������)</xsl:text></xsl:if>
        <xsl:if test="@CreditOpMethod = 2"><xsl:text> (����.�������� �� ����.������, �������� ��������� �������)</xsl:text></xsl:if>
        <xsl:if test="@CreditOpMethod = 3"><xsl:text> (����.�������� �� ������������� ���������� ������)</xsl:text></xsl:if>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:for-each select="@GrantDate">
          <xsl:text>���� �������������� ������� ����� ������  : </xsl:text><xsl:call-template name="date"/>
        </xsl:for-each>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>����, �� ������� ��������������� ������   : </xsl:text><xsl:value-of select="@ReqCredTerm"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>����� �������                             : </xsl:text>
        <xsl:for-each select="@ApplicationSum"><xsl:call-template name="ffsum"/></xsl:for-each>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>���������� ������ �� �������              : </xsl:text><xsl:value-of select="@WideApplicationRate"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:if test="@CreditOpMethod != '3'">
          <xsl:text>�������� �� ��������� ��������������      : </xsl:text><xsl:value-of select="@IsPartable"/>
          <xsl:if test="@IsPartable = 0"><xsl:text> (���)</xsl:text></xsl:if>
          <xsl:if test="@IsPartable = 1"><xsl:text> (��)</xsl:text></xsl:if>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:text>�������� �� �������������� �� ������ &#13;&#10;</xsl:text>
          <xsl:text>���������                                 : </xsl:text><xsl:value-of select="@IsTrancCut"/>
          <xsl:if test="@IsTrancCut = 0"><xsl:text> (���)</xsl:text></xsl:if>
          <xsl:if test="@IsTrancCut = 1"><xsl:text> (��)</xsl:text></xsl:if>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:if>
        <xsl:text>����������� ��������� ������������ ��     : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="ed:OrgName"/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                          : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>��� ����� �������� ��������� ����� ��     : </xsl:text><xsl:value-of select="@BIC"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>����� ��������� ����� ��                  : </xsl:text><xsl:value-of select="@CorrAcc"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>


        <xsl:text>��������������� ����� ����.�����������    : </xsl:text><xsl:value-of select="@OrgRegNum"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��� ����������� : </xsl:text>
        <xsl:value-of select="@AssetType"/>
        <xsl:if test="@AssetType = 1">
          <xsl:text> (������� ��� ����� (����������) ������ ����� �� ����������� ������ ����� ������ /&#13;&#10;</xsl:text>
          <xsl:text>                     ���������, ����������� � ����� �������������� �������������� ��������)</xsl:text>
        </xsl:if>
        <xsl:if test="@AssetType = 2"><xsl:text> (�������, ������������ ����������� ��������)</xsl:text></xsl:if>
        <xsl:if test="@AssetType = 3"><xsl:text> (�������, ������������ ����������������)</xsl:text></xsl:if>
        <xsl:if test="@AssetType = 4"><xsl:text> (�������, ������������ �������)</xsl:text></xsl:if>
        <xsl:if test="@AssetType = 5"><xsl:text> (�������, ������������ ����������)</xsl:text></xsl:if>
        <xsl:if test="@AssetType = 6">
          <xsl:text> (�������, ������������ ������� ���� ���������� �� ��������� ���������,&#13;&#10;</xsl:text>
          <xsl:text>                     ������������ ���������� ����������� ��� �����л)</xsl:text></xsl:if>
        <xsl:if test="@AssetType = 7">
          <xsl:text> (�������, ������������ ������� ���� ���������� �� ��������,&#13;&#10;</xsl:text>
          <xsl:text>                     ��������������� � ������ ���������� �������������� ��������)</xsl:text></xsl:if>
        <xsl:if test="@AssetType = 8">
          <xsl:text> (�������, ������������ ������� ���� ���������� �� ��������,��������������� &#13;&#10;</xsl:text>
          <xsl:text>                     �� ���� ������������ ������������� ���������� ���� ��� &#13;&#10;</xsl:text>
          <xsl:text>                     ������ ������������ �������� �����������)</xsl:text></xsl:if>
        <xsl:if test="@AssetType = 9"><xsl:text> (�������, ������������ ����������� � ��������� ��������� ��)</xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:for-each select="@CreditNum">
          <xsl:text>����� ������� (����� ���������            :&#13;&#10;</xsl:text>
          <xsl:text>� �������������� �������) ����� ������,   :&#13;&#10;</xsl:text>
          <xsl:text>� ������ �� �������� ��������� ������,    :&#13;&#10;</xsl:text>
          <xsl:text>������������ ��� ������������ �������     :&#13;&#10;</xsl:text>
          <xsl:text>����������� �� ������ �������             : </xsl:text><xsl:value-of select="."/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>

        <xsl:for-each select="@CreditDate">
          <xsl:text>���� �������������� ������� ����� ������, :&#13;&#10;</xsl:text>
          <xsl:text>� ������ �� �������� ��������� ������,    :&#13;&#10;</xsl:text>
          <xsl:text>������������ ��� ������������ �������     :&#13;&#10;</xsl:text>
          <xsl:text>����������� �� ������ �������             : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>

        <xsl:if test="@DepoCode != ''">
          <xsl:text>��� �����������                           : </xsl:text><xsl:value-of select="@DepoCode"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:text>� ����� ���� � �����������                : </xsl:text><xsl:value-of select="@DepoAccount"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:text>� ������� ����� ���� ��,                  :&#13;&#10;</xsl:text>
          <xsl:text>��������������� ��� �������� ������ ����� :&#13;&#10;</xsl:text>
          <xsl:text>����� ��������� ������� ����� ������ �    :&#13;&#10;</xsl:text>
          <xsl:text>��������� �� ����                         : </xsl:text><xsl:value-of select="@DepoPartition"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:text>������������ �����������                  : </xsl:text>
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="ed:DepoName"/>
            <xsl:with-param name="width" select="50"/>
            <xsl:with-param name="before" select="string('                                          : ')"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:if>


        <xsl:if test="count(ed:Guarantee) > 0">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>                            ��������������&#13;&#10;</xsl:text>
          <xsl:text>--------------------------------------------------------------------------------------------&#13;&#10;</xsl:text>
          <xsl:text>  � �/�    ����� ����������    ���� ����������      ������������ ��-����������&#13;&#10;</xsl:text>
          <xsl:text>--------------------------------------------------------------------------------------------&#13;&#10;</xsl:text>
          <xsl:apply-templates select="ed:Guarantee"/>
          <xsl:text>--------------------------------------------------------------------------------------------&#13;&#10;</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>

      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--  Guarantee  -->
  <xsl:template match="ed:Guarantee">
      <xsl:call-template name="LeftPad">
        <xsl:with-param name="input" select="position()"/>
        <xsl:with-param name="n" select="7"/>
      </xsl:call-template>
      <xsl:call-template name="Center">
        <xsl:with-param name="input" select="@AgreementNumber"/>
        <xsl:with-param name="n" select="22"/>
      </xsl:call-template>
      <xsl:value-of select="string('     ')"/>
      <xsl:for-each select="@AgreementDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:value-of select="string('     ')"/>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="ed:GuarantorName"/>
        <xsl:with-param name="width" select="40"/>
        <xsl:with-param name="before" select="string('                                                 ')"/>
      </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>