<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- ��������� ������ ������� ��������� (ED808) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

<!--  ed:ED808  -->


<xsl:template match="ed:ED808">

  <xsl:text>������� ���������   (ED808) &#13;&#10;&#13;&#10;</xsl:text>
  <xsl:call-template name="PriznGr"/>
  <xsl:text>&#13;&#10;</xsl:text>
  <xsl:text>���� � ����� �������� ��      : </xsl:text><xsl:value-of select="@CreationDateTime"/>
  <xsl:text>&#13;&#10;</xsl:text>

  <xsl:for-each select="@InfoTypeCode">
    <xsl:call-template name="InfoTypeCode"/>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:for-each>

  <xsl:for-each select="ed:CreationReason">
    <xsl:text>������� ������������ ������� ���������&#13;&#10;</xsl:text>
    <xsl:text>=======================================&#13;&#10;&#13;&#10;</xsl:text>
    <xsl:for-each select="ed:CreationReasonCode">
      <xsl:call-template name="CreationReason">
        <xsl:with-param name="str" select="''"/>
      </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
  </xsl:for-each>


  <xsl:if test= "count(ed:BICDirectoryEntry) != 0">
    <xsl:text>������ � ����������� ���&#13;&#10;</xsl:text>
    <xsl:text>==========================&#13;&#10;&#13;&#10;</xsl:text>
    <xsl:apply-templates select="ed:BICDirectoryEntry">
      <xsl:with-param name="pref" select="''"/>
    </xsl:apply-templates>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:if>

  <xsl:for-each select="ed:ParticipantProfile">
    <xsl:for-each select="ed:ParticipantProfileInfo">
      <xsl:text>���������� �� ���������, �������� � ������� ���������&#13;&#10;</xsl:text>
      <xsl:text>=====================================================&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:for-each select="@PtType">
        <xsl:call-template name="PtType"/>
      </xsl:for-each>
      <xsl:text>��� ���, ������. ��������� �������� : </xsl:text>
      <xsl:value-of select="@ServCBRBIC"/><xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>����� ��� �� ���������              : </xsl:text>
      <xsl:value-of select="@DefaultARMNo"/><xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="@ChannelMode">
        <xsl:text>������� ���.������ �������������� � �� �� : </xsl:text><xsl:value-of select="."/>
        <xsl:if test=".= 3"><xsl:text> - ����</xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:UFCFInfo">
        <xsl:for-each select="ed:UFBIC">
          <xsl:text>��� ��������������� �������       : </xsl:text><xsl:value-of select="."/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:if test= "count(ed:CFBICList) != 0">
          <xsl:text>C����� ��� ���������������� ��������&#13;&#10;</xsl:text>
          <xsl:text>====================================&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:CFBICList/ed:CFBIC">
            <xsl:value-of select="."/><xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
      </xsl:for-each>

      <xsl:for-each select="ed:IntradayRestrictions">
        <xsl:text>�����������, ���������� �� ���������&#13;&#10;</xsl:text>
        <xsl:text>====================================&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:StopUrgentFTS">
          <xsl:call-template name="IntradayRestrictions">
            <xsl:with-param name="type"
              select="'����������� �������������� ������� �������� �������� :'"/>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:for-each select="ed:StopSendFTI">
          <xsl:call-template name="IntradayRestrictions">
            <xsl:with-param name="type"
              select="'����������� ����������� ������������ :'"/>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:for-each select="ed:StopSendRecvEM">
          <xsl:call-template name="IntradayRestrictions">
            <xsl:with-param name="type"
              select="'����������� ����������� � ��������� ����������� ��������� :'"/>
          </xsl:call-template>

        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:for-each select="ed:StopSendEM">
          <xsl:call-template name="IntradayRestrictions">
            <xsl:with-param name="type"
              select="'����������� ����������� ����������� ��������� :'"/>
          </xsl:call-template>

        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:for-each select="ed:PendingDeletion">
          <xsl:call-template name="IntradayRestrictions">
            <xsl:with-param name="type"
              select="'� �������� �������� :'"/>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

      </xsl:for-each>

      <xsl:for-each select="ed:ParticipantAttributes">
        <xsl:text>�������� ���������&#13;&#10;</xsl:text>
        <xsl:text>===================&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="@CanBeAuthorOfConfLiqRsrv">
          <xsl:call-template name="LongText">
            <xsl:with-param name="text_t" select="'�������� ����� '"/>
            <xsl:with-param name="text_f" select="'�������� �� ����� '"/>
            <xsl:with-param name="text"
              select="'���� ������������ ��������������� ��������� � ������������ �� ������ �������������� �����������'"/>
          </xsl:call-template>
        </xsl:for-each>

        <xsl:for-each select="@CanBeAuthorOfCover">
          <xsl:call-template name="LongText">
            <xsl:with-param name="text_t" select="'�������� ����� '"/>
            <xsl:with-param name="text_f" select="'�������� �� ����� '"/>
            <xsl:with-param name="text"
              select="'���������� ��������� ������������ � �������������� ����������� ��������� ����������'"/>
          </xsl:call-template>
        </xsl:for-each>


        <xsl:for-each select="@CanSendDD">
          <xsl:call-template name="LongText">
            <xsl:with-param name="text_t" select="'�������� ����� '"/>
            <xsl:with-param name="text_f" select="'�������� �� ����� '"/>
            <xsl:with-param name="text"
              select="'���� ������������ ���������� � ������ �������� ���������� ��������'"/>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:for-each>

    <!-- ���� �� �����������

      <xsl:for-each select="ed:EPSInfo">
        <xsl:text>��������, ����������� � ��� � ����������� ��� ���������� ���&#13;&#10;</xsl:text>
        <xsl:text>=============================================================&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="@RCPAllowed">
          <xsl:call-template name="LongText">
            <xsl:with-param name="ind" select="."/>
            <xsl:with-param name="text_t" select="'��� ����� ���� ���������'"/>
            <xsl:with-param name="text_f" select="'��� �� ����� ���� ���������'"/>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:for-each>
    -->

      <xsl:if test= "count(ed:AccountsAuthorizedForDD) != 0">
        <xsl:text>������ ���������� � �� ������,��� ������� ������ ��������&#13;&#10;</xsl:text>
        <xsl:text>����� �������� �������������� ������������ ����������&#13;&#10;</xsl:text>
        <xsl:text>=========================================================&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:AccountsAuthorizedForDD/ed:AccountsList">
          <xsl:text>���    : </xsl:text><xsl:value-of select="@BIC"/><xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>����   : </xsl:text><xsl:value-of select="@Account"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:if>
    </xsl:for-each>


    <xsl:if test= "count(ed:AccountsInfo) != 0">
      <xsl:text>���������� � ������, �������� � ������� ���������&#13;&#10;</xsl:text>
      <xsl:text>=====================================================&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:AccountsInfo">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>����� ����� : </xsl:text><xsl:value-of select="@Account"/><xsl:text>&#13;&#10;</xsl:text>
        <xsl:for-each select="@AccountType">
          <xsl:call-template name="AccountType">
            <xsl:with-param name="str" select="'��� �����   : '"/>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:for-each select="@DateOfDeletion">
          <xsl:text>���� ����.���������� ���. � ����� : </xsl:text>
          <xsl:call-template name="date"/><xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>����� ��� �� ��������� ��� �����  : </xsl:text>
        <xsl:value-of select="@DefaultARMNo"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:for-each select="ed:AccIntradayRestrictions">
          <xsl:text>����������� �� �����&#13;&#10;</xsl:text>
          <xsl:text>----------------------&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:StopDebits">
            <xsl:call-template name="IntradayRestrictions">
              <xsl:with-param name="type"
                select="'��������� �������� :'"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>

          <xsl:for-each select="ed:StopCredits">
            <xsl:call-template name="IntradayRestrictions">
              <xsl:with-param name="type"
                select="'��������� ���������� :'"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>

          <xsl:for-each select="ed:StopUrgentFTS">
            <xsl:call-template name="IntradayRestrictions">
              <xsl:with-param name="type"
                select="'��������� �������������� ������� �������� �������� :'"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>

          <xsl:for-each select="ed:PendingDeletion">
            <xsl:call-template name="IntradayRestrictions">
              <xsl:with-param name="type"
                select="'� �������� �������� :'"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>

          <xsl:for-each select="ed:RouteToCBR">
            <xsl:call-template name="IntradayRestrictions">
              <xsl:with-param name="type"
                select="'�������� ��� ����� ����������� :'"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>

          <xsl:for-each select="ed:ManualVerifMode">
            <xsl:call-template name="IntradayRestrictions">
              <xsl:with-param name="type"
                select="'��������������� �������� ��� :'"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>

          <xsl:for-each select="ed:Arrest">
            <xsl:call-template name="IntradayRestrictions">
              <xsl:with-param name="type"
                select="'����� ��� ����������� �� �������������� ����� :'"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>

          <xsl:for-each select="ed:SpecialMode">
            <xsl:call-template name="IntradayRestrictions">
              <xsl:with-param name="type"
                select="'������� � ����� ������� ������������� � ���� ������������ :'"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>

        <xsl:for-each select="ed:PendingDeletionInfo">
          <xsl:text>���������� � ����������� �������� �����&#13;&#10;</xsl:text>
          <xsl:text>----------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:for-each select="@AccountCloseDate">
            <xsl:text>��������������� ���� �������� : </xsl:text>
            <xsl:call-template name="date"/><xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:DestinationInfo">
            <xsl:text>��� �����, � �������   : </xsl:text><xsl:value-of select="@DestinationBIC"/><xsl:text>&#13;&#10;</xsl:text>
            <xsl:text>������ ���� ���������� :&#13;&#10;</xsl:text>
            <xsl:text>���� ����������        : </xsl:text><xsl:value-of select="@DestinationAccount"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:for-each>

        <xsl:for-each select="ed:BalanceSweepingAccount">
          <xsl:text>���������� � �����, �� ������� ������������&#13;&#10;</xsl:text>
          <xsl:text>������������� ������������ �������� �������&#13;&#10;</xsl:text>
          <xsl:text>--------------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:text>���  : </xsl:text><xsl:value-of select="@BIC"/><xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>���� : </xsl:text><xsl:value-of select="@Account"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:DailyTransfer/ed:TimeTransfer">
              <xsl:text>����� ��� ����������� �������� ������� �������� ������� : </xsl:text>
              <xsl:value-of select="."/><xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:DailyTransfer/ed:EventTransfer">
            <xsl:text>������� ��� ����������� �������� ������� �������� �������&#13;&#10;</xsl:text>
            <xsl:text>---------------------------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
            <xsl:for-each select="@EventCode">
              <xsl:text>��� ���� ������� : </xsl:text><xsl:value-of select="."/>
              <xsl:if test=".= 'INCC'">
                <xsl:text> - ������������� ����������������� ����</xsl:text><xsl:text>&#13;&#10;</xsl:text>
              </xsl:if>
            </xsl:for-each>
            <xsl:text>����� ������� : </xsl:text><xsl:value-of select="@EventNumber"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:for-each>

        <xsl:for-each select="ed:PoolSPInfo">
          <xsl:text>���������� �� ���������� ���� �����������&#13;&#10;</xsl:text>
          <xsl:text>-------------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:ListOfSP">
            <xsl:text>������ ����������� ���������� ����&#13;&#10;</xsl:text>
            <xsl:text>----------------------------------&#13;&#10;&#13;&#10;</xsl:text>
            <xsl:for-each select="ed:SPInfo">
              <xsl:text>���  : </xsl:text><xsl:value-of select="@BIC"/><xsl:text>&#13;&#10;</xsl:text>
              <xsl:text>���� : </xsl:text><xsl:value-of select="@Account"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            </xsl:for-each>
          </xsl:for-each>

          <xsl:for-each select="ed:MainPoolParticipant">
            <xsl:text>��� � ���� ��������� ��������� ����&#13;&#10;</xsl:text>
            <xsl:text>------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
            <xsl:for-each select="@BIC">
              <xsl:text>��� �������� ��������� : </xsl:text><xsl:value-of select="."/><xsl:text>&#13;&#10;</xsl:text>
            </xsl:for-each>

            <xsl:for-each select="@Account">
              <xsl:text>����� �������� �����   : </xsl:text><xsl:value-of select="."/><xsl:text>&#13;&#10;</xsl:text>
            </xsl:for-each>
          </xsl:for-each>
        </xsl:for-each>

        <xsl:for-each select="ed:IPInfo">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>������ ��� ��������� ���������� ��� �������&#13;&#10;</xsl:text>
          <xsl:text>--------------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:BIC">
            <xsl:value-of select="."/><xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:for-each>


        <xsl:for-each select="ed:LimitsInfo">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>�������� � ������������� �������&#13;&#10;</xsl:text>
          <xsl:text>----------------------------------&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:LimitInfo">
            <xsl:call-template name="LimitInfo"/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:for-each>

        <xsl:for-each select="ed:AuthorizedClaimAuthors">
          <xsl:text>������ �������������� ������������ ����������&#13;&#10;</xsl:text>
          <xsl:text>-----------------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:AuthorizedClaimAuthorsList">
            <xsl:for-each select="@ClearingCircuit"><xsl:call-template name="ClearingCircuit"/></xsl:for-each>
            <xsl:for-each select="ed:BIC">
              <xsl:text>��� ��������������� ����������� ���������� : </xsl:text>
              <xsl:value-of select="."/><xsl:text>&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="ed:UID">
              <xsl:text>��� ��������������� ����������� ���������� : </xsl:text><xsl:value-of select="."/><xsl:text>&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:for-each>

        <xsl:for-each select="ed:ArrestsInfo">
          <xsl:text>���������� �� ������� �/��� ������������&#13;&#10;</xsl:text>
          <xsl:text>----------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:ArrestDetailedInfo">
            <xsl:call-template name="ArrestDetailedInfo"/>
          </xsl:for-each>
        </xsl:for-each>

      </xsl:for-each>

    </xsl:if>
  </xsl:for-each>

  <xsl:if test="ed:InitialED/@EDNo != ''">
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:apply-templates select="ed:InitialED"/>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:if>
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:template>


</xsl:stylesheet>
