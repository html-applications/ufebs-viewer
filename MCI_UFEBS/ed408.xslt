<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED408  -->
  <xsl:template match="ed:ED408">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>���������� � ����������� ������/��������� �� /ED408/</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>����� ����������� ��                       : </xsl:text><xsl:value-of select="@EDTime"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>���� (�� ����������� �������) ��������� �� : </xsl:text>
      <xsl:for-each select="@ReceiptDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>�����  (����������) ��������� ��           : </xsl:text><xsl:value-of select="@ReceiptTime"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��� ���������� ��������� ��  : </xsl:text><xsl:value-of select="@ResultCode"/>
      <xsl:if test="@ResultCode = 1"><xsl:text> (�� �������)</xsl:text></xsl:if>
      <xsl:if test="@ResultCode = 2"><xsl:text> (�� �����������)</xsl:text></xsl:if>
      <xsl:if test="@ResultCode = 3"><xsl:text> (�� ����������)</xsl:text></xsl:if>
      <xsl:if test="@ResultCode = 5"><xsl:text> (�������� � ������ ��������� � ���������� / ������ �� �������� � ��������)</xsl:text></xsl:if>
      <xsl:if test="@ResultCode = 6"><xsl:text> (���������� ���������/������ ����� ������ � ���������� / ������� � ��������)</xsl:text></xsl:if>
      <xsl:if test="@ResultCode = 7"><xsl:text> (������ �������������)</xsl:text></xsl:if>

      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="@CtrlCode">
          <xsl:text>��� ���������� ��������      : </xsl:text><xsl:value-of select="."/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@OperationType">
        <xsl:text>��� ���� ��������              : </xsl:text><xsl:value-of select="."/>
        <xsl:if test=". = 0"><xsl:text> ��� �������� �� ����� ���� ���������&#13;&#10;</xsl:text></xsl:if>
        <xsl:if test=". = 1">
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="concat('��������� �������� ����� ������ ��� ����� ',
              '������ ����� �� ����������� ������ ����� ������ ���� ��� ����� ���������')"/>
            <xsl:with-param name="width" select="50"/>
            <xsl:with-param name="before" select="string('                               :   ')"/>
          </xsl:call-template>
        </xsl:if>
        <xsl:if test=". = 2">
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="concat('��������� �������� ����� ������ ',
              '������������ ������� ���������� �������, �������������, ������, ',
              '������� ���� ���������� �� ��������� ���������, ������������ ���������� ',
              '����������� ��� �����л, ������� ���� ���������� �� ��������, ',
              '���������������  � ������ ���������� �������������� ��������, ',
              '������� ���� ���������� �� ��������, ��������������� �� ���� ������������ ������������� ',
              '���������� ���� ��� ������ ������������ �������� �����������, ',
              '����������� � ��������� ��������� ��')"/>
            <xsl:with-param name="width" select="50"/>
            <xsl:with-param name="before" select="string('                               :   ')"/>
          </xsl:call-template>
        </xsl:if>
        <xsl:if test=". = 3"><xsl:text> ���������� �������� ����� ������&#13;&#10;</xsl:text></xsl:if>

      </xsl:for-each>
      <xsl:for-each select="ed:Annotation">
          <xsl:text>����� ���������              : </xsl:text>
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="."/>
            <xsl:with-param name="width" select="50"/>
            <xsl:with-param name="before" select="string('                             : ')"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="ed:DetailInfo">
          <xsl:text>��������� ����������         : </xsl:text>
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="."/>
            <xsl:with-param name="width" select="50"/>
            <xsl:with-param name="before" select="string('                             : ')"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:if test="ed:EDRefID/@EDNo != ''">
          <xsl:apply-templates select="ed:EDRefID"/>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>