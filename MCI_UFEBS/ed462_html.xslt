<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="html" encoding="WINDOWS-1251"/>

 <!-- ED462 -->
<xsl:template match="ed:ED462">
  <xsl:call-template name="ED462"/>
</xsl:template>

 <!--  ����� �������� ����� ������ �� ���������� (ED462)  -->
 <!-- ======================================================================= -->
<xsl:template name="ED462">
<!--  -->
<html>

<head>
<meta charset="WINDOWS-1251"/>
<style>
html, body {font-family: 'Times New Roman', Times, serif; /* ��������� ������ */
  height:100%;
  margin:0;
  padding:0;
  /*background:#d33;*/
}

#wrapper {
  display: table;
  height: 100%;
}
/*
#header{
  height:100px;
  background: rgb(0,255,102);
}
*/

#footer {
  /*background:#769;*/
  display: table-row;
  /*display: table-footer-group;*/
  height: 10%;
}
#wrapper, #footer{
width:100%;
}
#content{
  /*background:#d33;*/
  /*display: table-row;*/
  height: 90%;
}

    @media all
    {
       table     {border-collapse:collapse; /* ������� ������� ����� ����� �������� */
         border:0; border-style:0; border-spacing:0;
       }
       td,th     {border: 1pt; padding:0; border-style:none none none none; font-size:11pt; }
       .line     {
        border-bottom:1px dashed #000; /* ��������� ����� */
        width:175mm; height:20mm;      /* ������ ����� */
       }                       /* t  r b l */
       .t        { border:windowtext 1pt; border-style:solid none none none;}
       .tr       { border:windowtext 1pt; border-style:solid solid none none;}
       .b        { border:windowtext 1pt;border-style:none none solid none;}
       .r        { border:windowtext 1pt;border-style:none solid none none; text-align:center;}
       .b_n      { border:white 1pt;border-style:none none solid none;} /* ��� ������������ ������������� � ��������������� */
/*       .bt       { border:windowtext 1pt;border-style:solid none solid none; text-align:center;} */
       .blt      { border:windowtext 1pt;border-style:solid none solid solid; text-align:center;}
/*       .br       { border:windowtext 1pt;border-style:none solid solid none; text-align:center;} */
       .bl       { border:windowtext 1pt;border-style:none none solid solid; text-align:center;}
       .blr      { border:windowtext 1pt;border-style:none solid solid solid;text-align:center;}
       .l        { border:windowtext 1pt;border-style:none none none solid;}
       .lr       { border:windowtext 1pt;border-style:none solid none solid;}
/*       .lt       { border:windowtext 1pt;border-style:solid none none solid; text-align:center}  */
       .ltr      { border:windowtext 1pt;border-style:solid solid none solid; text-align:center} 

       .main     {font-size:11pt; text-align:center;}
       .strike   {text-decoration: line-through;}
       .bltr     { border:windowtext 1pt;border-style:solid solid solid solid; padding-left:2mm; text-align:center}

       span      {font-size:11pt;}
       .small    {font-size:8pt;}
       .no_marg  {margin-left:8mm; margin-top:0; margin-Bottom:0; text-align:justify;}
       p.fl:first-letter
                 {text-transform: uppercase;}
       .nextpage { /*display:0;*/}
    }
    @media print
    {
       .line      { display:0;}
       .nextpage  { display:block; page-break-after:always; }
    }

</style>
<!--  <link rel="stylesheet" href="file:///c:\Astrasoft\AstraKBR\Schema_XML\MCI_UFEBS\style1.css"></link> -->
</head>

<body>
<div id="wrapper">
  <div id="content">
    <span>&#160;</span>
    <div align="right">
      <table border="1">
       <tr >
        <td style="width:40mm; border:solid windowtext 1pt; font-size:10pt">
          <p align="center" style="text-align:center">��� ����� ���������<br></br>�� ����</p>
        </td>
       </tr>
       <tr style="height:12.0pt">
        <td style="width:40mm; border:solid windowtext 1pt; border-top:0; font-size:10pt">
          <p style="text-align:center">0402108</p>
        </td>
       </tr>
      </table>
    </div>

    <div align="right">
      <table>
       <tr>
        <td style="width:60mm; padding-top:1mm;">
          <p class="main" ><xsl:value-of select="ed:Request/@NamePBR"/></p>
        </td>
       </tr>
       <tr>
        <td style="width:60mm;">
          <xsl:if test="ed:Request/@BicPBR !=''">
            <p class="main"><xsl:value-of select="concat('��� ',ed:Request/@BicPBR)"/></p>
          </xsl:if>
        </td>
       </tr>
       <tr>
        <td style="width:60mm;height:9mm; text-align:center;padding:0; border-top:solid windowtext 1pt;">
          <p align="center" >
            <span class="small">(������������ ���������� ����� ������,<br></br>��� (��� ��� �������)</span>
          </p>
        </td>
       </tr>
      </table>
    </div>

    <div align="center">
      <table>
       <tr>
        <td colspan="8">
          <p class="main"><strong>������ �� �����/��������� �������� ����������</strong></p>
        </td>
       </tr>
       <tr style="height:6mm; text-align:center; vertical-align:bottom; ">
        <td class="b_n" ><p class="main"><span>�� �</span></p></td>
        <td class="b" width="10mm">
          <p>
            <span>
              <xsl:call-template name="date_dd">
              <xsl:with-param name="d" select="ed:Request/@DocDate"/></xsl:call-template>
            </span>
          </p>
        </td>
        <td class="b_n" ><p class="main">�</p></td>
        <td class="b">
          <p class="main">
            <span  width="25mm">
              <xsl:call-template name="date_mm"><xsl:with-param name="d" select="ed:Request/@DocDate"/></xsl:call-template>
            </span>
          </p>
        </td>
        <td class="b_n"><p class="main">20</p></td>
        <td class="b" width="8mm">
          <p class="main">
            <span style="font-size:11pt;">
              <xsl:call-template name="date_yy"><xsl:with-param name="d" select="ed:Request/@DocDate"/></xsl:call-template>
            </span>
          </p>
        </td>
        <td class="b_n" ><p class="main">�.  �</p></td>
        <td class="b">
          <p class="main">
            <span style="width:20mm;"><xsl:value-of select="ed:Request/@DocNo"/></span>
          </p>
        </td>
       </tr>
      </table>
    </div>

    <div align="center">
      <table border="0">
        <tr><td><p>&#160;</p></td></tr>
        <tr>
          <td style="vertical-align:center;">
            <p><span style=""><xsl:value-of select="ed:Request/@NameClient"/></span></p>
          </td>
        </tr>
      </table>
    </div>
<!--
    <div align="center">
      <table border="1">
        <tr><td style=" border:windowtext 1pt; border-style:solid none none none;"><p>top</p></td></tr>
        <tr><td style=" border:windowtext 1pt; border-style:none solid none none;"><p>right</p></td></tr>
        <tr><td style=" border:windowtext 1pt; border-style:none none solid none;"><p>bottom</p></td></tr>
        <tr><td style=" border:windowtext 1pt; border-style:none none none solid;"><p>left</p></td></tr>
      </table>
    </div>
-->

    <div style="border:0;border-top:solid windowtext 1pt;padding:1.0pt 0cm 0cm 0cm;text-align:center">
      <span style="font:8pt/8pt 'Times New Roman'">(������ ��������� (�����������
    ��������� (��� ��� �������) ������������ ��������� �����������; ������
    ���������<br></br>(����������� ��������� (��� ��� �������) ������������ ��������� ����������� �
    ������������ �������; ������ ���������<br></br>
    (����������� ��������� (��� ��� �������) ������������ ��������� ����������� �
    ������������ � (���) ����� ���� ����<br></br>���������������� �������� (��� ����������
    ������������ � ������) ��������������� �����, ��������-��������� �����,<br></br>
    ������������� �����; ������ ��������� (����������� ��������� (��� ��� �������)
    ������������ ��������� �����������,<br></br>
    ������������ ������� � ������������ � (���) ����� ���� ���� ����������������
    �������� (��� ���������� ������������<br></br>
    � ������) ��������������� �����, ��������-��������� �����, ������������� �����;
    ������������ �����������)<br></br><br></br>&#160;</span>
    </div>

    <xsl:variable name="op" select="ed:Request/@OperationType"/>
<!--    <xsl:variable name="rub" select="concat('file:///',$ImageDirName,'ruble.gif')"/> -->

    <div>
      <table border="0"  style="width:175mm">
        <tr>
          <td class="b_n" style="width:55mm;">
              <span>�������� �&#160;</span>
              <xsl:if test="$op = 1">
                <span>�����/</span>
                <span class="strike">���������</span>
              </xsl:if>
              <xsl:if test="$op = 2">
                <span class="strike">�����</span>
                <span>/���������</span>
              </xsl:if>
          </td>
          <td  class="b" style="width:80mm;">
            <p class="main">
              <span ><xsl:call-template name="fdate">
                <xsl:with-param name="d" select="ed:Request/@OperationDate"/></xsl:call-template>
              </span>
            </p>
          </td>
          <td class="b_n" style="width:40mm">
            <p class="main"><span style="text-align:center">�������� ����������</span></p>
          </td>
        </tr>
        <tr>
          <td><p class="main"><span class="small">(�������� ����������)</span></p></td>
          <td><p class="main"><span class="small">(����)</span></p></td>
          <td><p class="main">&#160;</p></td>
        </tr>
      </table>
    </div>

    <div>
      <table border="0" style="width:175mm">
        <tr>
          <td class="b_n" style="width:85mm;">
              <span>���&#160;</span>
              <xsl:if test="$op = 1">
                <span>���������� �� ���� �/</span>
                <span class="strike">�������� �� ����� �&#160;</span>
              </xsl:if>
              <xsl:if test="$op = 2">
                <span class="strike">���������� �� ���� �</span>
                <span>/�������� �� ����� �&#160;</span>
              </xsl:if>
              <sup><small>1</small></sup>
          </td>
          <td  class="b" style="width:90mm;">
            <p class="main"><span ><xsl:value-of select="ed:Request/@Acc"/></span></p>
          </td>
        </tr>
        <tr>
          <td ><p class="main"><span class="small">(�������� ����������)</span></p></td>
          <td><p class="main">&#160;</p></td>
        </tr>
      </table>
    </div>

    <div>
      <table style="width:175mm">
        <tr>
          <td style="width:15mm"><p>� �����</p></td>
          <td style="width:35mm">
            <p class="main">
              <span style="vertical-align:center;">
                <xsl:for-each select="ed:Request/@Sum">
                  <xsl:call-template name="summa">
                    <xsl:with-param name="cep" select="string('-')"/>
                  </xsl:call-template>
                </xsl:for-each>
              </span>
              <span>&#160;
      <!--        <img src="file:///c:\Astrasoft\AstraKBR\Schema_XML\MCI_UFEBS\Images\Ruble.gif" alt="rub"></img> -->
                <img src="%rub%" alt="���."></img>
              </span>
            </p>
          </td>
          <td style="width:130mm">
            <p class="fl">
              <span >
                <xsl:for-each select="ed:Request/@Sum">
                  <xsl:call-template name="RublesToText">
                    <xsl:with-param name="sum" select="."/>
                  </xsl:call-template>
                </xsl:for-each>
              </span>
            </p>
          </td>
        </tr>
        <tr>
          <td></td>
          <td class="t"></td>
          <td class="t" colspan="2">
            <p class="main">
              <span class="small" style="vertical-align:top">(������� � ��������)</span>
            </p>
          </td>
        </tr>
      </table>
      <span>� ��������� ���������&#160;<sup><small>2</small></sup>:</span>
    </div>

    <div align="center">
      <table border="1" style="width:110mm; margin-top:2mm; border:solid windowtext 1pt;">
        <tr>
          <td class="r" style="width:55mm; text-align:center">
              <span>������� ������� (������)</span>
          </td>
          <td style="width:55mm;text-align:center">
              <span>����� (���., ���.)</span>
          </td>
        </tr>
        <tr><td class="ltr" colspan="2" style="padding-left:2mm; text-align:left"><span>��������</span></td></tr>
        <xsl:if test="count(ed:Cash[@CashType=1]) = 0">
          <tr><td class="tr" style="text-align:center"><span>-</span></td><td class="t" style="text-align:center"><span>-</span></td></tr>
          <tr><td class="tr" style="text-align:center"><span>-</span></td><td class="t" style="text-align:center"><span>-</span></td></tr>
        </xsl:if>
        <xsl:if test="count(ed:Cash[@CashType=1]) > 0">
          <xsl:for-each select="ed:Cash[@CashType=1]">
          <tr>
            <td class="tr" style="padding-left:2mm; text-align:left">
              <span>
              <xsl:for-each select="@Nominal">
                <xsl:call-template name="summa">
                  <xsl:with-param name="cep" select="string('-')"/>
                  <xsl:with-param name="rub_only" select="1"/>
                </xsl:call-template>
              </xsl:for-each>
              </span>
            </td>
            <td  class="t" style="padding-right:15mm; text-align:right">
              <span>
              <xsl:for-each select="@Sum">
                <xsl:call-template name="summa"><xsl:with-param name="cep" select="string('-')"/></xsl:call-template>
              </xsl:for-each>
              </span>
            </td>
          </tr>
          </xsl:for-each>
        </xsl:if>
        <tr><td class="ltr" colspan="2" style="padding-left:2mm; text-align:left"><span>������</span></td></tr>
        <xsl:if test="count(ed:Cash[@CashType=0]) = 0">
          <tr><td class="tr" style="text-align:center"><span>-</span></td><td class="t" style="text-align:center"><span>-</span></td></tr>
          <tr><td class="tr" style="text-align:center"><span>-</span></td><td class="t" style="text-align:center"><span>-</span></td></tr>
        </xsl:if>
        <xsl:if test="count(ed:Cash[@CashType=0]) > 0">
          <xsl:for-each select="ed:Cash[@CashType=0]">
          <tr>
            <td class="tr" style="padding-left:2mm; text-align:left">
              <span>
              <xsl:for-each select="@Nominal">
                <xsl:call-template name="summa">
                  <xsl:with-param name="cep" select="string('-')"/>
                  <xsl:with-param name="rub_only" select="1"/>
                </xsl:call-template>
              </xsl:for-each>
              </span>
            </td>
            <td class="t" style="padding-right:15mm; text-align:right">
              <span>
              <xsl:for-each select="@Sum">
                <xsl:call-template name="summa"><xsl:with-param name="cep" select="string('-')"/></xsl:call-template>
              </xsl:for-each>
              </span>
            </td>
          </tr>
          </xsl:for-each>
        </xsl:if>
      </table>
    </div>

    <div>
      <span>� ��� ����� �� ���������� �����������/������������ �����&#160;<sup><small>1</small></sup>:</span>
    </div>

    <div align="center">
      <table border="1" style="width:110mm; margin-top:2mm; border:solid windowtext 1pt;">
        <tr>
          <td class="r" style="width:55mm; text-align:center">
              <span>�������� �����������/<br></br>����������� ������</span>
          </td>
          <td style="width:55mm; text-align:center">
              <span>����� (���., ���.)</span>
          </td>
        </tr>
        <xsl:for-each select="ed:CashInfo">
          <tr>
            <td class="tr" style="padding-left:2mm; text-align:left">
              <span><xsl:value-of select="@CashCode"/></span>
            </td>
            <td  class="t" style="padding-right:15mm; text-align:right">
              <span>
                <xsl:for-each select="@CashSum">
                  <xsl:call-template name="summa"><xsl:with-param name="cep" select="string('-')"/></xsl:call-template>
                </xsl:for-each>
              </span>
            </td>
          </tr>
        </xsl:for-each>
        <xsl:if test="count(ed:CashInfo) &lt; 2">
          <tr><td class="tr"><span>&#160;</span></td><td class="t"><span>&#160;</span></td></tr>
        </xsl:if>
      </table>
    </div>

    <div>
      <table style="width:175mm; margin-top:2mm">
        <tr>
          <td class="b_n" style="width:85mm;">
              <xsl:if test="$op = 1">
                <span>���������/</span>
                <span class="strike">����������</span>
                <span>&#160;�������� ����������&#160;</span>
              </xsl:if>
              <xsl:if test="$op = 2">
                <span class="strike">���������</span>
                <span>/���������� �������� ����������&#160;</span>
              </xsl:if>
              <sup><small>1</small></sup>
          </td>
          <td  class="b" style="width:90mm;">
            <p class="main"><span ><xsl:value-of select="ed:CustomerData/@CustomerName"/></span></p>
          </td>
        </tr>
        <tr>
          <td ><p class="main"><span class="small">(�������� ����������)</span></p></td>
          <td ><p class="main"><span class="small">(�������, ���, �������� (��� �������)</span></p></td>
        </tr>
      </table>
    </div>

    <div>
      <p>
         <span>����������&#160;<sup>3</sup>:&#160;<xsl:value-of select="ed:Annotation"/></span>
      </p>
    </div>

    <div>
      <table style="width:175mm; margin-top:2mm">
        <xsl:for-each select="ed:CreatorInfo">
        <tr>
          <td class="b" style="width:70mm;">
            <p class="main"><span><xsl:value-of select="@Position"/></span></p>
          </td>
          <td style="width:5mm;"><span>&#160;</span></td>
          <td class="b" style="width:50mm;"><span>&#160;</span></td>
          <td style="width:5mm;"><span>&#160;</span></td>
          <td class="b" style="width:45mm;">
            <p class="main"><span><xsl:value-of select="@FIO"/></span></p>
          </td>
          <td>
            <xsl:if test="position()=last()">
              <sup><small>4</small></sup>
            </xsl:if>
          </td>
        </tr>
        <tr>
          <td><p class="main"><span class="small" style="vertical-align:top">(������������ ���������)</span></p></td>
          <td><span>&#160;</span></td>
          <td><p class="main"><span class="small" style="vertical-align:top">(������ �������)</span></p></td>
          <td><span>&#160;</span></td>
          <td><p class="main"><span class="small" style="vertical-align:top">(������� � ��������)</span></p></td>
          <td></td>
        </tr>
        </xsl:for-each>
      </table>
    </div>
  </div>

  <div id="footer">

    <hr size="1" align="left" width="33%"/>
    <p class="no_marg">
      <span class="small"><sup>1</sup>&#160;����������� � ������ ���������� ������ �� �����/��������� �������� ���������� 0402108 � ���� ������������ ���������.</span>
    </p>
    <p class="no_marg">
      <span class="small"><sup>2</sup>&#160;����� �� ����������� ��� ����� �������� ����������.</span>
    </p>
    <p class="no_marg">
      <span class="small"><sup>3</sup>&#160;����������� ����������, ����������� ��� ��������� �����������, ������������� ��������� �����������, ����������� (��� ������� ����� ����������).</span>
    </p>
    <p class="no_marg">
      <span class="small"><sup>4</sup>&#160;�� ����������� � ������ ���������� ������ �� �����/��������� �������� ���������� 0402108 �� �������� ��������.</span>
    </p>
  </div>
</div>

</body>
</html>

 </xsl:template>
 <xsl:template name="next">
<html>
<body>
   <div class="line"></div>
   <p class="nextpage"></p>
</body>
</html>
 </xsl:template>
</xsl:stylesheet>
