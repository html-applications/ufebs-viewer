<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED425  -->
  <xsl:template match="ed:ED425">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>��������� �� � ������ ������ �� ������� � ��������� ��������/&#13;&#10;</xsl:text>
      <xsl:text>    ��������� �� ��������� ������� �� �� ������������� ���������� ������ /ED425/&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>����� ����������� ��                      : </xsl:text><xsl:value-of select="@EDTime"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>����� ��������� � ������ ������/��������� : </xsl:text><xsl:value-of select="@UndoReqNum"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>���� � ����� ����������� ���������        : </xsl:text><xsl:value-of select="@UndoReqDateTime"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>�������� � ����� ������������ ������/��������� &#13;&#10;</xsl:text>
      <xsl:text>=================================================&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:CreditRequestInfo">
        <xsl:text>����� ���������� ������/���������             : </xsl:text><xsl:value-of select="@ReqNum"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>���� � ����� ����.���������� ������/��������� : </xsl:text><xsl:value-of select="@ReqDateTime"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="@CreditConsDate">
          <xsl:text>���� ������������ ���������� ��������         : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>����� ������������ ���������� ��������        : </xsl:text><xsl:value-of select="@CreditConsNo"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="@CreditOpDate">
          <xsl:text>���� ���������� ���������� �������� ���       :&#13;&#10;</xsl:text>
          <xsl:text>��������� �������� �� ����.���������� ������  : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>

        <xsl:text>������ ���������� ��������� ��������          : </xsl:text><xsl:value-of select="@CreditOpMethod"/>
        <xsl:if test="@CreditOpMethod = 0"><xsl:text> (����.�������� �� ����.������, ������ ���������� � ������������)</xsl:text></xsl:if>
        <xsl:if test="@CreditOpMethod = 1"><xsl:text> (����.�������� �� ����.������, ������ ���������� � �����������)</xsl:text></xsl:if>
        <xsl:if test="@CreditOpMethod = 2"><xsl:text> (����.�������� �� ����.������, �������� ��������� �������)</xsl:text></xsl:if>
        <xsl:if test="@CreditOpMethod = 3"><xsl:text> (����.�������� �� ������������� ���������� ������)</xsl:text></xsl:if>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:for-each select="@GrantDate">
          <xsl:text>���� �������������� ������� ����� ������      : </xsl:text><xsl:call-template name="date"/>
        </xsl:for-each>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>����, �� ���.��������������� ������(� ����)   : </xsl:text><xsl:value-of select="@ReqCredTerm"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>����� �������                                 : </xsl:text>
        <xsl:for-each select="@ApplicationSum"><xsl:call-template name="ffsum"/></xsl:for-each>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>���������� ������ �� �������                  : </xsl:text><xsl:value-of select="@WideApplicationRate"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>��� ����� �������� ��������� ����� ��         : </xsl:text><xsl:value-of select="@BIC"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>����� ��������� ����� ��                      : </xsl:text><xsl:value-of select="@CorrAcc"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>��������������� ����� ��                      : </xsl:text><xsl:value-of select="@OrgRegNum"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>����������� ��������� ������������ ��         : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="ed:OrgName"/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                              : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>