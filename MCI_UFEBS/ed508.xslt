<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED508  -->
  <xsl:template match="ed:ED508">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>���������� � ��������� �� /ED508/&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:for-each select="@SenderSWIFTBIC">
        <xsl:text>��� SWIFT �����-�����������  : </xsl:text><xsl:value-of select="."/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:if test="ed:EDRefID/@EDNo != ''">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:apply-templates select="ed:EDRefID"/>
      </xsl:if>
      <xsl:if test="count(ed:SWIFTDocCtrlInfo) !=0">
        <xsl:text>--------------------------------------------------------------------------------</xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>�������������  ��� ���-��          �����                 &#13;&#10;</xsl:text>
        <xsl:text>���������      ��������          ���������               &#13;&#10;</xsl:text>
        <xsl:text>--------------------------------------------------------------------------------</xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:apply-templates select="ed:SWIFTDocCtrlInfo"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
  </xsl:template>

  <!--  SWIFTDocCtrlInfo  -->
  <xsl:template match="ed:SWIFTDocCtrlInfo">
      <xsl:call-template name="Pad">
        <xsl:with-param name="input" select="@TerminalSessionNum"/>
        <xsl:with-param name="n" select="13"/>
      </xsl:call-template>

      <xsl:choose>
        <xsl:when test="@SWIFTCtrlCode">
          <xsl:call-template name="Center">
            <xsl:with-param name="input" select="@SWIFTCtrlCode"/>
            <xsl:with-param name="n" select="14"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise><xsl:value-of select="string('              ')"/></xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="ed:Annotation">
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="."/>
            <xsl:with-param name="width" select="50"/>
            <xsl:with-param name="before" select="string('                            ')"/>
          </xsl:call-template>
      </xsl:when>
      <xsl:otherwise><xsl:text>&#13;&#10;</xsl:text></xsl:otherwise>
      </xsl:choose>

      <xsl:if test="count(ed:SWIFTErrCodeList) !=0">
        <xsl:text>���� ������ SWIFT :&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:SWIFTErrCodeList">
          <xsl:value-of select="concat(@SWIFTErrCode,string(' '))"/>
          <xsl:for-each select="ed:SWIFTErrAnnotation">
            <xsl:call-template name="text_wrapper">
              <xsl:with-param name="text" select="."/>
              <xsl:with-param name="width" select="85"/>
              <xsl:with-param name="before" select="string('   ')"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:text>--------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>


</xsl:stylesheet>