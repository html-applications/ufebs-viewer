<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED462  -->
  <xsl:template match="ed:ED462">
      <xsl:for-each select="ed:Request">
        <xsl:text>                                                                                      ______________________&#13;&#10;</xsl:text>
        <xsl:text>                                                                                      | ��� ����� ��������� |&#13;&#10;</xsl:text>
        <xsl:text>                                                                                      |    �� ����          |&#13;&#10;</xsl:text>
        <xsl:text>                                                                                      |_____________________|&#13;&#10;</xsl:text>
        <xsl:text>                                                                                      |    0402108          |&#13;&#10;</xsl:text>
        <xsl:text>                                                                                      |_____________________|&#13;&#10;</xsl:text>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>                                                                              </xsl:text>
        <xsl:call-template name="wrapper">
          <xsl:with-param name="text" select="@NamePBR"/>
          <xsl:with-param name="width" select="30"/>
          <xsl:with-param name="before" select="string('                                                                              ')"/>
        </xsl:call-template>
        <xsl:if test="@BicPBR !=''">
          <xsl:text>                                                                              ��� </xsl:text><xsl:value-of select="@BicPBR"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
        <xsl:text>                                                                             ____________________________________&#13;&#10;</xsl:text>
        <xsl:text>                                                                                 (������������ ���������� &#13;&#10;</xsl:text>
        <xsl:text>                                                                                    ����� ������, ���)&#13;&#10;&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:if test="@OperationType = 1">                         ������ �� ����� �������� ����������&#13;&#10;</xsl:if>
        <xsl:if test="@OperationType = 2">                         ������ �� ��������� �������� ����������&#13;&#10;</xsl:if>
        <xsl:value-of select="string('                                    �� ')"/>
        <xsl:for-each select="@DocDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:value-of select="concat('    �  ',@DocNo)"/>
        <xsl:text>&#13;&#10;&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>                        </xsl:text>
        <xsl:call-template name="wrapper">
          <xsl:with-param name="text" select="@NameClient"/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                ')"/>
        </xsl:call-template>
<!--        <xsl:if test="@OrgBIC !=''">
          <xsl:text>                                        ��� </xsl:text><xsl:value-of select="@OrgBIC"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
-->
        <xsl:text>                        _____________________________________________________&#13;&#10;</xsl:text>
        <xsl:text>                               (������������ ��������� �����������)&#13;&#10;&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:if test="@OperationType = 1">          �������� � �����          </xsl:if>
        <xsl:if test="@OperationType = 2">          �������� � ���������      </xsl:if>
        <xsl:for-each select="@OperationDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:text>       �������� ����������&#13;&#10;</xsl:text>
        <xsl:if test="@OperationType = 1">          ��� ���������� �� ���� � </xsl:if>
        <xsl:if test="@OperationType = 2">          ��� �������� �� ����� � </xsl:if>
        <xsl:value-of select="@Acc"/>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>          � ����� </xsl:text>
        <xsl:for-each select="@Sum">
          <xsl:call-template name="ffsum">
            <xsl:with-param name="cep" select="string('.')"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
<!--          <xsl:text> (</xsl:text>  -->
        <xsl:text>               </xsl:text>
          <xsl:call-template name="RublesToText">
            <xsl:with-param name="sum" select="."/>
          </xsl:call-template>
<!--          <xsl:text>)&#13;&#10;</xsl:text>  -->
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:text>          � ��������� ��������� :&#13;&#10;</xsl:text>
      <xsl:text>               _____________________________________________________________&#13;&#10;</xsl:text>
      <xsl:text>               |������� ������� (������)     |  ����� (���., ���.)          |&#13;&#10;</xsl:text>
      <xsl:if test="count(ed:Cash[@CashType=1]) > 0">
        <xsl:text>               |____________________________________________________________|&#13;&#10;</xsl:text>
        <xsl:value-of select="string('               |��������                                                    |')"/>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:Cash[@CashType=1]">
          <xsl:text>               |____________________________________________________________|&#13;&#10;</xsl:text>
          <xsl:text>               |  </xsl:text>
          <xsl:for-each select="@Nominal">
            <xsl:call-template name="fsum"><xsl:with-param name="cep" select="string('.')"/></xsl:call-template>
          </xsl:for-each>
          <xsl:text>         |  </xsl:text>
          <xsl:for-each select="@Sum">
            <xsl:call-template name="fsum"><xsl:with-param name="cep" select="string('.')"/></xsl:call-template>
          </xsl:for-each>
          <xsl:text>          |&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="count(ed:Cash[@CashType=0]) > 0">
        <xsl:text>               |____________________________________________________________|&#13;&#10;</xsl:text>
        <xsl:value-of select="string('               |������                                                      |')"/>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:Cash[@CashType=0]">
          <xsl:text>               |____________________________________________________________|&#13;&#10;</xsl:text>
          <xsl:text>               |  </xsl:text>
          <xsl:for-each select="@Nominal">
            <xsl:call-template name="fsum"><xsl:with-param name="cep" select="string('.')"/></xsl:call-template>
          </xsl:for-each>
          <xsl:text>         |  </xsl:text>
          <xsl:for-each select="@Sum">
            <xsl:call-template name="fsum"><xsl:with-param name="cep" select="string('.')"/></xsl:call-template>
          </xsl:for-each>
          <xsl:text>          |&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:if>

      <xsl:text>               |____________________________________________________________|&#13;&#10;&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>          � ��� ����� �� ���������� �����������/������������ ����� :&#13;&#10;</xsl:text>
      <xsl:text>               _____________________________________________________________&#13;&#10;</xsl:text>
      <xsl:text>               |�������� �����������/        |  ����� (���., ���.)          |&#13;&#10;</xsl:text>
      <xsl:text>               |����������� ������           |                              |&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:CashInfo">
        <xsl:text>               |_____________________________|______________________________|&#13;&#10;</xsl:text>
        <xsl:value-of select="concat('               |            ',@CashCode,'               |  ')"/>
        <xsl:for-each select="@CashSum">
          <xsl:call-template name="fsum"><xsl:with-param name="cep" select="string('.')"/></xsl:call-template>
        </xsl:for-each>
        <xsl:text>          |&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>               |____________________________________________________________|&#13;&#10;&#13;&#10;</xsl:text>

      <xsl:if test="ed:Request/@OperationType = 1">          ��������� �������� ����������   : </xsl:if>
      <xsl:if test="ed:Request/@OperationType = 2">          ���������� �������� ����������  : </xsl:if>
      <xsl:call-template name="wrapper">
        <xsl:with-param name="text" select="ed:CustomerData/@CustomerName"/>
        <xsl:with-param name="width" select="50"/>
        <xsl:with-param name="before" select="string('                                            ')"/>
      </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:Annotation">
        <xsl:text>          ���������� : </xsl:text>
        <xsl:call-template name="wrapper">
          <xsl:with-param name="text" select="."/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                       ')"/>
        </xsl:call-template>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:CreatorInfo">
        <xsl:value-of select="string('          ')"/>
        <xsl:call-template name="wrapper">
          <xsl:with-param name="text" select="@Position"/>
          <xsl:with-param name="width" select="26"/>
          <xsl:with-param name="before" select="string('          ')"/>
          <xsl:with-param name="after" select="string('')"/>
          <xsl:with-param name="align" select="1" />
        </xsl:call-template>
        <xsl:text>                           </xsl:text>
        <xsl:call-template name="Center">
            <xsl:with-param name="input" select="@FIO"/>
            <xsl:with-param name="n" select="26"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>          __________________________     _______________________     _________________________&#13;&#10;</xsl:text>
        <xsl:text>          (������������ ���������)            (������ �������)           (������� � ��������)&#13;&#10;&#13;&#10;</xsl:text>
      </xsl:for-each>

  </xsl:template>

  <!--  ������ ��� ������������ ����� ��������   -->
  <xsl:template name="RublesToText">
    <xsl:param name="sum" select="current()"/>
    <xsl:choose>
      <xsl:when test="string-length($sum) >2">
        <xsl:call-template name="SumInWords">
          <xsl:with-param name="rub" select="substring($sum,1,string-length($sum)-2)" />
          <xsl:with-param name="kop" select="substring($sum,string-length($sum)-1,2)" />
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="string-length($sum) >1">
        <xsl:call-template name="SumInWords">
          <xsl:with-param name="rub" select="'0'" />
          <xsl:with-param name="kop" select="$sum" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="SumInWords">
          <xsl:with-param name="rub" select="'0'" />
          <xsl:with-param name="kop" select="concat('0',$sum)" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--  ������ ��� ��������� �����.   -->
  <xsl:template name="wrapper">
    <xsl:param name="text" />
    <xsl:param name="width" select="70" />
    <xsl:param name="pos" select="0" />
    <xsl:param name="before" select="string('                           : ')"/>
    <xsl:param name="after" select="string('&#13;&#10;')"/>
    <xsl:param name="align" select="0" />

    <!--  ������ ������. �����=80   -->
    <xsl:variable name="blnk80" select="string('                                                                                ')"/>

    <xsl:choose>

      <xsl:when test="contains( $text, ' ' )">
        <xsl:variable name="first-word" select="substring-before( $text, ' ' )" />
        <xsl:variable name="pos-now" select="$pos + 1 + string-length( $first-word )" />
        <xsl:choose>

          <xsl:when test="$pos > 0 and $pos-now >= $width">
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:value-of select="$before" />
            <xsl:call-template name="wrapper">
              <xsl:with-param name="text" select="$text" />
              <xsl:with-param name="width" select="$width" />
              <xsl:with-param name="pos" select="0" />
              <xsl:with-param name="before" select="$before"/>
              <xsl:with-param name="after" select="$after"/>
              <xsl:with-param name="align" select="$align"/>

            </xsl:call-template>
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="$first-word" />
            <xsl:text> </xsl:text>
            <xsl:call-template name="wrapper">
              <xsl:with-param name="text" select="substring-after( $text, ' ' )" />
              <xsl:with-param name="width" select="$width" />
              <xsl:with-param name="pos" select="$pos-now" />
              <xsl:with-param name="before" select="$before"/>
              <xsl:with-param name="after" select="$after"/>
              <xsl:with-param name="align" select="$align"/>
            </xsl:call-template>
          </xsl:otherwise>

        </xsl:choose>
      </xsl:when>

      <xsl:otherwise>
        <xsl:if test="$pos + string-length( $text ) >= $width">
          <xsl:if test="$pos > 0">
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:value-of select="$before" />
          </xsl:if>
        </xsl:if>
        <xsl:value-of select="$text" />
        <xsl:if test="$align != 0">
<!--          <xsl:value-of select="concat(' ***** ',$width - $pos - string-length($text))" /> -->
          <xsl:value-of select="substring($blnk80,1,$width - $pos - string-length($text))"/>
        </xsl:if>
        <xsl:value-of select="$after" />
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
