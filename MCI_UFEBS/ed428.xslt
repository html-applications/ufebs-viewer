<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED428  -->
  <xsl:template match="ed:ED428">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>����������� � ��������� ��������� ������������ �� ������� ����� ������   /ED428/ </xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:RepaymentPlan">
        <xsl:text>����� ���������                           : </xsl:text><xsl:value-of select="@ReqNum"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>���� � ����� ����������� ���������        : </xsl:text><xsl:value-of select="@ReqDateTime"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="@CreditConsDate">
          <xsl:text>���� ������������ ���������� ��������     : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>����� ������������ ���������� ��������    : </xsl:text><xsl:value-of select="@CreditConsNo"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>��������������� ����� ����.�����������    : </xsl:text><xsl:value-of select="@OrgRegNum"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>������������ ����.�����������             : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="@OrgName"/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                          : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>����� ������� ����� ������                : </xsl:text><xsl:value-of select="@CreditNum"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="@CreditDate">
          <xsl:text>���� �������������� ������� ����� ������  : </xsl:text>
          <xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@ReturnDate">
          <xsl:text>����������� ���� ��������� ������������   : </xsl:text>
          <xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@ReturnCreditSum">
          <xsl:text>����� �����, ����������� � ���������      : </xsl:text>
          <xsl:call-template name="ffsum"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@ReturnPercentSum">
          <xsl:text>����� ���������, ����������� � ���������  : </xsl:text>
          <xsl:call-template name="ffsum"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>