<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED201  -->
  <xsl:template match="ed:ED201">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:choose>
      <xsl:when test="position() =1">
        <xsl:value-of select="concat(position(),'.  ')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      </xsl:otherwise>
      </xsl:choose>

      <xsl:text>��������� � ����������� �������� �� (������ ��)/ED201/</xsl:text>
      <xsl:call-template name="PriznGr"/>

      <xsl:text>����� ���������� ��������    : </xsl:text><xsl:value-of select="@CtrlTime"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��� ���������� ��������      : </xsl:text><xsl:value-of select="@CtrlCode"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>����� ���������              : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="ed:Annotation"/>
        <xsl:with-param name="width" select="60"/>
        <xsl:with-param name="before" select="string('                             : ')"/>
      </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:if test="ed:ErrorDiagnostic != ''">
        <xsl:text>��������� ����������� ������ : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="ed:ErrorDiagnostic"/>
          <xsl:with-param name="width" select="60"/>
          <xsl:with-param name="before" select="string('                             : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="ed:MsgID != ''">
          <xsl:text>������������ ������������� ��������� : </xsl:text><xsl:value-of select="ed:MsgID"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="ed:EDRefID/@EDNo != ''">
          <xsl:apply-templates select="ed:EDRefID"/>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>