<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED810  -->
  <xsl:template match="ed:ED810">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>������ �� ���������� ������������ /ED810/&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="SystemCode">
        <xsl:with-param name="str" select="'������� ���������                 : '"/>
      </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>��� ��������� ��� ������. ���     : </xsl:text><xsl:value-of select="@BIC"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>���� ���������                    : </xsl:text><xsl:value-of select="@Acc"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="@LimitChangeType">
        <xsl:call-template name="LimitChangeType">
          <xsl:with-param name="str" select="'��� ��������� ������              : '"/>
        </xsl:call-template>
      </xsl:for-each>
      <xsl:for-each select="@LimitActivationDate">
        <xsl:text>���� ���������� � �������� ������ : </xsl:text>
        <xsl:call-template name="date"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:LimitInfo">
        <xsl:text>�������� � ������������� ������&#13;&#10;</xsl:text>
        <xsl:text>================================&#13;&#10;</xsl:text>
        <xsl:for-each select="@LimitType"><xsl:call-template name="LimitType"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@Value">
          <xsl:text>��������������� �������� ������   : </xsl:text><xsl:call-template name="ffsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@BIC">
          <xsl:text>���                               : </xsl:text><xsl:value-of select="."/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@Account">
          <xsl:text>���� ������������ ��������� ����  : </xsl:text><xsl:value-of select="."/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="@UID">
          <xsl:text>��� ����������� ����������        : </xsl:text><xsl:value-of select="."/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>

      <xsl:if test="ed:EDRefID/@EDNo != ''">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:apply-templates select="ed:EDRefID"/>
      </xsl:if>
  </xsl:template>

</xsl:stylesheet>