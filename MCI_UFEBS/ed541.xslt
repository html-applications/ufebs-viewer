<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED541  -->
  <xsl:template match="ed:ED541">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>���������� � ����������/���������� �� /ED541/&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>

      <xsl:choose> <xsl:when test="ed:PartInfo">
          <xsl:apply-templates select="ed:PartInfo"/>
      </xsl:when> </xsl:choose>

      <xsl:if test="ed:InitialED/@EDNo != ''">
          <xsl:apply-templates select="ed:InitialED"/>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text> ������ ����������� ��������� </xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text> ----------------------------- </xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:EDRequisiteList">
          <xsl:sort order="ascending" select="@EDProcessingTime"/>
          
          <xsl:text>� </xsl:text><xsl:value-of select="@EDNo"/>
          <xsl:text> �� </xsl:text>
          <xsl:for-each select="@EDDate"><xsl:call-template name="date"/></xsl:for-each>
          <xsl:text>  ��� : </xsl:text><xsl:value-of select="@EDAuthor"/>
          <xsl:text>  ��� ��:  </xsl:text><xsl:value-of select="@EDTypeNo"/>
          <xsl:text>&#13;&#10;</xsl:text>

          <xsl:if test="@TransportFileName != ''">
              <xsl:text>��� ����� :  </xsl:text>
              <xsl:for-each select="@TransportFileName"><xsl:value-of select="."/></xsl:for-each>
              <xsl:text>&#13;&#10;</xsl:text>
          </xsl:if>
          <xsl:text>����� ������/�������� ��������� :  </xsl:text><xsl:value-of select="@EDProcessingTime"/>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:if test="ed:EDRefID/@EDNo != ''">
              <xsl:text>&#13;&#10;</xsl:text>
              <xsl:apply-templates select="ed:EDRefID"/>
          </xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
  </xsl:template>


</xsl:stylesheet>