<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="html" encoding="WINDOWS-1251"/>

 <!--  ����� �������� ����� ���������� ���������  -->
 <!-- ======================================================================= -->
 <!-- ED114 -->
 <xsl:template match="ed:ED114">
	<p style="font-size:3mm;">������������ �� ������ ���������� ��������� /ED114/ : <span style="font-size:3mm;">
	  <xsl:call-template name="PriznGr"><xsl:with-param name="CrLfBefore" select="0"/></xsl:call-template></span>
	</p>
 <table style="width:180mm; height:9mm;">
	<tr style="height:4.5mm">
		<td style="width:35mm;">&#160;</td>
		<td style="width:13mm;">&#160;</td>
		<td style="width:35mm;">&#160;</td>
		<td style="width:80mm;">&#160;</td>
		<td style="width:14mm; border-style:solid; text-align:center; font-size:2.5mm;">0401071</td>
	</tr>
	<tr style="height:4.5mm">
		<td class="t" style="width:35mm; text-align:center; vertical-align:top; font-size:2.5mm;">������. � ���� ����.</td>
		<td style="width:13mm">&#160;</td>
		<td class="t" style="width:35mm; text-align:center; vertical-align:top; font-size:2.5mm;">������� �� ��. ����.</td>
		<td style="width:80mm">&#160;</td>
		<td style="width:14mm">&#160;</td>
	</tr>
 </table>
 <table style="width:180mm; height:15mm;">
	<tr style="height:7mm;">
		<td style="width:87mm; text-align:left; font-size:4.0mm">���������� ��������� � <span><xsl:value-of select="ed:AccDoc/@AccDocNo"/></span></td>
		<td style="width:35mm; text-align:center; vertical-align:bottom;">
		  <span>
			<xsl:call-template name="date"><xsl:with-param name="d" select="ed:AccDoc/@AccDocDate"/></xsl:call-template>
		  </span>
		</td>
		<td style="width:5mm">&#160;</td>
		<td style="width:35mm; text-align:center; vertical-align:bottom;">&#160;</td>
		<td style="width:8mm">&#160;</td>
		<td style="width:6mm; border-style:solid; text-align:center; vertical-align:center;">
		  <span><xsl:value-of select="ed:DepartmentalInfo/@DrawerStatus"/></span>
		</td>
	</tr>
	<tr style="height:3mm">
		<td style="width:87mm">&#160;</td>
		<td class="t" style="width:35mm; text-align:center; vertical-align:top; font-size:2.5mm;">����</td>
		<td style="width:5mm">&#160;</td>
		<td class="t" style="width:35mm; text-align:center; vertical-align:top; font-size:2.5mm;">��� �������</td>
		<td style="width:8mm">&#160;</td>
		<td style="width:6mm">&#160;</td>
	</tr>
 </table>
 <table style="width:180mm; height:15mm" border="1">
	<tr>
		<td class="b" style="width:20mm;  vertical-align:center;">�����&#xD;&#xA;��������</td>
		<td class="bl" style="width:160mm; vertical-align:center;">
		  <p class="fl" style="font-size:3.7mm; margin-left:1mm;">
			<xsl:call-template name="RublesToText">
			  <xsl:with-param name="sum" select="@Sum"/>
			</xsl:call-template>
		  </p>
		</td>
	</tr>
 </table>

 <table style="width:180mm;" border="1">
	<tr>
		<td class="br" style="width:50mm; align:left; font-size:3.7mm;">���&#160;<span><xsl:value-of select="ed:Payer/@INN"/></span></td>
		<td class="b"  style="width:50mm;  align:left; font-size:3.7mm;">���&#160;<span><xsl:value-of select="ed:Payer/@KPP"/></span></td>
		<td class="l"  style="width:15mm;  align:left; ">�����</td>
		<td class="l"  style="width:65mm;" colspan="3">
		  <span>
		    <xsl:call-template name="sum">
		      <xsl:with-param name="s" select="@Sum"/>
		      <xsl:with-param name="cep" select="string('-')"/>
		</xsl:call-template>
		</span>
		</td>
	</tr>

	<tr >
		<td style="width:100mm;" rowspan="4" colspan="2">
		  <span><xsl:value-of select="ed:Payer/ed:Name"/></span>
		</td>
		<td class="l" style="width:15mm;">&#160;</td>
		<td class="l" style="width:65mm;" colspan="3">&#160;</td>
	</tr>
	<tr >
		<td class="bl" style="width:15mm;">&#160;</td>
		<td class="bl" style="width:65mm;" colspan="3">&#160;</td>
	</tr>
	<tr >
		<td class="l" style="width:15mm;">��.�</td>
		<td class="l" style="width:65mm;" colspan="3">
		  <span><xsl:value-of select="ed:Payer/@PersonalAcc"/></span>
		</td>
	</tr>
	<tr >
		<td class="l" style="width:15mm;">&#160;</td>
		<td class="l" style="width:65mm;" colspan="3">&#160;</td>
	</tr>

	<tr >
		<td class="b"  style="width:100mm; align:left" colspan="2">����������</td>
		<td class="bl" style="width:15mm;">&#160;</td>
		<td class="l"  style="width:65mm;" colspan="3">&#160;</td>
	</tr>
	<tr >
		<td style="width:100mm;" rowspan="2" colspan="2">
		  <span>
			<xsl:call-template name="NameFromBic20">
			  <xsl:with-param name="bic" select="ed:Payer/ed:Bank/@BIC"/>
			</xsl:call-template>
		  </span>
		</td>
		<td class="bl" style="width:15mm;">���</td>
		<td class="l"  style="width:65mm;" colspan="3">
		  <span><xsl:value-of select="ed:Payer/ed:Bank/@BIC"/></span>
		</td>
	</tr>
	<tr >
		<td class="l" style="width:15mm;">��.�</td>
		<td class="l" style="width:65mm;" colspan="3">
		  <span><xsl:value-of select="ed:Payer/ed:Bank/@CorrespAcc"/></span>
		</td>
	</tr>
	<tr >
		<td class="b"  style="width:100mm; align:left" colspan="2">���� �����������</td>
		<td class="bl" style="width:15mm;">&#160;</td>
		<td class="bl" style="width:65mm;" colspan="3">&#160;</td>
	</tr>
	<tr >
		<td style="width:100mm;" rowspan="2" colspan="2">
		  <span>
			<xsl:call-template name="NameFromBic20">
			  <xsl:with-param name="bic" select="ed:Payee/ed:Bank/@BIC"/>
			</xsl:call-template>
		  </span>
		</td>
		<td class="bl" style="width:15mm;">���</td>
		<td class="l"  style="width:65mm;" colspan="3">
		  <span><xsl:value-of select="ed:Payee/ed:Bank/@BIC"/></span>
		</td>
	</tr>
	<tr >
		<td class="l" style="width:15mm;">��.�</td>
		<td class="l" style="width:65mm;" colspan="3">
		  <span><xsl:value-of select="ed:Payee/ed:Bank/@CorrespAcc"/></span>
		</td>
	</tr>
	<tr >
		<td class="b"  style="width:100mm; align:left"  colspan="2">���� ����������</td>
		<td class="bl" style="width:15mm;">&#160;</td>
		<td class="l"  style="width:65mm;" colspan="3">&#160;</td>
	</tr>
	<tr>
		<td class="b" style="width:50mm; align:left; font-size:3.7mm;">���&#160;
		  <span><xsl:value-of select="ed:Payee/@INN"/></span>
		</td>
		<td class="bl" style="width:50mm; align:left; font-size:3.7mm;">���&#160;
		  <span><xsl:value-of select="ed:Payee/@KPP"/></span>
		</td>
		<td class="l" style="width:15mm; align:left">��.�</td>
		<td class="l" style="width:65mm;" colspan="3">
		  <span><xsl:value-of select="ed:Payee/@PersonalAcc"/></span>
		</td>
	</tr>

	<tr >
		<td style="width:100mm;" rowspan="4" colspan="2">
		  <span><xsl:value-of select="ed:Payee/ed:Name"/></span>
		</td>
		<td class="l" style="width:15mm;">&#160;</td>
		<td class="l" style="width:65mm;" colspan="3">&#160;</td>
	</tr>
	<tr >
		<td class="bl" style="width:15mm;">&#160;</td>
		<td class="bl" style="width:65mm;" colspan="3">&#160;</td>
	</tr>
	<tr >
		<td class="bl" style="width:15mm;">��� ��.</td>
		<td class="l"  style="width:20mm;"><span><xsl:value-of select="@TransKind"/></span></td>
		<td class="bl" style="width:20mm;">���� ��.</td>
		<td class="l"  style="width:25mm;">&#160;</td>
	</tr>
	<tr >
		<td class="bl" style="width:15mm;">����.��.</td>
		<td class="l"  style="width:20mm; ">&#160;</td>
		<td class="bl" style="width:20mm;">����.��.</td>
		<td class="l"  style="width:25mm;">
		  <span><xsl:value-of select="@Priority"/></span>
		</td>
	</tr>
	<xsl:choose>
	  <xsl:when test="@PaymentID !='0'">

	<tr >
		<td class="b"  style="width:100mm;" colspan="2">����������</td>
		<td class="bl" style="width:15mm;">���</td>
		<td class="bl" style="width:65mm;" colspan="3">
		  <span><xsl:value-of select="@PaymentID"/></span>
		</td>
	</tr>

	  </xsl:when>
	  <xsl:otherwise>

	<tr >
		<td class="b"  style="width:100mm; align:left" colspan="2">����������</td>
		<td class="bl" style="width:15mm;">���</td>
		<td class="bl" style="width:20mm;">
		  <span><xsl:value-of select="@PaymentID"/></span>
		</td>
		<td class="bl" style="width:20mm;">���.����</td>
		<td class="bl" style="width:25mm;">&#160;</td>
	</tr>
	  </xsl:otherwise>
	</xsl:choose>
 </table>

 <table style="width:180mm; height:5mm;">
	<tr>
		<td class="b" style="width:45mm;">
		  <span style="width:40mm"><xsl:value-of select="ed:DepartmentalInfo/@CBC"/></span>
		</td>
		<td class="bl" style="width:30mm;">
		  <span style="width:25mm"><xsl:value-of select="ed:DepartmentalInfo/@OKATO"/></span>
		</td>
		<td class="bl" style="width:10mm;">
		  <span style="width:7mm"><xsl:value-of select="ed:DepartmentalInfo/@PaytReason"/></span>
		</td>
		<td class="bl" style="width:25mm;">
		  <span style="width:25mm"><xsl:value-of select="ed:DepartmentalInfo/@TaxPeriod"/></span>
		</td>
		<td class="bl" style="width:35mm;">
		  <span style="width:30mm"><xsl:value-of select="ed:DepartmentalInfo/@DocNo"/></span>
		</td>
		<td class="bl" style="width:25mm;">
		  <span style="width:20mm"><xsl:value-of select="ed:DepartmentalInfo/@DocDate"/></span>
		</td>
		<td class="bl" style="width:10mm;">
		  <span style="width:7mm;"><xsl:value-of select="ed:DepartmentalInfo/@TaxPaytKind"/></span>
		</td>
	</tr>
 </table>
 <table style="width:180mm; height:30mm;">
	<tr><td rowspan="5"><span><xsl:value-of select="ed:Purpose"/></span></td></tr>
	<tr><td>&#160;</td></tr>
	<tr><td>&#160;</td></tr>
	<tr><td>&#160;</td></tr>
	<tr><td>&#160;</td></tr>
	<tr><td style="vertical-align:bottom">���������� �������</td></tr>
    </table>
    <table style="width:180mm; height:30mm;">
	<tr style="height:15mm">
		<td class="t" style="width:50mm; text-align:center;" rowspan="2" ><p>�.�.</p></td>
		<td class="t" style="width:60mm; text-align:center;" valign="top">�������</td>
		<td class="t" style="width:70mm; text-align:right;"  valign="top">
			<p>������� ����� ����������</p>
		</td>
	</tr>
	<tr style="height:15mm;">
		<td class="bt" style="width:60mm;">&#160;</td>
		<td style="width:70mm;">&#160;</td>
	</tr>
	<tr style="height:5mm;">
		<td style="width:180mm;" colspan="3">&#160;</td>
	</tr>
 </table>

 <table style="width:180mm; height:40mm;" border="1">
	<tr style="height:10mm;">
		<td class="bt"  align="center">� �. ����.</td>
		<td class="blt" align="center">� ����. ������</td>
		<td class="blt" align="center">���� ����. ������</td>
		<td class="blt" align="center">����� ���������� �������</td>
		<td class="blt" align="center">����� ������� �������</td>
		<td class="blt" align="center">�������</td>
		<td style="width:55mm; text-align:right;"><p>���� ��������� � ���������</p></td>
	</tr>
	<tr>
		<td style="width:10mm;">&#160;</td>
		<td class="l" style="width:15mm;">&#160;</td>
		<td class="l" style="width:25mm;">&#160;</td>
		<td class="l" style="width:30mm;">&#160;</td>
		<td class="l" style="width:30mm;">&#160;</td>
		<td class="l" style="width:15mm;">&#160;</td>
		<td style="width:55mm; text-align:right;"><p>������� ����� �����������</p></td>
	</tr>
 </table>

 </xsl:template>
</xsl:stylesheet>
