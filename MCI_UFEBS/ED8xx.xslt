<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (ED8xx). -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates select="ed:ED801"/>
    <xsl:apply-templates select="ed:ED802"/>
    <xsl:apply-templates select="ed:ED804"/>
    <xsl:apply-templates select="ed:ED805"/>
    <xsl:apply-templates select="ed:ED806"/>
    <xsl:apply-templates select="ed:ED807"/>
    <xsl:apply-templates select="ed:ED808"/>
    <xsl:apply-templates select="ed:ED810"/>
    <xsl:apply-templates select="ed:ED811"/>
    <xsl:apply-templates select="ed:ED813"/>
    <xsl:apply-templates select="ed:ED814"/>
  </xsl:template>

  <xsl:include href="ED801.xslt" />
  <xsl:include href="ED802.xslt" />
  <xsl:include href="ED804.xslt" />
  <xsl:include href="ED805.xslt" />
  <xsl:include href="ED806.xslt" />
  <xsl:include href="ED807.xslt" />
  <xsl:include href="ED808.xslt" />
  <xsl:include href="ED810.xslt" />
  <xsl:include href="ED811.xslt" />
  <xsl:include href="ED813.xslt" />
  <xsl:include href="ED814.xslt" />

<!--  InfoTypeCode  -->

  <xsl:template name="InfoTypeCode">
    <xsl:param name="str" select="'��� �������������   : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".='FIRR'"><xsl:text> - ������ ����������&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".='SIRR'"><xsl:text> - ������� ����������&#13;&#10;</xsl:text></xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

<!--  CreationReason  -->

  <xsl:template name="CreationReason">
    <xsl:param name="str" select="'������� �������� �� : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".='RQST'"><xsl:text> - ����� �� ��-������ ���������&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".='EOCC'"><xsl:text> - �������� ������������� ����������������� ����&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".='EOBD'"><xsl:text> - �������� ����������� �����&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".='ACCH'"><xsl:text> - �������� ��������� ������ �����&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".='SOBD'"><xsl:text> - ���������� ���, ����������� � ������ ����.��� � �� ��&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".='UIRP'">
      <xsl:text> - ��������� ����������� � ��������� ���������</xsl:text>
    </xsl:if>
    <xsl:if test=".='RIRP'">
      <xsl:text> - ������ �������������� ����������� � ��������� ���������</xsl:text>
    </xsl:if>
    <xsl:if test=".='ANTC'">
      <xsl:text> - ��������� ��������� ������ �������������� /������ ��� �� ���������</xsl:text>
    </xsl:if>
    <xsl:if test=".='UIRA'">
      <xsl:text> - ��������� �������������� ����������� � ��������� ����� ���������</xsl:text>
    </xsl:if>
    <xsl:if test=".='RIRA'">
      <xsl:text> - ������ �������������� ����������� � ��������� ����� ���������</xsl:text>
    </xsl:if>
    <xsl:if test=".='APPA'"><xsl:text> - ����� �������, �������, ����</xsl:text></xsl:if>
    <xsl:if test=".='ALCH'">
      <xsl:text> - ��������� ������, ��������� �� ������ ���/���</xsl:text>
    </xsl:if>
    <xsl:if test=".='SMON'">
      <xsl:text> - � ����� ��������� ������� ������� �� ������. � ���� ������������</xsl:text>
    </xsl:if>
    <xsl:if test=".='SPOF'">
      <xsl:text> - � ����� ��������� ����������� ������� �� ������. � ���� ������������ </xsl:text>
    </xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

<!--  PtType  -->

  <xsl:template name="PtType">
    <xsl:param name="str" select="'��� ��������� �������� : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".= '00'"><xsl:text> - ������� ���������� ����� ������</xsl:text></xsl:if>
    <xsl:if test=".= '10'"><xsl:text> - ��������-�������� �����</xsl:text></xsl:if>
    <xsl:if test=".= '12'"><xsl:text> - ���������, ��������� � ������������ ����</xsl:text></xsl:if>
    <xsl:if test=".= '15'"><xsl:text> - ����������� ������������� ������������ �������� ��</xsl:text></xsl:if>
    <xsl:if test=".= '16'"><xsl:text> - �������� �����</xsl:text></xsl:if>
    <xsl:if test=".= '20'"><xsl:text> - ��������� �����������</xsl:text></xsl:if>
    <xsl:if test=".= '30'"><xsl:text> - ������ ��������� �����������</xsl:text></xsl:if>
    <xsl:if test=".= '40'"><xsl:text> - ������� ���������� ����� ������</xsl:text></xsl:if>
    <xsl:if test=".= '51'"><xsl:text> - ����������� ������������</xsl:text></xsl:if>
    <xsl:if test=".= '52'"><xsl:text> - ��������������� ����� ������������ ������������</xsl:text></xsl:if>
    <xsl:if test=".= '60'"><xsl:text> - ����������� ��������� �����������</xsl:text></xsl:if>
    <xsl:if test=".= '65'"><xsl:text> - ����������� ����������� (������������) ���� </xsl:text></xsl:if>
    <xsl:if test=".= '71'"><xsl:text> - ������ ��������� �����������, ���������� ��������� ����������</xsl:text></xsl:if>
    <xsl:if test=".= '78'"><xsl:text> - ������� ��������� �������</xsl:text></xsl:if>
    <xsl:if test=".= '90'"><xsl:text> - ���������� ����������� (����������,�������������� ��������) </xsl:text></xsl:if>
    <xsl:if test=".= '99'"><xsl:text> - ������ ����� ������, �� ���������� ���������� ��������� �������</xsl:text></xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

<!-- Srvcs  -->

  <xsl:template name="Srvcs">
    <xsl:param name="str" select="'��������� �������      : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".= '1'"><xsl:text> - ������ ���������� ��������</xsl:text></xsl:if>
    <xsl:if test=".= '2'"><xsl:text> - ������ �������� ��������</xsl:text></xsl:if>
    <xsl:if test=".= '3'"><xsl:text> - ������ ���������� � �������� ��������</xsl:text></xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

<!--  XchType  -->

  <xsl:template name="XchType">
    <xsl:param name="str" select="'�������� ������        : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".= '0'"><xsl:text> - �� �������� ������</xsl:text></xsl:if>
    <xsl:if test=".= '1'"><xsl:text> - �������� ������ � �� ��</xsl:text></xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

<!--  ParticipantStatus  -->

  <xsl:template name="ParticipantStatus">
    <xsl:param name="str" select="'������ ���������       : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".= 'PSAC'"><xsl:text> - �����������</xsl:text></xsl:if>
    <xsl:if test=".= 'PSDL'"><xsl:text> - ���������</xsl:text></xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

<!--  AccountType  -->

  <xsl:template name="AccountType">
    <xsl:param name="str" select="'��� �����             : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".='CBRA'"><xsl:text> - ���� ����� ������ </xsl:text></xsl:if>
    <xsl:if test=".='CRSA'"><xsl:text> - ����������������� ���� </xsl:text></xsl:if>
    <xsl:if test=".='BANA'"><xsl:text> - ���������� ����</xsl:text></xsl:if>
    <xsl:if test=".='TRSA'"><xsl:text> - ���� ������������ ������������ </xsl:text></xsl:if>
    <xsl:if test=".='CLRA'"><xsl:text> - ����������� ���� </xsl:text></xsl:if>
    <xsl:if test=".='GARA'"><xsl:text> - ���� ������������ ����� </xsl:text></xsl:if>
    <xsl:if test=".='UTRA'"><xsl:text> - ������ ������������ ���� </xsl:text></xsl:if>
    <xsl:if test=".='TRUA'"><xsl:text> - ���� �������������� ����������</xsl:text></xsl:if>
    <xsl:if test=".='EPSA'"><xsl:text> - ����������� ���� ���</xsl:text></xsl:if>
    <xsl:if test=".='EPGA'"><xsl:text> - ���� ������������ ����� ��� </xsl:text></xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

<!--  LimitType  -->

  <xsl:template name="LimitType">
    <xsl:param name="str" select="'��� ������ : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".= 'URGL'"><xsl:text> - ������� �����&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'NURL'"><xsl:text> - ��������� �����&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'IOCL'"><xsl:text> - ����� �������������� ������� � ������� ��������&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'FPML'"><xsl:text> - ����� ������������ ������������ ��������� ���� ����������� (������.)&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'VPML'"><xsl:text> - ����� ������������ ������������ ��������� ���� ����������� (�����.)&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'FIPL'"><xsl:text> - ����� ������������ ���������� ��������� (�������������)&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'VIPL'"><xsl:text> - ����� ������������ ���������� ��������� (����������)&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'IPCL'"><xsl:text> - ����� ���������� � ���������� ���������&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'ICCL'"><xsl:text> - ����� �������������� ����� ���������� ���������������&#13;&#10;</xsl:text>
      <xsl:text>       ����������� ���������� �������� ������� ������������ �������� &#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'TCCL'"><xsl:text> - ����� ����� ����� ���������� ��������������� �����������&#13;&#10;</xsl:text>
      <xsl:text>       ���������� �������� ������� ������������ ��������&#13;&#10;</xsl:text></xsl:if>
  </xsl:template>

<!--  ClearingCircuit  -->

  <xsl:template name="ClearingCircuit">
    <xsl:param name="str" select="'����������� ������� : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".= 'VISA'"><xsl:text> - VISA</xsl:text><xsl:text>&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'MSCR'"><xsl:text> - MasterCard</xsl:text><xsl:text>&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'MIRR'"><xsl:text> - ���</xsl:text><xsl:text>&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'UNPY'"><xsl:text> - UnionPay</xsl:text><xsl:text>&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'JCBB'"><xsl:text> - JCB</xsl:text><xsl:text>&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'AMEX'"><xsl:text> - American Express</xsl:text><xsl:text>&#13;&#10;</xsl:text></xsl:if>
    <xsl:if test=".= 'FSSS'"><xsl:text> - ���� ����������� �����������</xsl:text><xsl:text>&#13;&#10;</xsl:text></xsl:if>
  </xsl:template>

<!--  QueueType  -->


  <xsl:template name="QueueType">
    <xsl:param name="str" select="'��� ������� : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".= 'QINT'">
      <xsl:text> - ������������� �������</xsl:text><xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>
    <xsl:if test=".= 'QWRH'">
      <xsl:text> - ������� ������������ ��� ���������� �� ��������� ����</xsl:text><xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>
    <xsl:if test=".= 'QSPC'">
      <xsl:text> - ������� �� ����������� � ���� ������������</xsl:text><xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>
    <xsl:if test=".= 'QWAP'">
      <xsl:text> - ������� ������������, ��������� ���������� �� ���������� ��������</xsl:text><xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>
    <xsl:if test=".= 'QWCH'">
      <xsl:text> - ������� ������������, ��������� ��������</xsl:text><xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>


<!--  ed:BICDirectoryEntry  -->

  <xsl:template match="ed:BICDirectoryEntry" name="BICDirectoryEntry">
    <xsl:param name="pref" select="concat(position(),'.  ')"/>

    <xsl:value-of select="$pref"/>
    <xsl:for-each select="@ChangeType">
      <xsl:if test=".= 'ADDD'"><xsl:text>������ ����������� ��� ���� ��������� </xsl:text></xsl:if>
      <xsl:if test=".= 'CHGD'"><xsl:text>������ ����������� ��� ���� �������� </xsl:text></xsl:if>
      <xsl:if test=".= 'DLTD'"><xsl:text>������ ����������� ��� ���� ������� </xsl:text></xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>���          : </xsl:text><xsl:value-of select="@BIC"/><xsl:text>&#13;&#10;</xsl:text>
    <xsl:for-each select="ed:ParticipantInfo">
      <xsl:text>������������ : </xsl:text><xsl:value-of select="@NameP"/><xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>���.�����    : </xsl:text><xsl:value-of select="@RegN"/><xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>�����        : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="@Adr"/>
        <xsl:with-param name="width" select="60"/>
        <xsl:with-param name="before" select="string('             : ')"/>
      </xsl:call-template>
      <xsl:text>��� ���.���. : </xsl:text><xsl:value-of select="@PrntBIC"/><xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>���          : </xsl:text><xsl:value-of select="@UID"/><xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="@DateIn">
        <xsl:text>���� ���. � ������ ��-��� �������� : </xsl:text>
        <xsl:call-template name="date"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@DateOut">
        <xsl:text>���� ����. ���������� �� ��������� : </xsl:text>
        <xsl:call-template name="date"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@PtType">
        <xsl:call-template name="PtType"/>
      </xsl:for-each>

      <xsl:for-each select="@Srvcs">
        <xsl:call-template name="Srvcs"/>
      </xsl:for-each>

      <xsl:for-each select="@XchType">
        <xsl:call-template name="XchType"/>
      </xsl:for-each>

      <xsl:for-each select="@ParticipantStatus">
        <xsl:call-template name="ParticipantStatus"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:if test= "count(ed:RstrList) != 0">
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:text>�������� ����������� ��������� :&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:RstrList">
          <xsl:for-each select="@Rstr">
            <xsl:text>��� ����������� : </xsl:text><xsl:value-of select="."/>
            <xsl:if test=".= 'URRS'"><xsl:text> - ����������� ������� �������� ��������</xsl:text></xsl:if>
            <xsl:if test=".= 'LWRS'"><xsl:text> - ����� ��������</xsl:text></xsl:if>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:for-each select="@RstrDate">
            <xsl:text>���� ������ �������� ����������� : </xsl:text>
            <xsl:call-template name="date"/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:for-each select="ed:DirectParticipantAccount">
        <xsl:text>��� ������� ���������  : </xsl:text><xsl:value-of select="@BIC"/><xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>���� ������� ��������� : </xsl:text><xsl:value-of select="@Account"/><xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
    </xsl:for-each>

    <xsl:if test= "count(ed:SWBICS) != 0">
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>�������� ��� (�����)&#13;&#10;</xsl:text>
      <xsl:text>--------------------&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:SWBICS">
        <xsl:value-of select="@SWBIC"/>
        <xsl:if test="@DefaultSWBIC = 1"><xsl:text> (�� ���������)</xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>

    <xsl:if test= "count(ed:Accounts) != 0">
      <xsl:text>����� ���������&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:Accounts">
        <xsl:sort order="ascending" select="@Account"/>
        <xsl:text>--------------------&#13;&#10;</xsl:text>
        <xsl:text>����                  : </xsl:text><xsl:value-of select="@Account"/><xsl:text>&#13;&#10;</xsl:text>
        <xsl:for-each select="@RegulationAccountType">
          <xsl:call-template name="AccountType"/>
        </xsl:for-each>
        <xsl:text>����������� ����      : </xsl:text><xsl:value-of select="@CK"/><xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��� ���, ������. ���� : </xsl:text>
        <xsl:value-of select="@AccountCBRBIC"/><xsl:text>&#13;&#10;</xsl:text>
        <xsl:for-each select="@DateIn">
          <xsl:text>���� �������� �����   : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>

        <xsl:for-each select="@AccountStatus">
          <xsl:text>������ �����          : </xsl:text><xsl:value-of select="."/>
          <xsl:if test=".= 'ACAC'"><xsl:text> - �����������</xsl:text></xsl:if>
          <xsl:if test=".= 'ACDL'"><xsl:text> - ���������</xsl:text></xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:if test= "count(ed:AccRstrList) != 0">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>�������� ����������� �������� �� ����� :&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:AccRstrList">
            <xsl:for-each select="@AccRstr">
              <xsl:text>��� ����������� : </xsl:text><xsl:value-of select="."/>
              <xsl:if test=".= 'LMRS'">
              <xsl:text> - ���������� ������������ ����� � ��� ����������������� � �������.������</xsl:text>
              </xsl:if>
              <xsl:if test=".= 'URRS'"><xsl:text> - ����������� ������� �������� ��������</xsl:text></xsl:if>
              <xsl:if test=".= 'CLRS'"><xsl:text> - �������� �����</xsl:text></xsl:if>
              <xsl:text>&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:for-each select="@AccRstrDate">
              <xsl:text>���� ������ �������� ����������� : </xsl:text><xsl:call-template name="date"/>
              <xsl:text>&#13;&#10;</xsl:text>
            </xsl:for-each>
          </xsl:for-each>
        </xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
    </xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--  LimitChangeType  -->

  <xsl:template name="LimitChangeType">
    <xsl:param name="str" select="'��� ��������� ������  : '"/>
    <xsl:value-of select="concat($str,.)"/>
    <xsl:if test=".='SETL'"><xsl:text> - ��������� ������</xsl:text></xsl:if>
    <xsl:if test=".='DELL'"><xsl:text> - ������ ������</xsl:text></xsl:if>
    <xsl:if test=".='STND'"><xsl:text> - ��������� ������ �� ��������� ������������ ����</xsl:text></xsl:if>
    <xsl:if test=".='DLND'"><xsl:text> - ������ ������ �� ��������� ������������ ����</xsl:text></xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>


<!--  IntradayRestrictions  -->

<xsl:template name="IntradayRestrictions">
  <xsl:param name="type" select="''"/>
  <xsl:value-of select="string($type)"/><xsl:text>&#13;&#10;</xsl:text>
  <xsl:choose>
    <xsl:when test= "@Applied=1">
      <xsl:text>����������� ���������&#13;&#10;</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>����������� �� ���������&#13;&#10;</xsl:text>
    </xsl:otherwise>
  </xsl:choose>

  <xsl:for-each select="@ApplyDate">
    <xsl:text>���� ������������ (������) ����������� : </xsl:text>
    <xsl:call-template name="date"/>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:for-each>
  <xsl:for-each select="ed:RestrictionBasisDoc">
    <xsl:call-template name="RestrictionBasisDoc"/>
  </xsl:for-each>
<!--   <xsl:text>&#13;&#10;</xsl:text>  -->
</xsl:template>

<!--  RestrictionBasisDoc  -->

<xsl:template name="RestrictionBasisDoc">
    <xsl:text>��������� ��� ����������� ������� � �������� ��� ���������� �� ������� ���������� �������� :&#13;&#10;</xsl:text>
    <xsl:for-each select="@DocDate">
      <xsl:text>���� ���������     : </xsl:text>
      <xsl:call-template name="date"/><xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
    <xsl:for-each select="@DocNumber">
      <xsl:text>����� ���������    : </xsl:text>
      <xsl:value-of select="."/><xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
    <xsl:for-each select="@Description">
      <xsl:text>�������� ��������� : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="."/>
        <xsl:with-param name="width" select="60"/>
        <xsl:with-param name="before" select="string('                   : ')"/>
       </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
</xsl:template>

<!--  LimitsInfo  -->

<xsl:template name="LimitInfo">
  <xsl:for-each select="@LimitType">
    <xsl:call-template name="LimitType"/><xsl:text>&#13;&#10;</xsl:text>
  </xsl:for-each>
  <xsl:for-each select="@Value">
    <xsl:text>�������� ������      : </xsl:text>
    <xsl:call-template name="summa"/><xsl:text>&#13;&#10;</xsl:text>
  </xsl:for-each>
  <xsl:for-each select="@Utilization">
    <xsl:text>������� ������������� ������ : </xsl:text>
    <xsl:call-template name="summa"/><xsl:text>&#13;&#10;</xsl:text>
  </xsl:for-each>

  <xsl:text>���  : </xsl:text><xsl:value-of select="@BIC"/><xsl:text>&#13;&#10;</xsl:text>
  <xsl:text>���� : </xsl:text><xsl:value-of select="@Account"/><xsl:text>&#13;&#10;</xsl:text>
  <xsl:text>���  : </xsl:text><xsl:value-of select="@UID"/><xsl:text>&#13;&#10;</xsl:text>
  <xsl:for-each select="@ClearingCircuit">
    <xsl:call-template name="ClearingCircuit"/><xsl:text>&#13;&#10;</xsl:text>
  </xsl:for-each>
  <xsl:text>&#13;&#10;</xsl:text>
</xsl:template>

<!-- ArrestDetailedInfo  -->

<xsl:template name="ArrestDetailedInfo">
  <xsl:text>������������� ������ / ����������� : </xsl:text><xsl:value-of select="@ArrestID"/><xsl:text>&#13;&#10;</xsl:text>
  <xsl:text>&#13;&#10;</xsl:text>
  <xsl:for-each select="@IndeterminateAmountFlag">
    <xsl:call-template name="IndeterminateAmountFlag"/>
  </xsl:for-each>
  <xsl:for-each select="@ArrestAmount">
    <xsl:text>����� ������ / �����������         : </xsl:text>
    <xsl:call-template name="summa"/><xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
  </xsl:for-each>
  <xsl:for-each select="@OutstandingAmount">
    <xsl:text>����� ������������� ������ (�����������)    : </xsl:text>
    <xsl:call-template name="summa"/><xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>�� ����� �� ��������� ����������� ��������� :&#13;&#10;</xsl:text>
    <xsl:text>�� ����������� ���� ������������,           :&#13;&#10;</xsl:text>
    <xsl:text>����������� �� ���� ������������ �������    :&#13;&#10;&#13;&#10;</xsl:text>
  </xsl:for-each>
  <xsl:for-each select="ed:RestrictionBasisDoc">
    <xsl:call-template name="RestrictionBasisDoc"/>
  </xsl:for-each>
</xsl:template>

<!-- IndeterminateAmountFlag -->

<xsl:template name="IndeterminateAmountFlag">
  <xsl:call-template name="LongText">
    <xsl:with-param name="text_t"
      select="'��������� ������� ���������� ������ � ��������������� �������� �� ����� ��� �������� �����'"/>
  </xsl:call-template>
</xsl:template>

<!--  LongText  -->

<xsl:template name="LongText">
  <xsl:param name="ind" select="."/>
  <xsl:param name="text_t" select="''"/>
  <xsl:param name="text_f" select="''"/>
  <xsl:param name="text" select="''"/>
  <xsl:param name="width" select="80"/>
  <xsl:param name="before" select="''"/>

  <xsl:variable name="text_t_full" select="concat($text_t,$text)"/>
  <xsl:variable name="text_f_full" select="concat($text_f,$text)"/>

  <xsl:choose>
    <xsl:when test= "$ind = 1">
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="string($text_t_full)"/>
        <xsl:with-param name="width" select="80"/>
        <xsl:with-param name="before" select="string('')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="string($text_f_full)"/>
        <xsl:with-param name="width" select="80"/>
        <xsl:with-param name="before" select="string('')"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text>&#13;&#10;</xsl:text>
</xsl:template>

<!--  QueuingReasonCode  -->

<xsl:template name="QueuingReasonCode">
  <xsl:text>������� ��������� � ������� : </xsl:text><xsl:value-of select="."/>
  <xsl:if test=".='QLLQ'"><xsl:text> - ��������������� �������� �������&#13;&#10;</xsl:text></xsl:if>
  <xsl:if test=".='QWFC'"><xsl:text> - �������� ����� (��� ��������� ������������)&#13;&#10;</xsl:text></xsl:if>
  <xsl:if test=".='QLSF'"><xsl:text> - ������� ����� ������������ ������������ ��������� ���� ����������� (�������������)&#13;&#10;</xsl:text></xsl:if>
  <xsl:if test=".='QLSV'"><xsl:text> - ������� ����� ������������ ������������ ��������� ���� �����������(����������)&#13;&#10;</xsl:text></xsl:if>
  <xsl:if test=".='QLIF'"><xsl:text> - ������� ����� ������������ ���������� ��������� (�������������)&#13;&#10;</xsl:text></xsl:if>
  <xsl:if test=".='QLIV'"><xsl:text> - ������� ����� ������������ ���������� ��������� (����������)&#13;&#10;</xsl:text></xsl:if>
  <xsl:if test=".='QCLI'"><xsl:text> - ������� ����� ���������� � ���������� ���������&#13;&#10;</xsl:text></xsl:if>
  <xsl:if test=".='QICL'">
    <xsl:call-template name="text_wrapper">
      <xsl:with-param name="text" select="string(' - ������� ����� �������.����� ���������� ���������.����������� ���������� �������� ������� �������.��������')"/>
      <xsl:with-param name="before" select="string('                            :        ')"/>
    </xsl:call-template>
  </xsl:if>
  <xsl:if test=".='QTCL'">
    <xsl:call-template name="text_wrapper">
      <xsl:with-param name="text" select="string(' - ������� ����� ����� ����� ���������� ���������.����������� ���������� �������� ������� �������.��������')"/>
      <xsl:with-param name="before" select="string('                            :        ')"/>
    </xsl:call-template>
  </xsl:if>
  <xsl:if test=".='QLRS'">
    <xsl:call-template name="text_wrapper">
      <xsl:with-param name="text" select="string(' - �������������� ����������� �� �����, �� ����������� ��������., ���������� � ������� �������� �������� �������')"/>
      <xsl:with-param name="before" select="string('                            :        ')"/>
    </xsl:call-template>
  </xsl:if>
  <xsl:if test=".='QLRW'"><xsl:text> - �������������� ����������� �� ����� � ����� � ������� �������� �������� �������&#13;&#10;</xsl:text></xsl:if>
  <xsl:if test=".='QIRS'"><xsl:text> - � ��������� ����� ��������� � ������� ������������� ��� ����������� �����������&#13;&#10;</xsl:text></xsl:if>
  <xsl:if test=".='QACI'"><xsl:text> - �������� ��������������� ������������&#13;&#10;</xsl:text></xsl:if>
  <xsl:if test=".='QCES'">
    <xsl:call-template name="text_wrapper">
      <xsl:with-param name="text" select="string(' - ������������ �������� �� ������������� �������, �.�. ��������� � ��������.������� �������.������� �� �������� ���� ��� ������')"/>
      <xsl:with-param name="before" select="string('                            :        ')"/>
    </xsl:call-template>
  </xsl:if>
  <xsl:if test=".='QNET'">
    <xsl:call-template name="text_wrapper">
      <xsl:with-param name="text" select="string(' - ������������ �������� �� ������������� �������, �.�. �� ��������� ����������� ����� ���������� (��� �����, ���)')"/>
      <xsl:with-param name="before" select="string('                            :        ')"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>


</xsl:stylesheet>
