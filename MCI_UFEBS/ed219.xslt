<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED219  -->
  <xsl:template match="ed:ED219">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>�������� ����� /ED219/ </xsl:text>
      <xsl:if test="@EDReportID != ''">
          <xsl:value-of select="@EDReportID"/>
      </xsl:if>
      <xsl:text> �� </xsl:text>
      <xsl:for-each select="@ReportDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:call-template name="PriznGr"/>
      <xsl:if test="ed:InitialED/@EDNo != ''">
          <xsl:apply-templates select="ed:InitialED"/>
      </xsl:if>
      <xsl:for-each select="ed:ReportContent"><xsl:value-of select="current()"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>