<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED243  -->
  <xsl:template match="ed:ED243">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>������ �� ��������� ���������� �� ��� ��������� /ED243/ </xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:call-template name="EDDefineRequestCode"/>
      <xsl:text>�������������� ��������� ��� : </xsl:text>
      <xsl:text>� </xsl:text><xsl:value-of select="ed:OriginalEPD/@EDNo"/>
      <xsl:text> �� </xsl:text>
      <xsl:for-each select="ed:OriginalEPD/@EDDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text> ��� : </xsl:text><xsl:value-of select="ed:OriginalEPD/@EDAuthor"/>
      <xsl:text>&#13;&#10;&#10;</xsl:text>

      <xsl:for-each select="ed:EDDefineRequestInfo">
        <xsl:text>��������� ���, ���������� ������ </xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>=====================================</xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:if test="@AccDocNo != ''">
          <xsl:text>����� ����. ���������    : </xsl:text>
          <xsl:value-of select="@AccDocNo"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>

        <xsl:if test="@AccDocDate != ''">
          <xsl:text>���� ����. ���������     : </xsl:text>
          <xsl:for-each select="@AccDocDate"><xsl:call-template name="date"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
        <xsl:if test="@PayerAcc != ''">
          <xsl:text>����� ����� �����������  : </xsl:text>
          <xsl:value-of select="@PayerAcc"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
        <xsl:if test="@PayeeAcc != ''">
          <xsl:text>����� ����� ����������   : </xsl:text>
          <xsl:value-of select="@PayeeAcc"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>

        <xsl:if test="@Sum != ''">
          <xsl:text>�����                    : </xsl:text>
          <xsl:for-each select="@Sum"><xsl:call-template name="ffsum"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
        <xsl:if test="ed:PayerName != ''">
          <xsl:text>������������ ����������� : </xsl:text><xsl:value-of select="ed:PayerName"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:if test="ed:PayeeName != ''">
          <xsl:text>������������ ����������  : </xsl:text><xsl:value-of select="ed:PayeeName"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
        <xsl:if test="ed:EDDefineRequestText != ''">
          <xsl:text>��������� � �������      :</xsl:text>
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="ed:EDDefineRequestText"/>
            <xsl:with-param name="width" select="70"/>
            <xsl:with-param name="before" select="string('                         : ')"/>
          </xsl:call-template>
        </xsl:if>
        <xsl:if test="count(ed:EDFieldList) != 0">
          <xsl:text>������ ���������� ���������� � ����� ������������ ������������</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>-------------------------------------------------------------------</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:EDFieldList">
            <xsl:apply-templates select="."/>
          </xsl:for-each>
        </xsl:if>

        <xsl:if test="count(ed:EDReestrInfo) != 0">
          <xsl:text>���������� � ������� ������� �������� � ���������� ������������</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>-------------------------------------------------------------------</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:EDReestrInfo">
            <xsl:apply-templates select="."/>
          </xsl:for-each>
        </xsl:if>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>