<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED245  -->
  <xsl:template match="ed:ED245">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>����� ��� ������� �������������� ���� /ED245/ </xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>

      <xsl:text>���������� ���� � ������ :  </xsl:text><xsl:value-of select="@IMQuantity"/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:if test="@IMQuantity != 0">
        <xsl:text>          ���������� � ������� �������������� ���� &#13;&#10;</xsl:text>
        <xsl:text>-----------------------------------------------------------------&#13;&#10;</xsl:text>
        <xsl:text>   � �/�    ��������������            �������������� ������      &#13;&#10;</xsl:text>
        <xsl:text>            ��������������� ����      ���.����  (PacketEID)      &#13;&#10;</xsl:text>
        <xsl:text>-----------------------------------------------------------------&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:IMGroupInfo">
          <xsl:apply-templates select="."/>
        </xsl:for-each>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="ed:IMGroupInfo">
      <xsl:value-of select="concat('                        ������ ',@EDTypeNo)"/>
      <xsl:if test="@ReceiverTypeCode = 1"> <xsl:text>.  �������� �� ���</xsl:text> </xsl:if>
      <xsl:if test="@ReceiverTypeCode = 2"> <xsl:text>.  ���������� ���</xsl:text> </xsl:if>
      <xsl:text>&#13;&#10;                      -----------------------------------&#13;&#10;&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:IMInfo">

       <xsl:variable name="dat" select="concat(substring(@EDDate,9,2),'.',
         substring(@EDDate,6,2),'.',substring(@EDDate,1,4))"/>
       <xsl:variable name="datPacketEID" select="concat(substring(ed:RefPacketEID/@EDDate,9,2),'.',
         substring(ed:RefPacketEID/@EDDate,6,2),'.',substring(ed:RefPacketEID/@EDDate,1,4))"/>
       <xsl:variable name="datED273" select="concat(substring(ed:RefED273/@EDDate,9,2),'.',
         substring(ed:RefED273/@EDDate,6,2),'.',substring(ed:RefED273/@EDDate,1,4))"/>

        <xsl:call-template name="LeftPad">
          <xsl:with-param name="input" select="position()"/>
          <xsl:with-param name="n" select="9"/>
        </xsl:call-template>
        <xsl:value-of select="string('   ')"/>

        <xsl:call-template name="Pad">
          <xsl:with-param name="input" select="concat(@EDNo,' �� ',$dat)"/>
          <xsl:with-param name="n" select="27"/>
        </xsl:call-template>

        <xsl:choose>
          <xsl:when test="ed:RefPacketEID/@EDNo !=''">
            <xsl:call-template name="Pad">
              <xsl:with-param name="input" select="concat(ed:RefPacketEID/@EDNo,' �� ',$datPacketEID )"/>
              <xsl:with-param name="n" select="27"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="$blnk27"/></xsl:otherwise>
        </xsl:choose>
<!--
        <xsl:choose>
          <xsl:when test="ed:RefED273/@EDNo !=''">
            <xsl:call-template name="Pad">
              <xsl:with-param name="input" select="concat(ed:RefED273/@EDNo,' �� ',$datED273)"/>
              <xsl:with-param name="n" select="27"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="$blnk27"/></xsl:otherwise>
        </xsl:choose>
-->
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:value-of select="$blnk12"/>
        <xsl:call-template name="Pad">
          <xsl:with-param name="input" select="@EDAuthor"/>
          <xsl:with-param name="n" select="27"/>
        </xsl:call-template>
        <xsl:choose>
          <xsl:when test="ed:RefPacketEID/@EDNo !=''">
            <xsl:call-template name="Pad">
              <xsl:with-param name="input" select="ed:RefPacketEID/@EDAuthor"/>
              <xsl:with-param name="n" select="27"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="$blnk27"/></xsl:otherwise>
        </xsl:choose>
<!--
        <xsl:choose>
          <xsl:when test="ed:RefED273/@EDNo !=''">
            <xsl:call-template name="Pad">
              <xsl:with-param name="input" select="ed:RefED273/@EDAuthor"/>
              <xsl:with-param name="n" select="27"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="$blnk27"/></xsl:otherwise>
        </xsl:choose>
-->
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>-----------------------------------------------------------------&#13;&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>