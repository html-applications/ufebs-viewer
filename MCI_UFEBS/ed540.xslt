<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED540  -->
  <xsl:template match="ed:ED540">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>������ ���������� � ����������/���������� �� /ED540/&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>

      <xsl:text> ��� ������� ���������� : </xsl:text>
      <xsl:for-each select="@ExchangeTypeCode">
          <xsl:value-of select="."/>
          <xsl:if test=".=1"><xsl:text>  (���������� �������� �� ��� � ���) </xsl:text></xsl:if>
          <xsl:if test=".=2"><xsl:text>  (���������� �������� �� ��� � ���) </xsl:text></xsl:if>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:if test="@EDTypeNo != ''">
          <xsl:text> ��� ��      :  </xsl:text><xsl:value-of select="@EDTypeNo"/>
          <xsl:if test="@EDTypeNo =1001"><xsl:text>  (PacketEPD) </xsl:text></xsl:if>
          <xsl:if test="@EDTypeNo =1002"><xsl:text>  (PacketEsid) </xsl:text></xsl:if>
          <xsl:if test="@EDTypeNo =1003"><xsl:text>  (PacketEid) </xsl:text></xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="@InquiryDate != ''">
        <xsl:text> ����, �� ������� ������������� �� : </xsl:text>
        <xsl:for-each select="@InquiryDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@ARMNo != ''">
          <xsl:text> ����� ���   :  </xsl:text><xsl:value-of select="@ARMNo"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
  </xsl:template>


</xsl:stylesheet>