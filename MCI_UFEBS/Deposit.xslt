<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!-- Deposit -->
  <xsl:template name="Deposit">
    <xsl:for-each select="@DepositConsNo">
      <xsl:text>����� ������������ ����������� ���������� : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@DepositConsDate">
      <xsl:text>���� ������������ ����������� ����������  : </xsl:text><xsl:call-template name="date"/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@DepositOpDate">
      <xsl:text>���� ���������� ����������� �������� ���  :&#13;&#10;</xsl:text>
      <xsl:text>���������� �������� �� �������������      :&#13;&#10;</xsl:text>
      <xsl:text>���������� ������                         : </xsl:text><xsl:call-template name="date"/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@DepositOpMethod">
      <xsl:text>������ ���������� ���������� ��������     : </xsl:text>
      <xsl:value-of select="."/>
      <xsl:if test=". = 0">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="string(' (���������� �������� �� ���������� ������, ������ ���������� � ������������)')"/>
          <xsl:with-param name="width" select="40"/>
          <xsl:with-param name="before" select="string('                                          :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test=". = 1">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="string(' (���������� �������� �� ���������� ������, ������ ���������� � �����������)')"/>
          <xsl:with-param name="width" select="40"/>
          <xsl:with-param name="before" select="string('                                          :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test=". = 2">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="string(' (���������� �������� �� ���������� ������, �������� ��������� �������)')"/>
          <xsl:with-param name="width" select="40"/>
          <xsl:with-param name="before" select="string('                                          :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test=". = 3">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="string(' (���������� �������� �� ������������� ���������� ������)')"/>
          <xsl:with-param name="width" select="40"/>
          <xsl:with-param name="before" select="string('                                          :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@DepositOpKind">
      <xsl:text>���� �������� / ����������� �������       : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@AttractDate">
      <xsl:text>���� ������������ ������� � �������,      :&#13;&#10;</xsl:text>
      <xsl:text>����������� � ����� ������                : </xsl:text><xsl:call-template name="date"/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@ReturnDate">
      <xsl:text>���� �������� �������� � ���������        :&#13;&#10;</xsl:text>
      <xsl:text>���������                                 : </xsl:text><xsl:call-template name="date"/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@ApplicationSum">
      <xsl:text>����� ��������                            : </xsl:text>
      <xsl:call-template name="ffsum"/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@WideApplicationRate">
      <xsl:text>���������� ������ �� ��������             : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@BIC">
      <xsl:text>��� ����� �������� ��������� ����� ��     : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@CorrAcc">
      <xsl:text>����� ��������� ����� ��                  : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@AccBIC">
      <xsl:text>��� ����� �������� �������� �����         :&#13;&#10;</xsl:text>
      <xsl:text>��� ������������ ������� � �������        : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@DepositAcc">
      <xsl:text>����� �������� ����� ��� ������������     :&#13;&#10;</xsl:text>
      <xsl:text>������� � �������                         : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@OrgRegNum">
      <xsl:text>��������������� ����� ��                  : </xsl:text><xsl:value-of select="."/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:OrgName">
      <xsl:text>����������� ��������� ������������ ��     : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="."/>
        <xsl:with-param name="width" select="50"/>
        <xsl:with-param name="before" select="string('                                          : ')"/>
      </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>