<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          31-08-2015 ������ ����� 2015.4.2 �� 28-09-2015
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

<!-- ======================================================================= -->
<!-- � � � , �������� � ������ ������ � ��. �� (ED215,ED221,ED273)  -->
<!-- ======================================================================= -->
  <xsl:template name="ED_Tab">
    <xsl:param name="ed" select="."/>
    <!-- n=1 ����� �������   -->
    <!-- n=2 ������� ������� -->
    <xsl:param name="n" select="2"/>
    <xsl:param name="pos" select="1"/>
    <!-- <xsl:text>Pos=</xsl:text><xsl:value-of select="$pos"/> -->
    <xsl:for-each select="$ed">
      <xsl:if test="contains(name(..),'EDCopy')">
        <xsl:choose>
          <xsl:when test="../@CtrlCode">
              <xsl:text> </xsl:text>
              <xsl:value-of select="../@CtrlCode"/>
              <xsl:text> </xsl:text>
          </xsl:when>
          <xsl:otherwise><xsl:text>   -  </xsl:text></xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      <xsl:if test="not(contains(name(..),'EDCopy'))">
        <xsl:call-template name="LeftPad">
          <xsl:with-param name="input" select="$pos"/>
          <xsl:with-param name="n" select="6"/>
        </xsl:call-template>
      </xsl:if>

      <xsl:text>  </xsl:text>
      <xsl:choose>
        <xsl:when test="$n=1">
          <xsl:call-template name="PacketEPD_Str_1"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="PacketEPD_Str_2"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

<!-- ======================================================================= -->
<!--  ������ ������� EPD-����� (����� �������)  -->
  <xsl:template name="PacketEPD_Str_1">
    <xsl:for-each select="@EDNo">
      <xsl:call-template name="LeftPad">
        <xsl:with-param name="input" select="."/>
        <xsl:with-param name="n" select="9"/>
      </xsl:call-template>
    </xsl:for-each>
    <xsl:text> </xsl:text>
    <xsl:text> </xsl:text>
    <xsl:text> </xsl:text>
    <xsl:text> </xsl:text>
    <xsl:for-each select="ed:Payer">
        <xsl:for-each select="ed:Bank">
            <xsl:for-each select="@BIC"><xsl:value-of select="."/></xsl:for-each>
            <xsl:text> </xsl:text>
        </xsl:for-each>
        <xsl:text> </xsl:text>
        <xsl:choose>
            <xsl:when test="@PersonalAcc"><xsl:value-of select="@PersonalAcc"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
        </xsl:choose>
      <xsl:text> </xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:MemoOrderPayer">
      <xsl:value-of select="@BIC"/>
      <xsl:text> </xsl:text>
      <xsl:text> </xsl:text>
      <xsl:value-of select="@Acc"/>
      <xsl:text> </xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:PayerBrf">
        <xsl:for-each select="ed:Bank">
            <xsl:for-each select="@BIC"><xsl:value-of select="."/></xsl:for-each>
            <xsl:text> </xsl:text>
        </xsl:for-each>
        <xsl:text> </xsl:text>
        <xsl:choose>
            <xsl:when test="@PersonalAcc"><xsl:value-of select="@PersonalAcc"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
        </xsl:choose>
        <xsl:text> </xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:InstructingAgent">
       <xsl:value-of select="@BIC"/>
       <xsl:text> </xsl:text>
       <xsl:text> </xsl:text>
       <xsl:value-of select="concat('                    ','')"/>
       <xsl:text> </xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:Payee">
      <xsl:text> </xsl:text>
      <xsl:choose>
        <xsl:when test="@PersonalAcc"><xsl:value-of select="@PersonalAcc"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>

    <xsl:for-each select="ed:MemoOrderPayee">
  <!--        <xsl:text> </xsl:text>         -->
  <!--        <xsl:value-of select="@BIC"/>  -->
      <xsl:text> </xsl:text>
      <xsl:value-of select="@Acc"/>
    </xsl:for-each>

    <xsl:for-each select="ed:PayeeBrf">
      <xsl:text> </xsl:text>
      <xsl:choose>
        <xsl:when test="@PersonalAcc"><xsl:value-of select="@PersonalAcc"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>

    <xsl:for-each select="ed:InstructedAgent">
        <xsl:text> </xsl:text>
        <xsl:value-of select="concat('                    ','')"/>
    </xsl:for-each>


    <xsl:for-each select="@Sum"><xsl:call-template name="sum"/></xsl:for-each>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

<!-- ======================================================================= -->
<!--  ������ ������� EPD-����� (������� �������)  -->
  <xsl:template name="PacketEPD_Str_2">
    <xsl:value-of select="local-name(.)"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="@TransKind"/>
    <xsl:text> </xsl:text>
    <xsl:for-each select="@EDNo">
      <xsl:call-template name="LeftPad">
        <xsl:with-param name="input" select="."/>
        <xsl:with-param name="n" select="9"/>
      </xsl:call-template>
    </xsl:for-each>
    <xsl:text> </xsl:text>
    <xsl:for-each select="@EDDate"><xsl:call-template name="date"/></xsl:for-each>
    <xsl:text> </xsl:text>
    <xsl:value-of select="@EDAuthor"/>
    <xsl:text> </xsl:text>
    <xsl:choose>
      <xsl:when test="local-name(.) ='ED111'">

    <!-- =================================================================== -->
    <!--    ��� ������������� ������  -->

        <xsl:for-each select="ed:InitialAccDoc/@AccDocNo">
          <xsl:call-template name="LeftPad">
            <xsl:with-param name="input" select="."/>
            <xsl:with-param name="n" select="6"/>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:text> </xsl:text>

        <xsl:for-each select="ed:InitialAccDoc/@AccDocDate">
          <xsl:call-template name="date"/>
        </xsl:for-each>

        <xsl:if test="not(contains(name(..),'EDCopy'))">
          <xsl:text> </xsl:text>
          <xsl:for-each select="@Sum"><xsl:call-template name="sum"/></xsl:for-each>
        </xsl:if>

        <xsl:for-each select="ed:MemoOrderPayer">
          <xsl:text> </xsl:text>
          <xsl:value-of select="@BIC"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="concat('                    ','')"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="@Acc"/>
        </xsl:for-each>

        <xsl:if test="contains(name(..),'EDCopy')">
          <xsl:text> </xsl:text>
          <xsl:for-each select="@Sum"><xsl:call-template name="sum"/></xsl:for-each>
        </xsl:if>

        <xsl:for-each select="ed:MemoOrderPayee">
          <xsl:text> </xsl:text>
          <xsl:value-of select="@BIC"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="concat('                    ','')"/>
          <xsl:text> </xsl:text>
          <xsl:value-of select="@Acc"/>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

      </xsl:when>
      <xsl:when test="local-name(.) ='ED110'">

    <!-- =================================================================== -->
    <!--    ��� ��  -->

        <xsl:for-each select="ed:AccDoc/@AccDocNo">
          <xsl:call-template name="LeftPad">
            <xsl:with-param name="input" select="."/>
            <xsl:with-param name="n" select="6"/>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:text> </xsl:text>
        <xsl:for-each select="ed:AccDoc/@AccDocDate"><xsl:call-template name="date"/></xsl:for-each>

        <xsl:if test="not(contains(name(..),'EDCopy'))">
          <xsl:text> </xsl:text>
          <xsl:for-each select="@Sum"><xsl:call-template name="sum"/></xsl:for-each>
        </xsl:if>

        <xsl:for-each select="ed:PayerBrf">
            <xsl:text> </xsl:text>
            <xsl:for-each select="ed:Bank">
                <xsl:for-each select="@BIC"><xsl:value-of select="."/></xsl:for-each>
                <xsl:text> </xsl:text>
                <xsl:choose>
                    <xsl:when test="@CorrespAcc"><xsl:value-of select="@CorrespAcc"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:text> </xsl:text>
            <xsl:choose>
                <xsl:when test="@PersonalAcc"><xsl:value-of select="@PersonalAcc"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>

        <xsl:if test="contains(name(..),'EDCopy')">
          <xsl:text> </xsl:text>
          <xsl:for-each select="@Sum"><xsl:call-template name="sum"/></xsl:for-each>
        </xsl:if>

        <xsl:for-each select="ed:PayeeBrf">
            <xsl:text> </xsl:text>
            <xsl:for-each select="ed:Bank">
                <xsl:for-each select="@BIC"><xsl:value-of select="."/></xsl:for-each>
                <xsl:text> </xsl:text>
                <xsl:choose>
                    <xsl:when test="@CorrespAcc"><xsl:value-of select="@CorrespAcc"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:text> </xsl:text>
            <xsl:choose>
                <xsl:when test="@PersonalAcc"><xsl:value-of select="@PersonalAcc"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when>
    <!-- =================================================================== -->

      <xsl:otherwise>
        <xsl:for-each select="ed:AccDoc/@AccDocNo">
          <xsl:call-template name="LeftPad">
            <xsl:with-param name="input" select="."/>
            <xsl:with-param name="n" select="6"/>
          </xsl:call-template>
        </xsl:for-each>
        <xsl:text> </xsl:text>
        <xsl:for-each select="ed:AccDoc/@AccDocDate"><xsl:call-template name="date"/></xsl:for-each>

        <xsl:if test="not(contains(name(..),'EDCopy'))">
          <xsl:text> </xsl:text>
          <xsl:for-each select="@Sum"><xsl:call-template name="sum"/></xsl:for-each>
        </xsl:if>

        <xsl:for-each select="ed:Payer">
            <xsl:text> </xsl:text>
            <xsl:for-each select="ed:Bank">
                <xsl:for-each select="@BIC"><xsl:value-of select="."/></xsl:for-each>
                <xsl:text> </xsl:text>
                <xsl:choose>
                    <xsl:when test="@CorrespAcc"><xsl:value-of select="@CorrespAcc"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:text> </xsl:text>
            <xsl:choose>
                <xsl:when test="@PersonalAcc"><xsl:value-of select="@PersonalAcc"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>

        <xsl:for-each select="ed:InstructingAgent">
            <xsl:text> </xsl:text>
            <xsl:for-each select="@BIC"><xsl:value-of select="."/></xsl:for-each>
            <xsl:text> </xsl:text>
            <xsl:choose>
                <xsl:when test="@CorrespAcc"><xsl:value-of select="@CorrespAcc"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
            </xsl:choose>
            <xsl:text> </xsl:text>
            <xsl:value-of select="concat('                    ','')"/>
        </xsl:for-each>

        <xsl:if test="contains(name(..),'EDCopy')">
          <xsl:text> </xsl:text>
          <xsl:for-each select="@Sum"><xsl:call-template name="sum"/></xsl:for-each>
        </xsl:if>


        <xsl:for-each select="ed:Payee">
          <xsl:text> </xsl:text>
          <xsl:for-each select="ed:Bank">
            <xsl:for-each select="@BIC"><xsl:value-of select="."/></xsl:for-each>
            <xsl:text> </xsl:text>
            <xsl:choose>
              <xsl:when test="@CorrespAcc"><xsl:value-of select="@CorrespAcc"/></xsl:when>
              <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
          <xsl:text> </xsl:text>
          <xsl:choose>
            <xsl:when test="@PersonalAcc"><xsl:value-of select="@PersonalAcc"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>

        <xsl:for-each select="ed:InstructedAgent">
            <xsl:text> </xsl:text>
            <xsl:for-each select="@BIC"><xsl:value-of select="."/></xsl:for-each>
            <xsl:text> </xsl:text>
            <xsl:choose>
                <xsl:when test="@CorrespAcc"><xsl:value-of select="@CorrespAcc"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="concat('                    ','')"/></xsl:otherwise>
            </xsl:choose>
            <xsl:text> </xsl:text>
            <xsl:value-of select="concat('                    ','')"/>
        </xsl:for-each>


        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>