<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED211  -->
<!-- ======================================================================= -->
<!-- ������� �� �����   -->
  <xsl:template match="ed:ED211">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="ED211"/>
  </xsl:template>

<!-- ======================================================================= -->
<!--  ������� �� ����� (����� ������������� � �������������)  -->
  <xsl:template name="ED211">
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:if test="@AbstractKind = 0"><xsl:text>������������� ������� /ED211/</xsl:text></xsl:if>
      <xsl:if test="@AbstractKind = 1"><xsl:text>������������� ������� /ED211/</xsl:text></xsl:if>
      <xsl:if test="@AbstractKind = 2"><xsl:text>������� ������� �� ����� /ED211/</xsl:text></xsl:if>
      <xsl:if test="@AbstractKind = 3"><xsl:text>��������� ������� /ED211/</xsl:text></xsl:if>
      <xsl:text>  �� �����   </xsl:text><xsl:value-of select="@Acc"/>
      <xsl:text>   ����  </xsl:text>
      <xsl:for-each select="@AbstractDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:call-template name="PriznGr"/>

      <xsl:if test="ed:InitialED/@EDNo != ''">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:apply-templates select="ed:InitialED"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:choose> <xsl:when test="ed:PartInfo">
          <xsl:apply-templates select="ed:PartInfo"/>
      </xsl:when> </xsl:choose>

      <xsl:if test="@ReservedSum != ''">
          <xsl:text>����� ��������������� �������       : </xsl:text>
          <xsl:for-each select="@ReservedSum"><xsl:call-template name="fsum"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@CreditLimitSum != ''">
          <xsl:text>����� ������ �������������� ������� : </xsl:text>
          <xsl:for-each select="@CreditLimitSum"><xsl:call-template name="fsum"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@EnterBal != ''">
          <xsl:text>�������� �������  : </xsl:text>
          <xsl:for-each select="@EnterBal"><xsl:call-template name="fsum"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@DebetSum != ''">
          <xsl:text>����� �����       : </xsl:text>
          <xsl:for-each select="@DebetSum"><xsl:call-template name="fsum"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@CreditSum != ''">
          <xsl:text>������ �����      : </xsl:text>
          <xsl:for-each select="@CreditSum"><xsl:call-template name="fsum"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@OutBal != ''">
          <xsl:text>��������� ������� : </xsl:text>
          <xsl:for-each select="@OutBal"><xsl:call-template name="fsum"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@AbstractKind != 0">
          <xsl:text>������ ������������ �������       : </xsl:text>
          <xsl:if test="@BeginTime != ''"><xsl:value-of select="@BeginTime"/></xsl:if>
          <xsl:text> </xsl:text>
          <xsl:if test="@EndTime != ''"><xsl:value-of select="@EndTime"/></xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@LastMovetDate != 0">
          <xsl:text>���� ���������� �������� �� ����� : </xsl:text>
          <xsl:for-each select="@LastMovetDate"><xsl:call-template name="date"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@TransDate != 0">
          <xsl:text>���� ���������� �������� �� ����� : </xsl:text>
          <xsl:for-each select="@TransDate"><xsl:call-template name="date"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="@InquirySession != ''">
          <xsl:text>����� �����                       : </xsl:text> <xsl:value-of select="@InquirySession"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@ArrestSum != ''">
          <xsl:text>����� ������                      : </xsl:text>
          <xsl:for-each select="@ArrestSum"><xsl:call-template name="fsum"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="count(ed:TransInfo) != 0">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text> ��/�  �/�    �  ��    ���   �������.      ����                 ����          ���        �����              �      ����       ���   </xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>             ��      �������. ����     �����������           ����������       ��������                     ���     ���        ��� </xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:TransInfo">
              <!-- <xsl:sort order="descending" select="@AccDocNo"/> -->
              <xsl:text> </xsl:text>
              <xsl:value-of select="concat(substring('      ',1,6-string-length(position())),position())"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="@DC"/>
              <xsl:text> </xsl:text>
              <xsl:choose>
                  <xsl:when test="@AccDocNo">
                      <xsl:for-each select="@AccDocNo">
                        <xsl:call-template name="LeftPad">
                          <xsl:with-param name="input" select="."/>
                          <xsl:with-param name="n" select="6"/>
                        </xsl:call-template>
                      </xsl:for-each>
                    </xsl:when>
                  <xsl:otherwise><xsl:text>&#32;&#32;&#32;</xsl:text></xsl:otherwise>
              </xsl:choose>
              <xsl:text> </xsl:text>
              <xsl:value-of select="@TransKind"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="@BICCorr"/>
              <xsl:text> </xsl:text>
              <xsl:choose>
                  <xsl:when test="@CorrAccBrf">
                      <xsl:for-each select="@CorrAccBrf">
                          <xsl:value-of select="concat(substring('     ',1,5-string-length(current())),current())"/>
                      </xsl:for-each>
                  </xsl:when>
                  <xsl:otherwise><xsl:value-of select="concat('     ','')"/></xsl:otherwise>
              </xsl:choose>

<!--              <xsl:value-of select="concat('     ','')"/> -->
              <xsl:text> </xsl:text>
              <xsl:value-of select="@PayerPersonalAcc"/>
              <xsl:text> </xsl:text>
              <xsl:for-each select="@PayeePersonalAcc">
                <xsl:call-template name="Pad">
                  <xsl:with-param name="input" select="."/>
                  <xsl:with-param name="n" select="20"/>
                </xsl:call-template>
              </xsl:for-each>
              <xsl:text> </xsl:text>
              <xsl:if test="@TurnoverKind  = 2">��</xsl:if>
              <xsl:if test="@TurnoverKind  != 2">OO</xsl:if>
              <xsl:text> </xsl:text>
              <xsl:for-each select="@Sum"><xsl:call-template name="sum"/></xsl:for-each>
              <xsl:text> </xsl:text>
              <xsl:text> </xsl:text>
              <xsl:for-each select="ed:EDRefID/@EDNo">
                <xsl:call-template name="LeftPad">
                  <xsl:with-param name="input" select="."/>
                  <xsl:with-param name="n" select="9"/>
                </xsl:call-template>
              </xsl:for-each>
              <xsl:text> </xsl:text>
              <xsl:for-each select="ed:EDRefID/@EDDate">
                      <xsl:value-of select="concat(substring(current(),9,2),'.',substring(current(),6,2),'.',substring(current(),1,4))"/>
              </xsl:for-each>
              <xsl:text> </xsl:text>
              <xsl:value-of select="ed:EDRefID/@EDAuthor"/>
              <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>