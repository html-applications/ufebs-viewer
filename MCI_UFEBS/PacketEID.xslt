<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!-- =============================================================================================================
      ����� �������������� ��
  ================================================================================================================== -->
  <xsl:template match="ed:PacketEID">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>����� �������������� ��</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:choose>
          <xsl:when test="@EDCreationTime">
              <xsl:text>����� ����������� �� : </xsl:text>
              <xsl:value-of select="@EDCreationTime"/>
              <xsl:text>&#13;&#10;</xsl:text>
          </xsl:when>
          <xsl:otherwise>
              <xsl:text>&#13;&#10;</xsl:text>
          </xsl:otherwise>
      </xsl:choose>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:if test="count(ed:ED208)!=0">
          <xsl:text>�������                : </xsl:text>
          <xsl:value-of select="count(ed:ED208[@ResultCode = 1])"/>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>�������� ����������    : </xsl:text>
          <xsl:value-of select="count(ed:ED208[@ResultCode = 2])"/>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>�� �������� ���������� : </xsl:text>
          <xsl:value-of select="count(ed:ED208[@ResultCode = 3])"/>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:text>��������: </xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:if test="count(ed:ED113) + count(ed:ED114)>0">
        <xsl:call-template name="ED273"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
  <!--      <xsl:for-each select="*[starts-with(local-name(.),'ED') and local-name(.) != 'ED113' and local-name(.) != 'ED114']"> -->
      <xsl:apply-templates select="*[starts-with(local-name(.),'ED') and local-name(.) != 'ED113' and local-name(.) != 'ED114']"/>
  <!--       <xsl:apply-templates select="."/>
      </xsl:for-each>
                              -->

  </xsl:template>

</xsl:stylesheet>