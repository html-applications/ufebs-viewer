<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

<!-- ======================================================================= -->
  <xsl:template match="ed:PacketEPD">
      <xsl:call-template name="PacketEPD">
        <xsl:with-param name="q" select="@EDQuantity"/>
      </xsl:call-template>
  </xsl:template>
<!-- ======================================================================= -->
  <xsl:template name="PacketEPD">
    <xsl:param name="q"/>

    <!-- ========= ��������� ����� ������� ================================= -->
    <xsl:call-template name="PacketEPD_Header_1">
      <xsl:with-param name="q" select="$q"/>
    </xsl:call-template>
    <!-- ========= ������ ����� ������� ==================================== -->
    <xsl:choose>
      <xsl:when test="$q !=''">
        <xsl:for-each select="*[starts-with(local-name(.),'ED')]">
          <xsl:sort order="ascending" select="concat(substring(ed:Payee/@PersonalAcc,1,8),substring(ed:Payee/@PersonalAcc,10,11),
            substring(ed:PayeeBrf/@PersonalAcc,1,8),substring(ed:PayeeBrf/@PersonalAcc,10,11),
            substring(ed:MemoOrderPayee/@Acc,1,8),substring(ed:MemoOrderPayee/@Acc,10,11))"/>
          <xsl:sort order="ascending" data-type="number" select="@Sum"/>

          <xsl:call-template name="ED_Tab">
            <xsl:with-param name="n">1</xsl:with-param>
            <xsl:with-param name="pos" select="position()"/>
          </xsl:call-template>

        </xsl:for-each>

      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="ED_Tab">
          <xsl:with-param name="n">1</xsl:with-param>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    <!-- ============�����================================================== -->

    <xsl:text>-----------------------------------------------------------------------------------------------</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:if test="$q != ''">
      <xsl:text>����� :                                                                   </xsl:text>
      <xsl:for-each select="@Sum">
        <xsl:call-template name="sum"/>
      </xsl:for-each>
    </xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>........................................................................................................................................................................................</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>

    <!-- ========== ��������� ������� ������� ============================== -->

    <xsl:call-template name="PacketEPD_Header_2">
      <xsl:with-param name="q" select="$q"/>
    </xsl:call-template>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:choose>
      <xsl:when test="$q !=''">
    <!-- ========= ������ �������  ������� ================================= -->
        <xsl:for-each select="*[starts-with(local-name(.),'ED')]">
          <xsl:call-template name="ED_Tab">
            <xsl:with-param name="pos" select="position()"/>
          </xsl:call-template>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="ED_Tab"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#13;&#10;</xsl:text>


  </xsl:template>

<!-- ======================================================================= -->
<!--  ��������� ������� EPD-����� (����� �������)  -->
  <xsl:template name="PacketEPD_Header_1">
    <xsl:param name="q"/>

    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:if test="$q !=''">
      <xsl:text>����� ���</xsl:text>
    </xsl:if>

    <xsl:call-template name="PriznGr"/>
    <xsl:if test="$q != ''">
        <xsl:text>���������� ��� � ������    : </xsl:text><xsl:value-of select="@EDQuantity"/>
        <xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>
    <xsl:text>������� ������� ���������  : </xsl:text>
    <xsl:value-of select="@SystemCode			"/>
<!--     <xsl:if test="@SystemCode = 01"><xsl:text> (�����������)</xsl:text></xsl:if> -->
    <xsl:if test="@SystemCode = 02"><xsl:text> (������ ���������� ��������)</xsl:text></xsl:if>
    <xsl:if test="@SystemCode = 05"><xsl:text> (������ �������� ��������)</xsl:text></xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:if test="$q != ''">
      <xsl:text>�������� : </xsl:text>
    </xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>-----------------------------------------------------------------------------------------------</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>  �        � ���       ���       ���� �����������    ���� ����������                   ����� </xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>-----------------------------------------------------------------------------------------------</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

<!-- ======================================================================= -->
<!--  ��������� ������� EPD-����� (������� �������). ����� ���������� ����� ��� �����������  -->
  <xsl:template name="PacketEPD_Header_2">
    <xsl:param name="q"/>
    <xsl:if test="$q !=''">
      <xsl:text>����� ���</xsl:text>
    </xsl:if>

    <xsl:call-template name="PriznGr"/>
    <xsl:if test="$q != ''">
        <xsl:text>���������� ��� � ������    : </xsl:text><xsl:value-of select="@EDQuantity"/>
        <xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>
    <xsl:if test="@Sum != ''">
        <xsl:text>����� ����� ��� � ������   : </xsl:text>
        <xsl:for-each select="@Sum">
          <xsl:call-template name="ffsum"/>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>
    <xsl:text>������� ������� ���������  : </xsl:text>
    <xsl:value-of select="@SystemCode			"/>
    <xsl:if test="@SystemCode = 02"><xsl:text> (������ ���������� ��������)</xsl:text></xsl:if>
    <xsl:if test="@SystemCode = 05"><xsl:text> (������ �������� ��������)</xsl:text></xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:call-template name="Session"/>
    <xsl:if test="$q != ''">
      <xsl:text>�������� : </xsl:text>
    </xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>  �     ���  ��       �     ����        ���         �     ����                                    � � � � � � � � � �                                    � � � � � � � � � �</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text> �/�     ��          ��      ��         ��          ��     ��           �����             ���          ��                   ��               ���             ��                   ��</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>

  </xsl:template>

</xsl:stylesheet>