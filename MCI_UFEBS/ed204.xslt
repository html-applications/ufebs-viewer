<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED204  -->
  <xsl:template match="ed:ED204">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>������ �� ������ ��� (������ ���) /ED204/</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>��� �������� �� ������ : </xsl:text><xsl:value-of select="@Code"/>
      <xsl:if test="@Code = 0"><xsl:text> (����� ���)</xsl:text></xsl:if>
      <xsl:if test="@Code = 1"><xsl:text> (����� ������ ���)</xsl:text></xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:call-template name="SystemCode">
        <xsl:with-param name="str" select="'������� ���������      : '"/>
      </xsl:call-template>

      <xsl:for-each select="@ParticipantAccount">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>���� ���������         : </xsl:text><xsl:value-of select="."/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:if test="count(ed:SPAccountOrIPBIC) > 0">
        <xsl:text>����������� ��� ��������� ���������&#13;&#10;</xsl:text>
        <xsl:text>--------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:SPAccountOrIPBIC">
          <xsl:text>��� ���������  : </xsl:text><xsl:value-of select="concat(@BIC,'   ')"/>
          <xsl:if test="@SPAccount !=''"><xsl:text>���� ��������� : </xsl:text><xsl:value-of select="@SPAccount"/></xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:if>


      <xsl:if test="ed:EDRefID/@EDNo != ''">
          <xsl:apply-templates select="ed:EDRefID"/>
      </xsl:if>
  </xsl:template>

</xsl:stylesheet>