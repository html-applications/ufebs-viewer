<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED422  -->
  <xsl:template match="ed:ED422">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>��������� ������/��������� ����� ������ �� &#13;&#10;</xsl:text>
      <xsl:text>�������������� ������� ����� ������ /ED422/</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>

      <xsl:text>����� ����������� ��                      : </xsl:text><xsl:value-of select="@EDTime"/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>����� ��������� ������                    : </xsl:text><xsl:value-of select="@OppReqNum"/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

      <xsl:text>��� ������� � ��������                    : </xsl:text>
      <xsl:value-of select="@OpRC"/>
      <xsl:if test="@OpRC = 1"><xsl:text> (������ �������� �������������� � ������ �����)</xsl:text></xsl:if>
      <xsl:if test="@OpRC = 2"><xsl:text> (������ �������� �������������� ��������)</xsl:text></xsl:if>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

      <xsl:text>�������� � �������&#13;&#10;</xsl:text>
      <xsl:text>=====================&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:CreditTerms">
        <xsl:for-each select="CreditOpDate">
          <xsl:text>���� ���������� ���������� �������� ���   :&#13;&#10;</xsl:text>
          <xsl:text>��������� �������� �� �������������       :&#13;&#10;</xsl:text>
          <xsl:text>���������� ������                         : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
          <xsl:text>����� �������                             : </xsl:text>
          <xsl:for-each select="@ApplicationSum"><xsl:call-template name="ffsum"/></xsl:for-each>
            <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
          </xsl:for-each>

        <xsl:for-each select="@GrantDate">
          <xsl:text>���� �������������� ������� ����� ������  : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>����, �� ������� ��������������� ������   :&#13;&#10;</xsl:text>
        <xsl:text>� ����������� ����                        : </xsl:text><xsl:value-of select="@ReqCredTerm"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>���������� ������ �� �������              : </xsl:text><xsl:value-of select="@ApplicationRate"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>��������������� ����� ����.�����������    : </xsl:text><xsl:value-of select="@OrgRegNum"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>��� ����� �������� ��������� ����� ��     : </xsl:text><xsl:value-of select="@BIC"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

        <xsl:text>����� ��������� ����� ��                  : </xsl:text><xsl:value-of select="@CorrAcc"/>
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>

      </xsl:for-each>
      <xsl:text>=====================&#13;&#10;</xsl:text>
      <xsl:if test="ed:EDRefID/@EDNo != ''">
          <xsl:apply-templates select="ed:EDRefID"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:apply-templates select="ed:ED421"/>
  </xsl:template>

</xsl:stylesheet>