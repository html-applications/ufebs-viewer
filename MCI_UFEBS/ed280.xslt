<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������). -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED280  -->
  <xsl:template match="ed:ED280">
      <xsl:param name="pos" select="position()-$modif_pos"/>
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat($pos,'.  ')"/>
      <xsl:text>��������� � ��������� �� /ED280/</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>����� ����������� ��  : </xsl:text><xsl:value-of select="@EDCreationTime"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��� ��, �������������&#13;&#10;</xsl:text>
      <xsl:text>�������� ����������   : </xsl:text><xsl:value-of select="@OrgBIC"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��� ���, ���������&#13;&#10;</xsl:text>
      <xsl:text>�������� ����������   : </xsl:text><xsl:value-of select="@BICPBR"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:if test="ed:NameClient != ''">
        <xsl:text>������-�����������    : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="ed:NameClient"/>
          <xsl:with-param name="width" select="60"/>
          <xsl:with-param name="before" select="string('                      : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="ed:InitialED/@EDNo != ''">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:apply-templates select="ed:InitialED"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
  </xsl:template>
</xsl:stylesheet>
