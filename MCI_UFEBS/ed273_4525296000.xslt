<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).  -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

<!--       ED273 -->
  <xsl:template name="ED273">
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>������������ �� ������ ���������� ���������&#13;&#10;</xsl:text>
      <xsl:text>� ��������� ���������� /ED113, ED114/</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>  �     ���  ��       �     ����        ���         �     ����                                    � � � � � � � � � �                                    � � � � � � � � � �</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text> �/�     ��          ��      ��         ��          ��     ��           �����             ���          ��                   ��               ���             ��                   ��</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="*[local-name(.)='ED113' or local-name(.)='ED114']">
          <xsl:call-template name="ED_Tab">
            <xsl:with-param name="ed" select="."/>
            <xsl:with-param name="pos" select="position()"/>
          </xsl:call-template>
      </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>