<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI.
     ��������� ��� ����������� ������� �������������� (���=044525296)
     ������� �� MCI_UFEBS.xslt :
       ED_Tab
       ED101-ED114
       PacketEPD
       ED211
       ED273
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>
  <xsl:template match="/">
    <xsl:apply-templates select="ed:ED101"/>
    <xsl:apply-templates select="ed:ED103"/>
    <xsl:apply-templates select="ed:ED104"/>
    <xsl:apply-templates select="ed:ED105"/>
    <xsl:apply-templates select="ed:ED107"/>
    <xsl:apply-templates select="ed:ED108"/>
    <xsl:apply-templates select="ed:ED110"/>
    <xsl:apply-templates select="ed:ED111"/>
    <xsl:apply-templates select="ed:ED113"/>
    <xsl:apply-templates select="ed:ED114"/>
    <xsl:apply-templates select="ed:ED201"/>
    <xsl:apply-templates select="ed:ED202"/>
    <xsl:apply-templates select="ed:ED203"/>
    <xsl:apply-templates select="ed:ED204"/>
    <xsl:apply-templates select="ed:ED205"/>
    <xsl:apply-templates select="ed:ED206"/>
    <xsl:apply-templates select="ed:ED207"/>
    <xsl:apply-templates select="ed:ED208"/>
    <xsl:apply-templates select="ed:ED210"/>
    <xsl:apply-templates select="ed:ED211"/>
    <xsl:apply-templates select="ed:ED213"/>
    <xsl:apply-templates select="ed:ED214"/>
    <xsl:apply-templates select="ed:ED215"/>
    <xsl:apply-templates select="ed:ED217"/>
    <xsl:apply-templates select="ed:ED218"/>
    <xsl:apply-templates select="ed:ED219"/>
    <xsl:apply-templates select="ed:ED221"/>
    <xsl:apply-templates select="ed:ED222"/>
    <xsl:apply-templates select="ed:ED240"/>
    <xsl:apply-templates select="ed:ED241"/>
    <xsl:apply-templates select="ed:ED242"/>
    <xsl:apply-templates select="ed:ED243"/>
    <xsl:apply-templates select="ed:ED244"/>
    <xsl:apply-templates select="ed:ED245"/>
    <xsl:apply-templates select="ed:ED273"/>
    <xsl:apply-templates select="ed:ED274"/>
    <xsl:apply-templates select="ed:ED275"/>
    <xsl:apply-templates select="ed:ED276"/>
    <xsl:apply-templates select="ed:ED277"/>
    <xsl:apply-templates select="ed:ED280"/>
    <xsl:apply-templates select="ed:ED330"/>
    <xsl:apply-templates select="ed:ED462"/>
    <xsl:apply-templates select="ed:ED463"/>
    <xsl:apply-templates select="ed:ED464"/>
    <xsl:apply-templates select="ed:ED465"/>
    <xsl:apply-templates select="ed:ED999"/>
    <xsl:apply-templates select="ed:PacketEID"/>
    <xsl:apply-templates select="ed:PacketEPD"/>
    <xsl:apply-templates select="ed:PacketESID"/>
    <xsl:apply-templates select="ed:PacketCash"/>
    <xsl:apply-templates select="ed:ReestrED"/>
  </xsl:template>

  <xsl:template match="*">
  </xsl:template>

  <xsl:include href="ED_Tab_4525296000.xslt" />
  <xsl:include href="ED101_4525296000.xslt" />
  <xsl:include href="ED103_4525296000.xslt" />
  <xsl:include href="ED104_4525296000.xslt" />
  <xsl:include href="ED105_4525296000.xslt" />
  <xsl:include href="ED107_4525296000.xslt" />
  <xsl:include href="ED108_4525296000.xslt" />
  <xsl:include href="ED110_4525296000.xslt" />
  <xsl:include href="ED111_4525296000.xslt" />
  <xsl:include href="ED113_4525296000.xslt" />
  <xsl:include href="ED114_4525296000.xslt" />
  <xsl:include href="ED201.xslt" />
  <xsl:include href="ED202.xslt" />
  <xsl:include href="ED203.xslt" />
  <xsl:include href="ED204.xslt" />
  <xsl:include href="ED205.xslt" />
  <xsl:include href="ED206.xslt" />
  <xsl:include href="ED207.xslt" />
  <xsl:include href="ED208.xslt" />
  <xsl:include href="ED210.xslt" />
  <xsl:include href="ED211_4525296000.xslt" />
  <xsl:include href="ED213.xslt" />
  <xsl:include href="ED214.xslt" />
  <xsl:include href="ED215.xslt" />
  <xsl:include href="ED217.xslt" />
  <xsl:include href="ED218.xslt" />
  <xsl:include href="ED219.xslt" />
  <xsl:include href="ED221.xslt" />
  <xsl:include href="ED222.xslt" />
  <xsl:include href="ED240.xslt" />
  <xsl:include href="ED241.xslt" />
  <xsl:include href="ED242.xslt" />
  <xsl:include href="ED243.xslt" />
  <xsl:include href="ED244.xslt" />
  <xsl:include href="ED245.xslt" />
  <xsl:include href="ED273_4525296000.xslt" />
  <xsl:include href="ED274.xslt" />
  <xsl:include href="ED275.xslt" />
  <xsl:include href="ED276.xslt" />
  <xsl:include href="ED276.xslt" />
  <xsl:include href="ED277.xslt" />
  <xsl:include href="ED280.xslt" />
  <xsl:include href="ED330.xslt" />
  <xsl:include href="ED462.xslt" />
  <xsl:include href="ED463.xslt" />
  <xsl:include href="ED464.xslt" />
  <xsl:include href="ED465.xslt" />
  <xsl:include href="ED999.xslt" />
  <xsl:include href="PacketEPD_4525296000.xslt" />
  <xsl:include href="PacketESID.xslt" />
  <xsl:include href="PacketEID.xslt" />
  <xsl:include href="PacketCash.xslt" />
  <xsl:include href="ReestrED.xslt" />

  <xsl:include href="ED_Single.xslt" />
  <xsl:include href="ED8xx.xslt" />
  <xsl:include href="Common.xslt" />
  <xsl:include href="PPS.xslt" />

</xsl:stylesheet>
