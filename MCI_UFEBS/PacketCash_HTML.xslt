<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������). -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="html" encoding="WINDOWS-1251"/>

  <xsl:template match="ed:PacketCash">
      <xsl:for-each select="*[starts-with(local-name(.),'ED462')]">
        <xsl:call-template name="ED462"/>
        <xsl:if test="position() &lt; last()">
          <xsl:call-template name="next"/>
        </xsl:if>
      </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>