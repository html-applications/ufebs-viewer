<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED274  -->
  <xsl:template match="ed:ED274">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>����������� � ����������� ������ � ���������� ������������� �� ������ ����������� ���������,&#13;&#10;</xsl:text>
      <xsl:text>        ������������� �� ������ ���������� ���������� /ED274/ </xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:call-template name="InfoCode"/>

      <xsl:for-each select="@SumPT">
          <xsl:text>�����, ������������� � ������� : </xsl:text><xsl:call-template name="ffsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@AcptSum">
          <xsl:text>����� �������                  : </xsl:text><xsl:call-template name="ffsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@AcptDate">
        <xsl:text>���� ����������� ��������� �� ������� : </xsl:text>
        <xsl:call-template name="date"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:text>����� ��������� : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="ed:Annotation"/>
        <xsl:with-param name="width" select="70"/>
        <xsl:with-param name="before" select="string('                : ')"/>
      </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
  <!--

      <xsl:text>�������������� ��������� �� (ED273) : </xsl:text>
      <xsl:text>� </xsl:text><xsl:value-of select="ed:InitialED/@EDNo"/>
      <xsl:text> �� </xsl:text>
      <xsl:for-each select="ed:InitialED/@EDDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text> ��� : </xsl:text><xsl:value-of select="ed:InitialED/@EDAuthor"/>
      <xsl:text>&#13;&#10;</xsl:text>
 -->
      <xsl:text>�������������� ��������� ��� : </xsl:text>
      <xsl:text>� </xsl:text><xsl:value-of select="ed:EDRefID/@EDNo"/>
      <xsl:text> �� </xsl:text>
      <xsl:for-each select="ed:EDRefID/@EDDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text> ��� : </xsl:text><xsl:value-of select="ed:EDRefID/@EDAuthor"/>

      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>