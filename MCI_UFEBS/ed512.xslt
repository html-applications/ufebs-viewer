<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED512  -->
  <xsl:template match="ed:ED512">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>������������ ��� ���������� ����������� ������������� ���� ����� ������ /ED512/&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="ed:ChangeCOSDirDataInfo">
        <xsl:text>  ���������� � ����������� �����������&#13;&#10;</xsl:text>
        <xsl:text>  ------------------------------------&#13;&#10;</xsl:text>
        <xsl:text>    ��� ����������� �����������   : </xsl:text><xsl:value-of select="@DictionUpdateMode"/>
        <xsl:if test="@DictionUpdateMode = 1"><xsl:text> (����������)</xsl:text></xsl:if>
        <xsl:if test="@DictionUpdateMode = 2"><xsl:text> (��������)</xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:if test="count(ed:Counteragent) != 0">
          <xsl:text>    ������ ��� ������������ ED501 : </xsl:text>
          <xsl:for-each select="ed:Counteragent/@UIS">
            <xsl:if test="position() mod 5 = 0">
              <xsl:text>&#13;&#10;                                    </xsl:text>
            </xsl:if>
            <xsl:value-of select="concat(.,string(' '))"/>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>

        </xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:if test="count(ed:OURSWIFTBIC) != 0">
          <xsl:for-each select="ed:OURSWIFTBIC">
            <xsl:text>    �������������� ����������&#13;&#10;</xsl:text>
            <xsl:text>    -------------------------&#13;&#10;</xsl:text>
            <xsl:text>        ��� SWIFT   : </xsl:text><xsl:value-of select="@SWIFTBIC"/>
            <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
            <xsl:text>        ������ ��������������� ������������ ED503&#13;&#10;</xsl:text>
            <xsl:text>        -------------------------------------------------------------------------------------</xsl:text>
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:text>        ����.�������������      ���        ������ ����� ��������� SWIFT&#13;&#10;</xsl:text>
            <xsl:text>        ����������� ��         SWIFT&#13;&#10;</xsl:text>
            <xsl:text>        -------------------------------------------------------------------------------------</xsl:text>
            <!--                   ..........      ...........     ..... ..... ..... ..... ..... ..... ..... .....-->
            <xsl:text>&#13;&#10;</xsl:text>

            <xsl:for-each select="ed:SenderSWIFTBIC">
              <xsl:value-of select="concat(string('           '),@UIS,string('      '))"/>
              <xsl:call-template name="Center">
                <xsl:with-param name="input" select="@SWIFTBIC"/>
                <xsl:with-param name="n" select="11"/>
              </xsl:call-template>
              <xsl:value-of select="string('  ')"/>
              <xsl:if test="count(ed:SWIFTTypeList) != 0">
                <xsl:for-each select="ed:SWIFTTypeList/@SWIFTTypeNo">
                  <xsl:if test="position() mod 8 = 0">
                    <xsl:text>&#13;&#10;                                        </xsl:text>
                  </xsl:if>
                  <xsl:value-of select="concat(.,string(' '))"/>
                </xsl:for-each>
              </xsl:if>
              <xsl:text>&#13;&#10;</xsl:text>
            </xsl:for-each>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:if>
      </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
