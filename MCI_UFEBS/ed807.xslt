<?xml version="1.0" encoding="WINDOWS-1251"?>

<!-- ��������� ������ ����������� ��� (ED807) -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

<xsl:template match="ed:ED807">

  <xsl:text>==============   ���������� ���   (ED807) ===============&#13;&#10;&#13;&#10;</xsl:text>
  <xsl:call-template name="PriznGr"/>
  <xsl:text>&#13;&#10;</xsl:text>
  <xsl:text>���� � ����� �������� �� : </xsl:text><xsl:value-of select="@CreationDateTime"/>
  <xsl:text>&#13;&#10;</xsl:text>
  <xsl:for-each select="@CreationReason">
    <xsl:call-template name="CreationReason">
      <xsl:with-param name="str" select="'������� �������� ��      : '"/>
    </xsl:call-template>
  </xsl:for-each>
  <xsl:text>��� �������������        : </xsl:text><xsl:value-of select="@InfoTypeCode"/>
  <xsl:if test="@InfoTypeCode= 'FIRR'"><xsl:text> - ������ ���������� ��� </xsl:text></xsl:if>
  <xsl:if test="@InfoTypeCode= 'SIRR'"><xsl:text> - ��������� � ����������� ��� </xsl:text></xsl:if>

  <xsl:if test="ed:PartInfo">
    <xsl:apply-templates select="ed:PartInfo"/>
  </xsl:if>

  <xsl:text>&#13;&#10;</xsl:text>
  <xsl:if test="ed:InitialED/@EDNo != ''">
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:apply-templates select="ed:InitialED"/>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:if>
  <xsl:text>&#13;&#10;</xsl:text>


  <xsl:if test="count(ed:BICDirectoryEntry) != 0">
<!--    <xsl:value-of select="concat(position(),'.  ')"/> -->
    <xsl:apply-templates select="ed:BICDirectoryEntry">
      <xsl:sort order="ascending" select="@BIC"/>
    </xsl:apply-templates>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
