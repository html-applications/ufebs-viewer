<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="html" encoding="WINDOWS-1251"/>



<!--       ED273 -->
  <xsl:template match="ed:ED273">
  <html>
    <head>
    <style type="text/css"  charset="windows-1251">
      @media all
	{body	{font:9pt sans-serif,"Times New Roman","Arial", Verdana; margin:20px 20px;}
	  h1	{margin:20px 20px;font-size:5mm; }
	 .t	{border-style:solid none none none;}
	 .b	{border-style:none none solid none;}
	 .bt	{border-style:solid none solid none;}
	 .blt	{border-style:solid none solid solid;}
	 .br	{border-style:none solid solid none;}
	 .bl	{border-style:none none solid solid;}
	 .blr	{border-style:none solid solid solid;}
	 .l	{border-style:none none none solid;}
	 .lr	{border-style:none solid none solid;}

	 table	{border-collapse:collapse; /* ������� ������� ����� ����� �������� */
		border-style:none;
		}
	 td	{border: 1px;
	 	border-style:none none none none;
		font-size:3mm;
		}
	 .line	{
		border-bottom:1px dashed #000; /* ��������� ����� */
		width:180mm; height:20mm;    /* ������ ����� */
		}
	 span	{font-size:3.7mm;  margin-left:1mm;}
	 p.fl:first-letter {text-transform: uppercase;}
	 .nextpage  { /*display:none;*/}
	}
      @media print {
        .line      { display:none;}
        .nextpage  { display:block; page-break-after:always; }
        }
    </style>
    </head>

    <body  style="font-family:sans-serif;">
<!--  <h1 style="font-size:4mm; width:180mm;  align:left;">��������� ��� �������� ������������ �� ������ ���������� ��������� � ��������� ���������� /ED273/</h1>
      <p style="font-size:4mm;"><xsl:call-template name="PriznGr"/></p>
-->
      <xsl:for-each select="*[starts-with(local-name(.),'ED11')]">
        <xsl:apply-templates select="."/>

        <xsl:if test="position() &lt; last()">
        	<div class="line"></div>
          <p class="nextpage">&#160;</p>
        </xsl:if>

      </xsl:for-each>
      <xsl:if test="position() &lt; $count273">
        <div class="line"></div>
        <p class="nextpage">&#160;</p>
      </xsl:if>
    </body>

  </html>
  </xsl:template>

<!--       ED273 -->
  <xsl:template name="ED273">
  <html>
    <head>
    <style type="text/css"  charset="windows-1251">
      @media all
	{body	{font:9pt sans-serif,"Times New Roman","Arial", Verdana; margin:20px 20px;}
	  h1	{margin:20px 20px;font-size:5mm; }
	 .t	{border-style:solid none none none;}
	 .b	{border-style:none none solid none;}
	 .bt	{border-style:solid none solid none;}
	 .blt	{border-style:solid none solid solid;}
	 .br	{border-style:none solid solid none;}
	 .bl	{border-style:none none solid solid;}
	 .blr	{border-style:none solid solid solid;}
	 .l	{border-style:none none none solid;}
	 .lr	{border-style:none solid none solid;}

	 table	{border-collapse:collapse; /* ������� ������� ����� ����� �������� */
		border-style:none;
		}
	 td	{border: 1px;
	 	border-style:none none none none;
		font-size:3mm;
		}
	 .line	{
		border-bottom:1px dashed #000; /* ��������� ����� */
		width:180mm; height:20mm;    /* ������ ����� */
		}
	 span	{font-size:3.7mm;  margin-left:1mm;}
	 p.fl:first-letter {text-transform: uppercase;}
	 .nextpage  { /*display:none;*/}
	}
      @media print {
        .line      { display:none;}
        .nextpage  { display:block; page-break-after:always; }
        }
    </style>
    </head>

    <body  style="font-family:sans-serif;">
      <xsl:for-each select="*[local-name(.)='ED113' or local-name(.)='ED114']">

        <xsl:apply-templates select="."/>

        <xsl:if test="position() &lt; last()">
        	<div class="line"></div>
          <p class="nextpage">&#160;</p>
        </xsl:if>
      </xsl:for-each>
    </body>

  </html>
  </xsl:template>


</xsl:stylesheet>
