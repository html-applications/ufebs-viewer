<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED207  -->
  <xsl:template match="ed:ED207">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>��������� � ������ ��� /ED207/  </xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:call-template name="StatusCode"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:if test="@QuantityED != ''"><xsl:text>���������� ���  : </xsl:text><xsl:value-of select="@QuantityED"/></xsl:if>
      <xsl:if test="@Sum != ''">
          <xsl:text>  ����� : </xsl:text><xsl:for-each select="@Sum"><xsl:call-template name="summa"/></xsl:for-each>
      </xsl:if>
      <xsl:if test="@EDDayNo != ''">   <xsl:text>  ����� ����      : </xsl:text><xsl:value-of select="@EDDayNo"/></xsl:if>
      <xsl:if test="@MessageNo != ''"> <xsl:text>  ����� ��������� : </xsl:text><xsl:value-of select="@MessageNo"/></xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:if test="ed:InitialED/@EDNo != ''">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:apply-templates select="ed:InitialED"/>
      </xsl:if>
      <xsl:if test="@QuantityED != ''">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:for-each select="ed:EDInfo">
              <xsl:value-of select="concat('   ',position(),'.  ')"/>
              <xsl:text> </xsl:text>
              <xsl:for-each select="@FileDate"><xsl:call-template name="date"/></xsl:for-each>
              <xsl:text> </xsl:text>
              <xsl:for-each select="@Sum"><xsl:call-template name="sum"/></xsl:for-each>
              <!--
              <xsl:if test="ed:EDRefID/@EDNo != ''">
                  <xsl:text>&#13;&#10;</xsl:text>
                  <xsl:apply-templates select="ed:EDRefID"/>
              </xsl:if>
              -->
              <xsl:if test="ed:EDRefID/@EDNo != ''">
                <xsl:text> </xsl:text>
                <xsl:text> �������� �� : </xsl:text>
                <xsl:text>� </xsl:text><xsl:value-of select="ed:EDRefID/@EDNo"/>
                <xsl:text> �� </xsl:text>
                <xsl:for-each select="ed:EDRefID/@EDDate"><xsl:call-template name="date"/></xsl:for-each>
                <xsl:text> ���: </xsl:text><xsl:value-of select="ed:EDRefID/@EDAuthor"/>
              </xsl:if>
              <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>