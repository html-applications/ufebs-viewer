<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ����� ��������� ��� �� ���
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"

  xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <xsl:template name="SystemCode">
    <xsl:param name="str" select="'������� ���������          : '"/>
    <xsl:for-each select="@SystemCode">
      <xsl:value-of select="concat($str,.)"/>
      <xsl:if test=".='02'"><xsl:text> (������ ���������� ��������)</xsl:text></xsl:if>
      <xsl:if test=".='05'"><xsl:text> (������ �������� ��������)</xsl:text></xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="SettleNotEarlier">
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:for-each select="ed:SettleNotEarlier">
      <xsl:text>��������� ������������ �� ����� </xsl:text>
      <xsl:choose>
        <xsl:when test="ed:SessionID !=''">
          <xsl:value-of select="ed:SessionID"/>
          <xsl:text> �����&#13;&#10;</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="ed:SettlementTime"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <xsl:template name="ProcessingDetails">
    <xsl:for-each select="ed:ProcessingDetails">
      <xsl:text>&#13;&#10;</xsl:text>
<!--       <xsl:text>������������� ������-��������, ������������ ��������&#13;&#10;</xsl:text>
      <xsl:text>=====================================================&#13;&#10;</xsl:text>
-->
      <xsl:if test="ed:BusinessScenario !=''">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>������������� ������-��������, ������������ �������� : </xsl:text>
        <xsl:value-of select="ed:BusinessScenario"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="@DebitDate !=''">
        <xsl:text>���� �������� �� �����     : </xsl:text>
        <xsl:for-each select="@DebitDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:if test="@CreditDate !=''">
        <xsl:text>���� ���������� �� ����    : </xsl:text>
        <xsl:for-each select="@CreditDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:call-template name="Session"/>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="Session">
    <xsl:for-each select="ed:Session">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>������������ ��������� � </xsl:text>
      <xsl:choose>
        <xsl:when test="ed:SessionID !=''">
          <xsl:value-of select="concat(ed:SessionID,' ����� ')"/>
        </xsl:when>
        <xsl:otherwise><xsl:value-of select="ed:SettlementTime"/></xsl:otherwise>
      </xsl:choose>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="@SessionType">
        <xsl:value-of select="concat('��� ����� : ',current(),' ')"/>
        <xsl:if test=".='URGN'"><xsl:text> (������� ����)</xsl:text></xsl:if>
        <xsl:if test=".='NURG'"><xsl:text> (��������� ����)</xsl:text></xsl:if>
        <xsl:if test=".='CONS'"><xsl:text> (����������������� ����)</xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="ReqSettlementDate">
    <xsl:for-each select="@ReqSettlementDate">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>����������� (���������) ����&#13;&#10;</xsl:text>
      <xsl:text>���������� ������������      : </xsl:text>
      <xsl:call-template name="date"/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>

  <!--  ����� ��������� ��� �� ��� ��� ��������� ���������� -->
  <xsl:template name="PPS">
    <xsl:param name="EDName" select="string('ED101')"/>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:if test="@PaymentPrecedence !=''">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��������� �������            : </xsl:text>
      <xsl:value-of select="@PaymentPrecedence"/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>

    <xsl:call-template name="ReqSettlementDate"/>

    <xsl:call-template name="SettleNotEarlier"/>

    <xsl:for-each select="ed:SettleNotLater">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��������� ������������ �� ������� </xsl:text>
      <xsl:value-of select="ed:SettlementTime"/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:ParticipantBusinessScenario">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>������������� ������-��������, ������������ ���������� : </xsl:text>
      <xsl:value-of select="."/>
      <xsl:if test=". = 00"> (��� ��������)</xsl:if>
      <xsl:if test=". = 01"> (��� �������� �� ������������� �������)</xsl:if>

      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:SettlementCondition">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��������� ������� ���������� ������������&#13;&#10;</xsl:text>
      <xsl:text>=========================================&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>��� ������� (�����) ���������� ������������  : </xsl:text>
      <xsl:value-of select="@ConditionCode"/>
      <xsl:if test="@ConditionCode = 00"> (��� ��������)</xsl:if>

      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:LiquidityReservationSchemeParameters">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��������� ������� ��� ����� "�������������� �����������  "&#13;&#10;</xsl:text>
        <xsl:text>---------------------------------------------------------&#13;&#10;</xsl:text>
        <xsl:if test="@ConfirmationMessageAuthor !=''">
          <xsl:text>��� ���������, ����������� ������������ &#13;&#10;</xsl:text>
          <xsl:text>������������ �� � ���������� ������� ���������� : </xsl:text>
          <xsl:value-of select="@ConfirmationMessageAuthor"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>

        <xsl:text>������� ����, ��� ��������� ������������ ������ &#13;&#10;</xsl:text>
        <xsl:text>���� ������������, ���� �������������� �����������&#13;&#10;</xsl:text>
        <xsl:text>�� ����� ���� ��������� ���������������       : &#13;&#10;</xsl:text>
        <xsl:value-of select="@CancelIfNotReserved"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="ed:LiquidityReservationSchemeParameters">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��������� ������� ��� ����� "�������������� �����������"&#13;&#10;</xsl:text>
        <xsl:text>---------------------------------------------------------&#13;&#10;</xsl:text>
        <xsl:if test="@ConfirmationMessageAuthor !=''">
          <xsl:text>��� ���������, ����������� ������������ &#13;&#10;</xsl:text>
          <xsl:text>������������ �� � ���������� ������� ���������� : </xsl:text>
          <xsl:value-of select="@ConfirmationMessageAuthor"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>

        <xsl:text>������� ����, ��� ��������� ������������ ������ &#13;&#10;</xsl:text>
        <xsl:text>���� ������������, ���� �������������� �����������&#13;&#10;</xsl:text>
        <xsl:text>�� ����� ���� ��������� ���������������       : &#13;&#10;</xsl:text>
        <xsl:value-of select="@CancelIfNotReserved"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="ed:CoverFundsSchemeParameters">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��������� ������� ��� ����� "���������� ���������� ������������&#13;&#10;</xsl:text>
        <xsl:text>� �������������� �������������� �������� �������"&#13;&#10;</xsl:text>
        <xsl:text>---------------------------------------------------------------&#13;&#10;</xsl:text>
        <xsl:if test="@ParticipantToBeNotified != ''">
          <xsl:text>��� ���������, �������� ������������ �����&#13;&#10;</xsl:text>
          <xsl:text>������������� ���������� ������������       : &#13;&#10;</xsl:text>
          <xsl:value-of select="@ParticipantToBeNotified"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>

        <xsl:for-each select="ed:CoverSum/@Min">
        <xsl:text>����������� �����, ���������� �����������   : </xsl:text>
        <xsl:call-template name="ffsum"/>
        </xsl:for-each>
        <xsl:for-each select="ed:CoverSum/@Max">
          <xsl:text>������������ �����, ���������� �����������   : </xsl:text>
          <xsl:call-template name="ffsum"/>
        </xsl:for-each>
      </xsl:for-each>

      <xsl:for-each select="ed:RCPSchemeParameters">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��������� ������� ��� ����� "���������� ������� ����������� �������" &#13;&#10;</xsl:text>
        <xsl:text>--------------------------------------------------------------------&#13;&#10;</xsl:text>
        <xsl:text>������ ������� ��������� �������      : </xsl:text>
        <xsl:for-each select="@BeginProcessingDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>����� ������� ��������� �������       : </xsl:text>
        <xsl:for-each select="@EndProcessingDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:if test="@ClearingCircuit != ''">
          <xsl:text>����������� �����,� ������� ���������&#13;&#10;</xsl:text>
          <xsl:text>������ ����������� �������            : &#13;&#10;</xsl:text>
          <xsl:value-of select="@ClearingCircuit"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>

    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:call-template name="ProcessingDetails"/>
    <xsl:call-template name="Session"/>

    <!--  ��� ED103, ED104   -->
    <xsl:if test="$EDName ='ED103' or $EDName ='ED104'">
      <xsl:if test="@IPBIC !=''">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��� ���������� ���������-����������� : </xsl:text>
        <xsl:value-of select="@IPBIC"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="ed:InitialED/@EDNo !=''">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:apply-templates select="ed:InitialED" />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="ed:ArrestID !=''">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>������������� ������ (�����������)  : </xsl:text>
        <xsl:value-of select="ed:ArrestID"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>