<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI ( ����� �� � ���).
     20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
     25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
     11-11-2014 ������ ����� 2.6.4 �� 15-12-2014
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates select="ed:ED501"/>
    <xsl:apply-templates select="ed:ED503"/>
    <xsl:apply-templates select="ed:ED508"/>
    <xsl:apply-templates select="ed:ED510"/>
    <xsl:apply-templates select="ed:ED511"/>
    <xsl:apply-templates select="ed:ED512"/>
    <xsl:apply-templates select="ed:ED530"/>
    <xsl:apply-templates select="ed:ED540"/>
    <xsl:apply-templates select="ed:ED541"/>
    <xsl:apply-templates select="ed:ED542"/>
    <xsl:apply-templates select="ed:ED573"/>
    <xsl:apply-templates select="ed:ED574"/>
    <xsl:apply-templates select="ed:ED599"/>
  </xsl:template>

  <xsl:template match="*">
  </xsl:template>

  <xsl:include href="ed501.xslt" />
  <xsl:include href="ed503.xslt" />
  <xsl:include href="ed508.xslt" />
  <xsl:include href="ed510.xslt" />
  <xsl:include href="ed511.xslt" />
  <xsl:include href="ed512.xslt" />
  <xsl:include href="ed530.xslt" />
  <xsl:include href="ed540.xslt" />
  <xsl:include href="ed541.xslt" />
  <xsl:include href="ed542.xslt" />
  <xsl:include href="ed573.xslt" />
  <xsl:include href="ed574.xslt" />
  <xsl:include href="ed599.xslt" />
  <xsl:include href="Common.xslt" />
</xsl:stylesheet>
