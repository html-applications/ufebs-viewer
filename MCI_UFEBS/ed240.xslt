<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED240  -->
  <xsl:template match="ed:ED240">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>������ ���������� � ����������/���������� �� /ED240/ </xsl:text>
      <xsl:call-template name="PriznGr"/>

      <xsl:text> ��� ������� ���������� : </xsl:text>
      <xsl:for-each select="@ExchangeTypeCode">
          <xsl:value-of select="."/>
          <xsl:if test=".=1"><xsl:text>  (���������� �������� �� ��� � ���(��)) </xsl:text></xsl:if>
          <xsl:if test=".=2"><xsl:text>  (���������� �������� �� ���(��) � ���) </xsl:text></xsl:if>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:if test="@EDTypeNo != ''">
          <xsl:text> ��� ��      :  </xsl:text><xsl:value-of select="@EDTypeNo"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="ed:Session/ed:SessionID != ''">
          <xsl:text> ����� ����� : </xsl:text><xsl:value-of select="ed:Session/ed:SessionID"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@ARMNo != ''">
          <xsl:text> ����� ���   :  </xsl:text><xsl:value-of select="@ARMNo"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@InquiryDate != ''">
        <xsl:text> ����, �� ������� ������������� �� : </xsl:text>
        <xsl:for-each select="@InquiryDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
  </xsl:template>

</xsl:stylesheet>