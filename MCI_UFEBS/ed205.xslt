<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED205  -->
  <xsl:template match="ed:ED205">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>��������� � ������� ��� (������ ���) /ED205/</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:call-template name="StatusStateCode"/>

      <xsl:if test="@Balance != ''">
          <xsl:text>����� ������� �������        : </xsl:text>
          <xsl:for-each select="@Balance"><xsl:call-template name="summa"/></xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@CtrlCode != ''">
          <xsl:text>��� ���������� ��������      : </xsl:text><xsl:value-of select="@CtrlCode"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="@CtrlTime != ''">
          <xsl:text>����� ���������� �������� �� (������ ��) : </xsl:text><xsl:value-of select="@CtrlTime"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
  <!--        <xsl:if test="@SessionID != '' and @SessionID != '0'">
          <xsl:text>����� �����, � ������� �� �������, ���� �������� � ������� : </xsl:text>
          <xsl:value-of select="@SessionID"/> <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
  -->
      <xsl:call-template name="ReqSettlementDate"/>
      <xsl:if test="ed:Annotation != ''">
          <xsl:text>����� ���������              : </xsl:text>
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="ed:Annotation"/>
            <xsl:with-param name="width" select="60"/>
            <xsl:with-param name="before" select="string('                             : ')"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="ed:ErrorDiagnostic != ''">
          <xsl:text>��������� ����������� ������ : </xsl:text>
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="ed:ErrorDiagnostic"/>
            <xsl:with-param name="width" select="60"/>
            <xsl:with-param name="before" select="string('                             : ')"/>
          </xsl:call-template>

          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:if test="count(ed:UnsatisfiedConditions) > 0">
        <xsl:text>������������� ������� ����������&#13;&#10;</xsl:text>
        <xsl:text>---------------------------------&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:UnsatisfiedConditions">
          <xsl:text>������� ��������� �� ������������� ������� : </xsl:text>
          <xsl:value-of select="@QueuingReasonCodeType"/>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:call-template name="SettleNotEarlier"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="count(ed:CancellationDetails) > 0">
        <xsl:text>���������� �� ������������� ������������&#13;&#10;</xsl:text>
        <xsl:text>-----------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:CancellationDetails">
          <xsl:text>������������ ���� � ����� ������������� : </xsl:text>
          <xsl:value-of select="@DateTime"/>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:for-each select="@ReasonCode">
            <xsl:text>��� ������� ��� �������������           : </xsl:text>
            <xsl:value-of select="."/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="count(ed:PartialPaymentInfo) > 0">
        <xsl:text>����������� � �������� ����������� ������������&#13;&#10;</xsl:text>
        <xsl:text>------------------------------------------------&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:PartialPaymentInfo">
          <xsl:for-each select="@InitialPaymentSum">
            <xsl:text>����� ��������������� ������������          : </xsl:text>
            <xsl:call-template name="ffsum"/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:for-each select="@DebtSum">
            <xsl:text>���������� ����� ������������ ������������� : </xsl:text>
            <xsl:call-template name="ffsum"/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:if>

      <xsl:if test="ed:InitialED/@EDNo != ''">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:apply-templates select="ed:InitialED"/>
      </xsl:if>
      <xsl:if test="ed:EDRefID/@EDNo != ''">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:apply-templates select="ed:EDRefID"/>
      </xsl:if>
      <xsl:call-template name="Session"/>
  </xsl:template>

</xsl:stylesheet>