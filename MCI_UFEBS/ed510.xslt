<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 25-05-2015 ������ ����� 2.6.8 �� 29-06-2015
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED510  -->
  <xsl:template match="ed:ED510">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>���������� �� �������, ��������������� ������ ������ ������������ ���� /ED510/&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>����� �������              : </xsl:text><xsl:value-of select="@RegisterNo"/>
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text>����� ����� �����          : </xsl:text>
      <xsl:for-each select="@TotalSum"><xsl:call-template name="ffsum"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>�� ��������������� ������&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:if test="count(ed:PayServicesCodeList) != 0">
        <xsl:text>                   ������ ��������������� ����� �� �����&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>--------------------------------------------------------------------------------&#13;&#10;</xsl:text>
        <xsl:text>    ������     :    ���      :   ����     :   ���-��  :         ����� (� ���.)&#13;&#10;</xsl:text>
        <xsl:text>               :   SWIFT     :  ������    :   �����   :         ����� (� ���.)&#13;&#10;</xsl:text>
        <xsl:text>---------------:-------------:------------:-----------:-------------------------&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:PayServicesCodeList">
            <xsl:call-template name="PayServicesCodeList">
        <!--      <xsl:with-param name="pos" select="position()"/> -->
            </xsl:call-template>
        </xsl:for-each>

      </xsl:if>
  </xsl:template>

  <xsl:template name="PayServicesCodeList">
    <!--  <xsl:param name="pos"/> -->

    <xsl:if test="@ServicesCode=1"> <xsl:value-of select="string('����������     : ')"/></xsl:if>
    <xsl:if test="@ServicesCode=2"> <xsl:value-of select="string('���������      : ')"/></xsl:if>

    <xsl:call-template name="Center">
      <xsl:with-param name="input" select="@ReceiverSWIFTBIC"/>
      <xsl:with-param name="n" select="11"/>
    </xsl:call-template>
    <xsl:value-of select="string(' : ')"/>

    <xsl:for-each select="@ServicesDate"><xsl:call-template name="date"/></xsl:for-each>
    <xsl:value-of select="string(' : ')"/>

    <xsl:call-template name="Center">
      <xsl:with-param name="input" select="@ServicesQuantity"/>
      <xsl:with-param name="n" select="9"/>
    </xsl:call-template>
    <xsl:value-of select="string(' :')"/>

    <xsl:for-each select="@ServicesRate">
      <xsl:call-template name="fsum"/>
    </xsl:for-each>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:if test="@ServicesCode=1"> <xsl:value-of select="string('���������      : ')"/></xsl:if>
    <xsl:if test="@ServicesCode=2"> <xsl:value-of select="string('������.������� : ')"/></xsl:if>
    <xsl:value-of select="string('            :            :           :')"/>
    <xsl:for-each select="@Sum">
      <xsl:call-template name="fsum">
      <!--  <xsl:call-template name="ffsum_center">
        <xsl:with-param name="width" select="24"/>   -->
      </xsl:call-template>
    </xsl:for-each>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>--------------------------------------------------------------------------------&#13;&#10;</xsl:text>
  </xsl:template>
</xsl:stylesheet>
