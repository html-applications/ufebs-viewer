<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � HTML-�������  � ���������� AstraKBR � AstraKBR_GUI (�������� �������).

     ��������� ������ : 30-10-2015 �������� ����� ED113 ED114 �� ED273 � Html-�������
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="html" encoding="WINDOWS-1251"/>

  <xsl:variable name="count273" select="count(//ed:ED273)"/>

  <xsl:template match="/">
    <xsl:apply-templates select="ed:ED101"/>
    <xsl:apply-templates select="ed:ED113"/>
    <xsl:apply-templates select="ed:ED114"/>
    <xsl:apply-templates select="ed:ED273"/>
    <xsl:apply-templates select="ed:ED462"/>
    <xsl:apply-templates select="ed:PacketEID"/>
  </xsl:template>

  <xsl:template match="*">
  </xsl:template>

  <xsl:include href="MCI_Params_HTML.xslt" />
  <xsl:include href="ED101_HTML.xslt" />
  <xsl:include href="ED113_HTML.xslt" />
  <xsl:include href="ED114_HTML.xslt" />
  <xsl:include href="ED273_HTML.xslt" />
  <xsl:include href="ED462_HTML.xslt" />
  <xsl:include href="PacketEID_HTML.xslt" />
  <xsl:include href="PacketCash_HTML.xslt" />
  <xsl:include href="Common_HTML.xslt" />
  <xsl:include href="��������_HTML.xslt" />
</xsl:stylesheet>
