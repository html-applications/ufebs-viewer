<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI  -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"

  xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--   ���   -->
  <xsl:template match="sen:SigEnvelope">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>���� � ������� SigEnvelope</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--dsig:SigValue-->
  <xsl:template match="dsig:SigValue" xmlns:dsig="urn:cbr-ru:dsig:v1.1">
      <!-- ��������� ��������� ������� � ������ �� ������� -->
  </xsl:template>

  <!--   ��������� �������  -->
  <xsl:template match="env:Envelope">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>��������� �������</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="env:Header/props:MessageInfo/props:To">
          <xsl:text>����� ����������                  : </xsl:text><xsl:value-of select="current()"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:text>����� �����������                 : </xsl:text><xsl:value-of select="env:Header/props:MessageInfo/props:From"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="env:Header/props:MessageInfo/props:MessageType">
          <xsl:text>��� ������������ ���������        : </xsl:text><xsl:value-of select="current()"/>
          <xsl:if test="current() = '1'"><xsl:text> (��������� ��������� �������) </xsl:text></xsl:if>
          <xsl:if test="current() = '2'"><xsl:text> (��� ������ ��������������� �������) </xsl:text></xsl:if>
          <xsl:if test="current() = '3'"><xsl:text> (��� ��������������� ���������-���������) </xsl:text></xsl:if>
          <xsl:if test="current() = '5'"><xsl:text> (������ ��������� ���������) </xsl:text></xsl:if>
      </xsl:for-each>

      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��� �����                         : </xsl:text>
      <xsl:value-of select="env:Header/props:MessageInfo/props:LegacyTransportFileName"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>���� � ����� �������� ������������� ��������� : </xsl:text>
      <xsl:value-of select="env:Header/props:MessageInfo/props:CreateTime"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="env:Header/props:DocInfo">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>����������������� ���������� </xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>-------------------------------</xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>������ ��������� : </xsl:text><xsl:value-of select="props:DocFormat"/>
        <xsl:if test="props:DocFormat = '1'"><xsl:text> (�����) </xsl:text></xsl:if>
        <xsl:if test="props:DocFormat = '2'"><xsl:text> (���������� ������ ��� ���) </xsl:text></xsl:if>
        <xsl:if test="props:DocFormat = '3'"><xsl:text> (����� ��, ���) </xsl:text></xsl:if>
        <xsl:if test="props:DocFormat = '9'"><xsl:text> (������) </xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��� ���������    : </xsl:text><xsl:value-of select="props:DocType"/>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:if test="props:DocFormat = '1'">
          <xsl:text>��������� ��������� �����</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>-------------------------------</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:for-each select="props:EDRefID">
            <xsl:text>����������� �������� � </xsl:text><xsl:value-of select="@EDNo"/>
            <xsl:text> �� </xsl:text>
            <xsl:for-each select="@EDDate"><xsl:call-template name="date"/></xsl:for-each>
            <xsl:text>  ��� : </xsl:text><xsl:value-of select="@EDAuthor"/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:if>
        <xsl:if test="props:DocID != ''">
          <xsl:text>������������� ������������� ��������� : </xsl:text><xsl:value-of select="props:DocID"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
      </xsl:for-each>
      <xsl:for-each select="env:Header/props:AcknowledgementInfo">
        <xsl:text>��� ���������                     : </xsl:text><xsl:value-of select="props:AcknowledgementType"/>
        <xsl:if test="props:AcknowledgementType = '1'"><xsl:text> (��������) </xsl:text></xsl:if>
        <xsl:if test="props:AcknowledgementType = '2'"><xsl:text> (����������� � ���� ��������������� ��� ��� ���������) </xsl:text></xsl:if>
        <xsl:if test="props:AcknowledgementType = '3'"><xsl:text> (��������� �����������-�����������) </xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��������� ������������ ��������   : </xsl:text><xsl:value-of select="props:ResultCode"/>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:if test="props:ResultText != ''">
          <xsl:text>�������� ���������� ��������      : </xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:value-of select="props:ResultText"/>
        </xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>

      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>


</xsl:stylesheet>


