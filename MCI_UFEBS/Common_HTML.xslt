<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 26-04-2018 ������ ����� 2018.3.0

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:bnkseek="bic20.uri" exclude-result-prefixes='bnkseek'
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">


  <xsl:output method="html" encoding="WINDOWS-1251"/>

  <!--   ���   -->
  <xsl:template match="sen:SigEnvelope">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>���� � ������� SigEnvelope</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--dsig:SigValue-->
  <xsl:template match="dsig:SigValue" xmlns:dsig="urn:cbr-ru:dsig:v1.1">
      <!-- ��������� ��������� ������� � ������ �� ������� -->
  </xsl:template>


  <!--   ��������� �������  -->
  <xsl:template match="env:Envelope">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>��������� �������</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="env:Header/props:MessageInfo/props:To">
          <xsl:text>����� ����������                  : </xsl:text><xsl:value-of select="current()"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:text>����� �����������                 : </xsl:text><xsl:value-of select="env:Header/props:MessageInfo/props:From"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="env:Header/props:MessageInfo/props:MessageType">
          <xsl:text>��� ������������ ���������        : </xsl:text><xsl:value-of select="current()"/>
          <xsl:if test="current() = '1'"><xsl:text> (��������� ��������� �������) </xsl:text></xsl:if>
          <xsl:if test="current() = '2'"><xsl:text> (��� ������ ��������������� �������) </xsl:text></xsl:if>
          <xsl:if test="current() = '3'"><xsl:text> (��� ��������������� ���������-���������) </xsl:text></xsl:if>
          <xsl:if test="current() = '5'"><xsl:text> (������ ��������� ���������) </xsl:text></xsl:if>
      </xsl:for-each>

      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��� �����                         : </xsl:text>
      <xsl:value-of select="env:Header/props:MessageInfo/props:LegacyTransportFileName"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>���� � ����� �������� ������������� ��������� : </xsl:text>
      <xsl:value-of select="env:Header/props:MessageInfo/props:CreateTime"/>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:for-each select="env:Header/props:DocInfo">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>����������������� ���������� </xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>-------------------------------</xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>������ ��������� : </xsl:text><xsl:value-of select="props:DocFormat"/>
        <xsl:if test="props:DocFormat = '1'"><xsl:text> (�����) </xsl:text></xsl:if>
        <xsl:if test="props:DocFormat = '2'"><xsl:text> (���������� ������ ��� ���) </xsl:text></xsl:if>
        <xsl:if test="props:DocFormat = '3'"><xsl:text> (����� ��, ���) </xsl:text></xsl:if>
        <xsl:if test="props:DocFormat = '9'"><xsl:text> (������) </xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��� ���������    : </xsl:text><xsl:value-of select="props:DocType"/>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:if test="props:DocFormat = '1'">
          <xsl:text>��������� ��������� �����</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>-------------------------------</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:for-each select="props:EDRefID">
            <xsl:text>����������� �������� � </xsl:text><xsl:value-of select="@EDNo"/>
            <xsl:text> �� </xsl:text>
            <xsl:for-each select="@EDDate"><xsl:call-template name="date"/></xsl:for-each>
            <xsl:text>  ��� : </xsl:text><xsl:value-of select="@EDAuthor"/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
        </xsl:if>
        <xsl:if test="props:DocID != ''">
          <xsl:text>������������� ������������� ��������� : </xsl:text><xsl:value-of select="props:DocID"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>
      </xsl:for-each>
      <xsl:for-each select="env:Header/props:AcknowledgementInfo">
        <xsl:text>��� ���������                     : </xsl:text><xsl:value-of select="props:AcknowledgementType"/>
        <xsl:if test="props:AcknowledgementType = '1'"><xsl:text> (��������) </xsl:text></xsl:if>
        <xsl:if test="props:AcknowledgementType = '2'"><xsl:text> (����������� � ���� ��������������� ��� ��� ���������) </xsl:text></xsl:if>
        <xsl:if test="props:AcknowledgementType = '3'"><xsl:text> (��������� �����������-�����������) </xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>��������� ������������ ��������   : </xsl:text><xsl:value-of select="props:ResultCode"/>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:if test="props:ResultText != ''">
          <xsl:text>�������� ���������� ��������      : </xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:value-of select="props:ResultText"/>
        </xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>

      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--   ���   -->
  <xsl:template match="sen:SigEnvelope">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>���� � ������� SigEnvelope</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>



  <!--  PartInfo  -->
  <xsl:template match="ed:PartInfo">
      <xsl:text>-------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>    ���������� � �����</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>    ����� �����       : </xsl:text> <xsl:value-of select="@PartNo"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>    ���������� ������ : </xsl:text> <xsl:value-of select="@PartQuantity"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>    ���������� ������������� ������������ ������ : </xsl:text> <xsl:value-of select="@PartAggregateID"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>-------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--  InitialED  -->
  <xsl:template match="ed:InitialED">
      <!--  <xsl:text>&#13;&#10;</xsl:text>  -->
      <xsl:text>�������������� ��������� ���� : </xsl:text>
      <xsl:text>� </xsl:text><xsl:value-of select="@EDNo"/>
      <xsl:text>  �� </xsl:text>
      <xsl:for-each select="@EDDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text>    ��� : </xsl:text><xsl:value-of select="@EDAuthor"/>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>


  <!--  EDRefID  -->
  <xsl:template match="ed:EDRefID">
      <!-- <xsl:text>&#13;&#10;</xsl:text>  -->
      <xsl:text>�������������� ��������� ��  : </xsl:text>
      <xsl:text>� </xsl:text><xsl:value-of select="@EDNo"/>
      <xsl:text>  �� </xsl:text>
      <xsl:for-each select="@EDDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text>    ���: </xsl:text><xsl:value-of select="@EDAuthor"/>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--  ����� ���������� ������ � ���� �� � NNNNNN �� ��.��.����  ��� : NNNNNNNNNN [(��� ��� ����� ������)]  [��� : NNNNNNNNNN]  -->

  <xsl:template name="PriznGr">
      <xsl:param name="CrLfBefore" select="1"/>
      <xsl:if test="$CrLfBefore = 1">
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:text>�� � </xsl:text><xsl:value-of select="@EDNo"/>
      <xsl:text>  �� </xsl:text><xsl:for-each select="@EDDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text>    ��� : </xsl:text><xsl:value-of select="@EDAuthor"/>
      <xsl:if test="@EDAuthor = 4583001999"><xsl:text> (��� ��� ����� ������)</xsl:text></xsl:if>
      <xsl:if test="@EDReceiver != ''">
          <xsl:text>  ��� : </xsl:text><xsl:value-of select="@EDReceiver"/>
      </xsl:if>
      <xsl:if test="@ActualReceiver != ''">
          <xsl:text>  ��� : </xsl:text><xsl:value-of select="@ActualReceiver"/>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--  ����� ������ ��� StatusCode ( ED203,ED207)  -->
  <xsl:template name="StatusCode">
    <xsl:for-each select="@StatusCode">
      <xsl:text>��� ������� ���           : </xsl:text><xsl:value-of select="."/>
      <xsl:if test=". = 00"> (��� ��������)</xsl:if>
      <xsl:if test=". = 01"> (��� �������� �� ������������� �������)</xsl:if>
      <xsl:if test=". = 02"> (��� �������� � ������� �� ������.� ���� ������������ � ���)</xsl:if>
      <xsl:if test=". = 04"> (��� ������������)</xsl:if>
      <xsl:if test=". = 06"> (��� ������ �� ������� �� ������.� ���� ������������ �� ������� ������� ���������� �������� �������)</xsl:if>
      <xsl:if test=". = 08"> (��� �������� �� ������� �� ������.� ���� ������������)</xsl:if>
      <xsl:if test=". = 09"> (��� ��������� �� ������� �� ������.� ���� ������������)</xsl:if>
      <xsl:if test=". = 11"> (��� �������� � ��������� ����������, ��������� �������)</xsl:if>
      <xsl:if test=". = 19"> (��� ������ �������� (������� �����))</xsl:if>
      <xsl:if test=". = 20"> (��� �� ������ ��������)</xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>

  <!--  ����� ������ ��� StatusStateCode ( ED205, ED215)  -->
  <xsl:template name="StatusStateCode">
    <xsl:for-each select="@StatusStateCode">
      <xsl:text>��� ������� ���              : </xsl:text><xsl:value-of select="."/>
      <xsl:if test=". = 00"> (��� ��������)</xsl:if>
      <xsl:if test=". = 01"> (��� �������� �� ������������� �������)</xsl:if>
      <xsl:if test=". = 02"> (��� �������� � ������� �� ������.� ���� ������������ � ���))</xsl:if>
      <xsl:if test=". = 04"> (��� ������������)</xsl:if>
      <xsl:if test=". = 06"> (��� ������ �� ��������� �� ������� ������� ���������� �������� �������)</xsl:if>
      <xsl:if test=". = 08"> (��� �������� �� ������� �� ������.� ���� ������������)</xsl:if>
      <xsl:if test=". = 09"> (��� ��������� �� ������� �� ������.� ���� ������������)</xsl:if>
      <xsl:if test=". = 10"> (��� ����������� (�� ����� ���� ��������))</xsl:if>
      <xsl:if test=". = 11"> (��� �������� � ��������� ����������, ��������� �������)</xsl:if>
      <xsl:if test=". = 19"> (��� ������ ��������)</xsl:if>
      <xsl:if test=". = 20"> (��� �� ������ ��������)</xsl:if>
      <xsl:if test=". = 21"> (����� ��� �� ������ ��������)</xsl:if>
      <xsl:if test=". = 30"> (����� ��� ������ ��������)</xsl:if>
      <xsl:if test=". = 31"> (����� ��� �����������)</xsl:if>
      <xsl:if test=". = 32"> (����� ��� �� ����� ���� �������/�����������)</xsl:if>
      <xsl:if test=". = 33"> (��� �������� ��� �������� ���������� ���)</xsl:if>
      <xsl:if test=". = 41"> (��� �������� � ������� ��� ���������� �� ��������� ��)</xsl:if>
      <xsl:if test=". = 42"> (��� �������� � ������� ������������, ����.���������� �� ���������� ��������)</xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>

  <!--  ����� ������ ��� StatusStateCode ( ED465)  -->
  <xsl:template name="StatusStateCode465">
    <xsl:for-each select="@StatusStateCode">
      <xsl:text>��� ������� ���              : </xsl:text><xsl:value-of select="."/>
      <xsl:if test=". = 01"> (���� ������������)</xsl:if>
      <xsl:if test=". = 02"> (���� �� ����� ���� ��������/������������)</xsl:if>
      <xsl:if test=". = 03"> (���� �� �������)</xsl:if>
      <xsl:if test=". = 04"> (���� �� ������ ��������)</xsl:if>
      <xsl:if test=". = 21"> (����� ���� �����������)</xsl:if>
      <xsl:if test=". = 22"> (����� ���� �� ����� ���� �������/�����������)</xsl:if>
      <xsl:if test=". = 23"> (����� ���� �� ������)</xsl:if>
      <xsl:if test=". = 24"> (����� ���� �� ������ ��������)</xsl:if>
      <xsl:if test=". = 41"> (�������� ������� ��� �������� ���������� ���)</xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>                                                                                                                                          x
  </xsl:template>


  <!--  ����� ������ ��� EDDefineRequestCode ( ED243,ED244)  -->
  <xsl:template name="EDDefineRequestCode">
    <xsl:for-each select="@EDDefineRequestCode">
      <xsl:text>��� ������� : </xsl:text><xsl:value-of select="."/>
      <xsl:if test=". = 00"> (����� �� ������� ������������)</xsl:if>
      <xsl:if test=". = 01"> (�������� ����� ����������� (��������) ����� ����������)</xsl:if>
      <xsl:if test=". = 02"> (�������� �������� ���������� ���������� �������)</xsl:if>
      <xsl:if test=". = 03"> (����������� ���������� ������������ ������������ � ���������� �����������)</xsl:if>
      <xsl:if test=". = 04"> (����������� ������������ �����������)</xsl:if>
      <xsl:if test=". = 05"> (����������� ������������ ����������)</xsl:if>
      <xsl:if test=". = 06">
        <xsl:text> (������������ ���������� � �����������-���.���� �� ������������ �  ����������� :&#13;&#10;</xsl:text>
        <xsl:text>                 ��� ���� ����� ����� ����������� ��� ����� ����������</xsl:text>
      </xsl:if>
      <xsl:if test=". = 07"> (����������� ���������� �������)</xsl:if>
      <xsl:if test=". = 08"> (����������� �������� ��������� ����� (60,61,101-110))</xsl:if>
      <xsl:if test=". = 09"> (�������� �������� ��������� (��������.) ��������� � ����� ������������ ������������)</xsl:if>
      <xsl:if test=". = 10"> (������ ������� �������� ��������. ����� � ��������� ��������� � ������� ������� � ��������)</xsl:if>
      <xsl:if test=". = 11"> (������ ������������� � ���������� �������� ������� ���������� �������)</xsl:if>
      <xsl:if test=". = 12"> (����������� �������� ���������� � ������ ������� � ������� �� ����.��������� �� ����� ����� � ��������)</xsl:if>
      <xsl:if test=". = 13">
        <xsl:text> (�������� �������� ��������� (��������.) ��������� ������ � �������&#13;&#10;</xsl:text>
        <xsl:text>                 �� ����� ������������� ����.��������� �� ����� ����� � ��������)</xsl:text>
      </xsl:if>
      <xsl:if test=". = 14"> (������ ����������, ��������� � ���������� � �������������� ��������� ����)</xsl:if>
      <xsl:if test=". = 15"> (�������� ��������� ��������� ����� � ��������� �������)</xsl:if>
      <xsl:if test=". = 16"> (�������� �������� ��������� (��������.) ��������� � ����� ������������ ��������� �����)</xsl:if>
      <xsl:if test=". = 99"> (������, ��� �������� �� �������������� ��������� ������������� ���� ������������� ����� �������)</xsl:if>x
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>

  <!--  ����� ������ ��� EDDefineAnswerCode (ED244)  -->
  <xsl:template name="EDDefineAnswerCode">
    <xsl:for-each select="@EDDefineAnswerCode">
      <xsl:text>��� ������  : </xsl:text><xsl:value-of select="."/>
      <xsl:if test=". = 01">
        <xsl:text> (������������ ���������� ���������� � ���������� ������������ ������������&#13;&#10;</xsl:text>
        <xsl:text>                 �� ����� ���� ������������, ����� �������� ��������)</xsl:text>
      </xsl:if>
      <xsl:if test=". = 02"> (���������� ���������� �������� ���������� ����������)</xsl:if>
      <xsl:if test=". = 03"> (������������ ���������� ������������ �� ������������ � ���������� �����������)</xsl:if>
      <xsl:if test=". = 04"> (�������������� (����������) ������������ �����������)</xsl:if>
      <xsl:if test=". = 05"> (�������������� (����������) ������������ ����������)</xsl:if>
      <xsl:if test=". = 06"> (�������� ���������� � �����������: ��� ���� ����� ����� ����������� ��� ����������)</xsl:if>
      <xsl:if test=". = 07"> (�������������� (����������) ���������� �������)</xsl:if>
      <xsl:if test=". = 08"> (�������� �������� ���� (�����) ���������� ��������� � ���������� �������� (60, 61, 101 � 110))</xsl:if>
      <xsl:if test=". = 09"> (������������� ���������� �������� ������� �� ���� ����������)</xsl:if>
      <xsl:if test=". = 10"> (��������� � ��������.����� ����.����� ���������� ������. �������� ����� ��������� ����������)</xsl:if>
      <xsl:if test=". = 11"> (�������� ���������� �������� ��������� (����������) ��������. � ���������� ��������)</xsl:if>
      <xsl:if test=". = 12">
        <xsl:text> (�������� ���������� �������� ���������� ������ ������� � ��������� �������&#13;&#10;</xsl:text>
        <xsl:text>                 �� ���������� ��������� �� ����� ����� � ��������)</xsl:text>
      </xsl:if>
      <xsl:if test=". = 13"> (����� �� ���������� �� ������� ���������� �������� ���������� ����� ���������� ��� �������)</xsl:if>
      <xsl:if test=". = 14"> (����� ����� ���������� �� ���������� �������� ������� ���������� �������)</xsl:if>
      <xsl:if test=". = 15"> (����� ����� ���������� (�������� �������� ���������� ������� � �������� �����))</xsl:if>
      <xsl:if test=". = 16"> (���������� �� �������, ���������� � ���������� � �������������� ��������� ����)</xsl:if>
      <xsl:if test=". = 17"> (�������� ��������� ��������� ����� � ��������� �������)</xsl:if>
      <xsl:if test=". = 18"> (�������� �����.�������� ���������� ��������� ����� � ����.�������)</xsl:if>
      <xsl:if test=". = 19"> (���������� ��������� �������� �� ����.� ������������ ����.����� ����������)</xsl:if>
      <xsl:if test=". = 99"> (�����, ��� �������� �� �������������� ��������� ������������� ���� ������������� ����� ������)</xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>

  <!--  ����� ������ ��� InfoCode (ED274)  -->
  <xsl:template name="InfoCode">
      <xsl:text>��� ����������� : </xsl:text><xsl:value-of select="@InfoCode"/>
      <xsl:if test="@InfoCode = 1">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text"
            select="string(' ������������� ��������� �������� ���������� ������������ (� ��� ����� ����������� ��������, ������� ��������, ������������ ������ ����������������), ���������� �� ������������� ����������� ���������, ���������� ����������')"/>
          <xsl:with-param name="width" select="70"/>
          <xsl:with-param name="before" select="string('                :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="@InfoCode = 2">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text"
            select="string(' ������������ ��������� ����������� (����������), �����-����������� (�����-����������), ������� �� ��������� ��������� ���������� ���������, ���� ��������� ���������� (��������, ����� �������� � ��������� �����������), ���������� �� ������������� ����������� ���������, ���������� ����������')"/>
          <xsl:with-param name="width" select="70"/>
          <xsl:with-param name="before" select="string('                :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="@InfoCode = 3">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text"
            select="string(' ����� �� ������� ������������, ���������� �� ������ �� ������� �����������, � �� ������������� ���������� ����������')"/>
          <xsl:with-param name="width" select="70"/>
          <xsl:with-param name="before" select="string('                :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="@InfoCode = 4">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text"
            select="string(' ��������� ����� �� ������� ���������� ���������� ������������, ���������� � ��������� ���������� ������� �����������')"/>
          <xsl:with-param name="width" select="70"/>
          <xsl:with-param name="before" select="string('                :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="@InfoCode = 5">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text"
            select="string(' ����������� ������� �� ����������� � ������������� ����, ���������� � ����������� ������� ����������� � �� ������������� ���������� ����������')"/>
          <xsl:with-param name="width" select="70"/>
          <xsl:with-param name="before" select="string('                :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="@InfoCode = 6">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text"
            select="string(' ��������������� �������� ������� �� ����� �����������, ���������� � ��������� ����������� ���������, ���������� ���������� � ������� �� ����������� � ���� ������������ (� ������������ � ����������[�� ������ ����� ������) � � ������ ����������� ���������, ���������� ���������� � ����������')"/>
          <xsl:with-param name="width" select="70"/>
          <xsl:with-param name="before" select="string('                :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="@InfoCode = 7">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text"
            select="string(' ������������ �������� ������� �� ����� �����������, ���������� �� ������������� ����������� ���������, ���������� ����������')"/>
          <xsl:with-param name="width" select="70"/>
          <xsl:with-param name="before" select="string('                :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="@InfoCode = 8">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text"
            select="string(' ������������� ���������� ���� �������� ������ � ���������� (������� ������������� ��������� �������� ������������� �������� ������� �� ����� �����������), ���������� � ������ ����������� ���������, ���������� ���������� � ����������')"/>
          <xsl:with-param name="width" select="70"/>
          <xsl:with-param name="before" select="string('                :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="@InfoCode = 9">
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text"
            select="string(' ������������� ��������� ������� �������� ������� �� ��������� ������������ � ������������� ���� �� ������� ��������������� �������� ������� �� ����������������� ����� (��������) ��������� ����������� (�� �������), �������� � ����� ������, ��������� ����������� ���������� � ��������� ����������� ��������� ������ ������ � ������� ������������� � ���� ������������ � � ������ ����������� ��������� � ����������')"/>
          <xsl:with-param name="width" select="70"/>
          <xsl:with-param name="before" select="string('                :   ')"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--  EDFieldList  (ED243,ED244) -->
  <xsl:template match="ed:EDFieldList">
    <xsl:text>� ��� ������������ ��������� : </xsl:text>
    <xsl:value-of select="ed:FieldNo"/><xsl:text>&#13;&#10;</xsl:text>
    <xsl:if test="ed:FieldValue != ''">
      <xsl:text>�������� ���������           : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="ed:FieldValue"/>
        <xsl:with-param name="width" select="70"/>
        <xsl:with-param name="before" select="string('                             : ')"/>
      </xsl:call-template>
    </xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--  EDReestrInfo  (ED243,ED244) -->
  <xsl:template match="ed:EDReestrInfo">
    <xsl:text>����� ������ � ������� : </xsl:text>
    <xsl:value-of select="@TransactionID"/><xsl:text>&#13;&#10;</xsl:text>
    <xsl:if test="count(ed:EDReestrFieldList) != 0">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>������ ���������� ������ �������</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>---------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:EDReestrFieldList">
        <xsl:text>� ��� ������������ ��������� : </xsl:text>
        <xsl:value-of select="ed:FieldNo"/><xsl:text>&#13;&#10;</xsl:text>
        <xsl:if test="ed:FieldValue != ''">
          <xsl:text>�������� ���������           : </xsl:text>
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="ed:FieldValue"/>
            <xsl:with-param name="width" select="70"/>
            <xsl:with-param name="before" select="string('                             : ')"/>
          </xsl:call-template>
        </xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="ed:FieldValue != ''">
      <xsl:text>�������� ���������         : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="ed:FieldValue"/>
        <xsl:with-param name="width" select="70"/>
        <xsl:with-param name="before" select="string('                           : ')"/>
      </xsl:call-template>
    </xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!-- ���������� "modif_pos" �������� ���-�� �����, �������������� ���������� ����� -->
  <xsl:variable name="modif_pos" select="0"/>
  <!--
  <xsl:variable name="modif_pos" select="count(//ed:PacketEPD/dsig:SigValue) + count(//ed:PacketESID/dsig:SigValue)
     + count(//ed:PacketEID/dsig:SigValue) + count(//ed:PacketESID/ed:InitialPacketED) +
     count(//ed:ED273/dsig:SigValue) + count(//ed:ED503/dsig:SigValue)" />
  -->
  <!-- ���������� "ED1xx" �������� ���-�� ��������� ���������� � ����� -->
  <!--

  <xsl:variable name="ED1xx" select="count(//ed:PacketEPD/@EDNo) + count(//ed:PacketESID/@EDNo) + count(//ed:ED215/@EDNo) + count(//ed:ED221/@EDNo) + count(//ed:ED273/@EDNo)" />

  <xsl:variable name="ED1xx" select="count(//ed:PacketEPD/ed:ED101) + count(//ed:PacketEPD/ed:ED103) +
    count(//ed:PacketEPD/ed:ED103) + count(//ed:PacketEPD/ed:ED104) + count(//ed:PacketEPD/ed:ED105) +
    count(//ed:PacketEPD/ed:ED108) + count(//ed:PacketEPD/ed:ED111)" />
  -->

  <!-- ���������� "EDReceiver" �������� EDReceiver ������ ��������� ����������-->
  <xsl:variable name="EDReceiver" select="substring(//ed:PacketEPD/@EDReceiver,1,7)" />

  <!-- ���������� "ED211_EDReceiver" �������� EDReceiver ������ ��������� ����������-->
  <xsl:variable name="ED211_EDReceiver" select="substring(//ed:ED211/@EDReceiver,1,7)" />

  <!--  �������������� ����� �� ������ � ������ "�����=�������"  -->
  <xsl:template name="sum">
    <xsl:param name="cep" select="string('=')"/>
    <xsl:param name="s" select="current()"/>
    <xsl:choose>
      <xsl:when test="string-length($s) >2">
        <xsl:value-of
            select = "concat(substring('                  ',1,18-string-length($s)),substring($s,1,
                       string-length($s)-2),$cep, substring($s,string-length($s)-1,2))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select = "concat('               0',$cep)"/>
        <xsl:call-template name="LeftPad0">
            <xsl:with-param name="input" select="$s"/>
            <xsl:with-param name="n" select="2"/>
        </xsl:call-template>

      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--  �������������� ����� �� ������ � ������ "�����=�������"  -->
  <!--  �������� �� ��������� ����� ��������� �� 18 ��������(?) -->
  <xsl:template name="summa">
    <xsl:param name="cep" select="string('=')"/>
    <xsl:param name="s" select="current()"/>
    <xsl:param name="rub_only" select="0"/>    <!-- ������� ������ ����� (�� ��������� ��������� (0)) -->
    <xsl:variable name="kopecks" select="substring($s,string-length($s)-1,2)" />
    <xsl:choose>
      <xsl:when test="string-length($s) >2">
        <xsl:value-of select = "substring($s,1,string-length($s)-2)"/>
        <xsl:if test="$rub_only=0 or $kopecks != string('00')">
          <xsl:value-of
              select = "concat($cep, substring($s,string-length($s)-1,2))"/>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select = "'0'"/>
        <xsl:value-of select = "string($cep)"/>
        <xsl:call-template name="LeftPad0">
            <xsl:with-param name="input" select="$s"/>
            <xsl:with-param name="n" select="2"/>
        </xsl:call-template>

      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!--  �������������� ����� �� ������ � ������ "�����.�������" (����� ��������������� � ������ �� 3 �����) -->
  <!--  �������� ��������� ����� ��������� �� 18 ��������(?) -->
  <xsl:decimal-format name="ru" decimal-separator='.' grouping-separator=' ' />
  <xsl:template name="fsum">
    <xsl:choose>
      <xsl:when test="string-length(current()) >2">
        <xsl:variable name="modif_sum"
          select="concat(format-number(substring(current(),1,string-length(current())-2),'### ##0','ru'),
            '=',substring(current(),string-length(current())-1,2))"/>
        <xsl:value-of select="concat(substring('                  ',1,18 - string-length($modif_sum)),$modif_sum)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select = "concat('              0','=')"/>
        <xsl:call-template name="LeftPad0">
            <xsl:with-param name="input" select="current()"/>
            <xsl:with-param name="n" select="2"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!--  �������������� ���� �� ������� ����-��-�� � ��.��.����  -->
  <!--  ����� ����� ����� ����������� ����� BEFORE. �� ��������� ������ ������

  <xsl:template name="date">
    <xsl:param name="before" select="string('')"/>
    <xsl:value-of select="concat(before,substring(current(),9,2),'.',substring(current(),6,2),'.',substring(current(),1,4))"/>
  </xsl:template>
  -->
  <!--  �������������� ���� �� ������� ����-��-�� � ��.��.����  -->
  <!--  ����� ����� ����� ����������� ����� BEFORE. �� ��������� ������ ������ -->
  <xsl:template name="date">
    <xsl:param name="before" select="string('')"/>
    <xsl:param name="d" select="current()"/>
    <xsl:value-of select="concat($before,substring($d,9,2),'.',substring($d,6,2),'.',substring($d,1,4))"/>
  </xsl:template>

  <!--  �������������� ���� �� ������� ����-��-�� � �� ����� ���� ���� -->
  <!--  ����� ����� ����� ����������� ����� BEFORE. �� ��������� ������ ������ -->
  <xsl:template name="fdate">
    <xsl:param name="d" select="current()"/>
    <xsl:variable name="mm" select="substring($d,6,2)"/>
    <xsl:value-of select="concat(substring($d,9,2),'&#160;')"/>
    <xsl:if test="$mm='01'"><xsl:value-of select="'������'"  /></xsl:if>
    <xsl:if test="$mm='02'"><xsl:value-of select="'�������'" /></xsl:if>
    <xsl:if test="$mm='03'"><xsl:value-of select="'�����'"   /></xsl:if>
    <xsl:if test="$mm='04'"><xsl:value-of select="'������'"  /></xsl:if>
    <xsl:if test="$mm='05'"><xsl:value-of select="'���'"     /></xsl:if>
    <xsl:if test="$mm='06'"><xsl:value-of select="'����'"    /></xsl:if>
    <xsl:if test="$mm='07'"><xsl:value-of select="'����'"    /></xsl:if>
    <xsl:if test="$mm='08'"><xsl:value-of select="'�������'" /></xsl:if>
    <xsl:if test="$mm='09'"><xsl:value-of select="'��������'"/></xsl:if>
    <xsl:if test="$mm='10'"><xsl:value-of select="'�������'" /></xsl:if>
    <xsl:if test="$mm='11'"><xsl:value-of select="'������'"  /></xsl:if>
    <xsl:if test="$mm='12'"><xsl:value-of select="'�������'" /></xsl:if>
    <xsl:value-of select="concat('&#160;',substring($d,1,4),'&#160;����')"   />
  </xsl:template>

  <xsl:template name="date_dd">
    <xsl:param name="d" select="current()"/>
    <xsl:value-of select="substring($d,9,2)"/>
  </xsl:template>

  <xsl:template name="date_mm">
    <xsl:param name="d" select="current()"/>
    <xsl:variable name="mm" select="substring($d,6,2)"/>
    <xsl:if test="$mm='01'"><xsl:value-of select="'������'"  /></xsl:if>
    <xsl:if test="$mm='02'"><xsl:value-of select="'�������'" /></xsl:if>
    <xsl:if test="$mm='03'"><xsl:value-of select="'�����'"   /></xsl:if>
    <xsl:if test="$mm='04'"><xsl:value-of select="'������'"  /></xsl:if>
    <xsl:if test="$mm='05'"><xsl:value-of select="'���'"     /></xsl:if>
    <xsl:if test="$mm='06'"><xsl:value-of select="'����'"    /></xsl:if>
    <xsl:if test="$mm='07'"><xsl:value-of select="'����'"    /></xsl:if>
    <xsl:if test="$mm='08'"><xsl:value-of select="'�������'" /></xsl:if>
    <xsl:if test="$mm='09'"><xsl:value-of select="'��������'"/></xsl:if>
    <xsl:if test="$mm='10'"><xsl:value-of select="'�������'" /></xsl:if>
    <xsl:if test="$mm='11'"><xsl:value-of select="'������'"  /></xsl:if>
    <xsl:if test="$mm='12'"><xsl:value-of select="'�������'" /></xsl:if>
  </xsl:template>

  <xsl:template name="date_yy">
    <xsl:param name="d" select="current()"/>
    <xsl:value-of select="substring($d,3,2)"/>
  </xsl:template>


  <!--  ������ ��� �������� �������� �����.

  <xsl:template name="Trim">
       <xsl:param name="input"/>
       <xsl:choose>
           <xsl:when test="contains($input, ' ')">
               <xsl:value-of select="substring-before($input, ' ')"/>
               <xsl:call-template name="Trim">
                   <xsl:with-param name="input" select="substring-after($input,' ')"/>
               </xsl:call-template>
           </xsl:when>
           <xsl:otherwise><xsl:value-of select="$input"/></xsl:otherwise>
       </xsl:choose>
  </xsl:template>
  -->

<!--  �������������� ����� �� ������ � ������ "�����=�������" (����� ��������������� � ������ �� 3 �����) -->
<!--  �������� �� ����������� ����� ��������� �� 18 ��������(!) -->
<xsl:decimal-format name="ru" decimal-separator='.' grouping-separator=' ' />
<xsl:template name="ffsum">
  <xsl:choose>
    <xsl:when test="string-length(current()) >2">
      <xsl:variable name="modif_sum"
        select="concat(format-number(substring(current(),1,string-length(current())-2),'### ##0','ru'),
          '=',substring(current(),string-length(current())-1,2))"/>
      <xsl:value-of select="$modif_sum"/>
    </xsl:when>
    <xsl:otherwise>
        <xsl:value-of select = "string('0=')"/>
        <xsl:call-template name="LeftPad0">
            <xsl:with-param name="input" select="current()"/>
            <xsl:with-param name="n" select="2"/>
        </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>



  <!--  ������ ��� ���������� �������� �����. -->
  <xsl:template name="LeftPad">
       <xsl:param name="input"/>
       <xsl:param name="n" select="25"/>
       <xsl:choose>
           <xsl:when test="string-length($input) &lt; $n">
               <xsl:call-template name="LeftPad">
                   <xsl:with-param name="input" select="concat(' ',$input)"/>
                   <xsl:with-param name="n" select="$n"/>
               </xsl:call-template>
           </xsl:when>
           <xsl:otherwise><xsl:value-of select="$input"/></xsl:otherwise>
       </xsl:choose>
  </xsl:template>

  <!--  ������ ��� ���������� ����� �����. -->
  <xsl:template name="LeftPad0">
       <xsl:param name="input"/>
       <xsl:param name="n" select="25"/>
       <xsl:choose>
           <xsl:when test="string-length($input) &lt; $n">
               <xsl:call-template name="LeftPad0">
                   <xsl:with-param name="input" select="concat('0',$input)"/>
                   <xsl:with-param name="n" select="$n"/>
               </xsl:call-template>
           </xsl:when>
           <xsl:otherwise><xsl:value-of select="$input"/></xsl:otherwise>
       </xsl:choose>
  </xsl:template>

  <!--  ������ ��� ���������� �������� ������ -->
  <xsl:template name="Pad">
       <xsl:param name="input"/>
       <xsl:param name="n" select="25"/>
       <xsl:choose>
           <xsl:when test="string-length($input) &lt; $n">
               <xsl:call-template name="Pad">
                   <xsl:with-param name="input" select="concat($input,' ')"/>
                   <xsl:with-param name="n" select="$n"/>
               </xsl:call-template>
           </xsl:when>
           <xsl:otherwise><xsl:value-of select="$input"/></xsl:otherwise>
       </xsl:choose>
  </xsl:template>

  <!--  ������ ��� ���������� �������� ������ � �����, ����� �������� ���� � ������ -->
  <xsl:template name="Center">
       <xsl:param name="input"/>
       <xsl:param name="n" select="25"/>
       <xsl:param name="i" select="string('Left')"/>
       <xsl:choose>
           <xsl:when test="string-length($input) &lt; $n">
                <xsl:choose>
                 <xsl:when test="$i=string('Left')">
                   <xsl:call-template name="Center">
                       <xsl:with-param name="input" select="concat(' ',$input)"/>
                       <xsl:with-param name="n" select="$n"/>
                       <xsl:with-param name="i" select="string('Right')"/>
                   </xsl:call-template>
                 </xsl:when>
                 <xsl:otherwise>
                   <xsl:call-template name="Center">
                       <xsl:with-param name="input" select="concat($input,' ')"/>
                       <xsl:with-param name="n" select="$n"/>
                       <xsl:with-param name="i" select="string('Left')"/>
                   </xsl:call-template>
                 </xsl:otherwise>
               </xsl:choose>
           </xsl:when>
           <xsl:otherwise>
             <xsl:value-of select="$input"/>
           </xsl:otherwise>
       </xsl:choose>
  </xsl:template>

  <!--  ������ ��� ��������� �����.
  <xsl:template name="text_wrapper">
    <xsl:param name="text"/>
    <xsl:param name="width" select="70"/>
    <xsl:param name="before" select="string('')"/>

    <xsl:if test="string-length($text)">
      <xsl:value-of select="substring($text, 1, $width)"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:if test="string-length(substring($text, $width + 1))">
        <xsl:value-of select="$before" />
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="substring($text, $width + 1)"/>
          <xsl:with-param name="width" select="$width"/>
          <xsl:with-param name="before" select="$before"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:if>
  </xsl:template>
  -->
  <!--  ������ ��� ��������� �����.   -->
  <xsl:template name="text_wrapper">
    <xsl:param name="text" />
    <xsl:param name="width" select="70" />
    <xsl:param name="pos" select="0" />
    <xsl:param name="before" select="string('                           : ')"/>

    <xsl:choose>

      <xsl:when test="contains( $text, ' ' )">
        <xsl:variable name="first-word" select="substring-before( $text, ' ' )" />
        <xsl:variable name="pos-now" select="$pos + 1 + string-length( $first-word )" />
        <xsl:choose>

          <xsl:when test="$pos > 0 and $pos-now >= $width">
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:value-of select="$before" />
            <xsl:call-template name="text_wrapper">
              <xsl:with-param name="text" select="$text" />
              <xsl:with-param name="width" select="$width" />
              <xsl:with-param name="pos" select="0" />
              <xsl:with-param name="before" select="$before"/>
            </xsl:call-template>
          </xsl:when>

          <xsl:otherwise>
            <xsl:value-of select="$first-word" />
            <xsl:text> </xsl:text>
            <xsl:call-template name="text_wrapper">
              <xsl:with-param name="text" select="substring-after( $text, ' ' )" />
              <xsl:with-param name="width" select="$width" />
              <xsl:with-param name="pos" select="$pos-now" />
              <xsl:with-param name="before" select="$before"/>
            </xsl:call-template>
          </xsl:otherwise>

        </xsl:choose>
      </xsl:when>

      <xsl:otherwise>
        <xsl:if test="$pos + string-length( $text ) >= $width">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:value-of select="$before" />
        </xsl:if>
        <xsl:value-of select="$text" />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>

  <!--  ������ ��� ������������ ����� ��������   -->
  <xsl:template name="RublesToText">
    <xsl:param name="sum" select="current()"/>
    <xsl:choose>
      <xsl:when test="string-length($sum) >2">
        <xsl:call-template name="SumInWords">
          <xsl:with-param name="rub" select="substring($sum,1,string-length($sum)-2)" />
          <xsl:with-param name="kop" select="substring($sum,string-length($sum)-1,2)" />
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="string-length($sum) >1">
        <xsl:call-template name="SumInWords">
          <xsl:with-param name="rub" select="'0'" />
          <xsl:with-param name="kop" select="$sum" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="SumInWords">
          <xsl:with-param name="rub" select="'0'" />
          <xsl:with-param name="kop" select="concat('0',$sum)" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--  ������ ��� ������ ������������ ����� �� ���   -->
  <xsl:template name="NameFromBic20">
    <xsl:param name="bic" select="''"/>

    <xsl:for-each
      select="document(concat($NsiDirName,'bic20.xml'))//bnkseek:bank[@newnum=$bic]/bnkseek:namep">
      <xsl:value-of select="."/>
    </xsl:for-each>
  </xsl:template>

  <!--  ������ ������. �����=12   -->
  <xsl:variable name="blnk12" select="string('            ')"/>

  <!--  ������ ������. �����=27   -->
  <xsl:variable name="blnk27" select="string('                           ')"/>

</xsl:stylesheet>


