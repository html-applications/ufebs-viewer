<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
     ��������� ������ : 19-11-2017 ������ ����� 2018.1.3
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED277  -->
  <xsl:template match="ed:ED277">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>��������� � ����������� ������ ������ �������� ������� � ������� �������� /ED277/&#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:call-template name="PriznGr"/>

      <xsl:text>���� � ����� ����������� �� : </xsl:text><xsl:value-of select="@EDDateTime"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:NotificationInfo">
        <xsl:text>��������� ���������&#13;&#10;</xsl:text>
        <xsl:text>====================&#13;&#10;</xsl:text>
        <xsl:text>���� ���������              : </xsl:text>
        <xsl:for-each select="@NotificationDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>����� ���������             : </xsl:text><xsl:value-of select="@NotificationNum"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:OrgInfo">
        <xsl:text>��������� ��������� �����������&#13;&#10;</xsl:text>
        <xsl:text>================================&#13;&#10;</xsl:text>
        <xsl:text>��������������� �����       : </xsl:text><xsl:value-of select="@OrgRegNum"/>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>���� �������� �� �������    : </xsl:text>
        <xsl:for-each select="@AgreementDate"><xsl:call-template name="date"/></xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>����� �������� �� �������   : </xsl:text><xsl:value-of select="@AgreementNum"/>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>����� ����������� �����     : </xsl:text><xsl:value-of select="@AccDepositNum"/>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:for-each select="ed:OrgName">
          <xsl:text>������������                : </xsl:text>
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="."/>
            <xsl:with-param name="width" select="40"/>
            <xsl:with-param name="before" select="string('                            : ')"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>�������� � ����������� ��������&#13;&#10;</xsl:text>
      <xsl:text>=================================&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:DepositTerms">
        <xsl:for-each select="@ApplicationSum">
          <xsl:text>����� ��������              : </xsl:text>
          <xsl:call-template name="ffsum"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>

        <xsl:text>������ (� ���������)        : </xsl:text><xsl:value-of select="@ApplicationRate"/>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:for-each select="@PlacementDate">
          <xsl:text>���� ����������             : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>

        <xsl:for-each select="@ReturnDate">
          <xsl:text>���� ��������               : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
      </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>