<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!-- =============================================================================================================
      ����� ����
  ================================================================================================================== -->
  <xsl:template match="ed:PacketESID">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>����� ����</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:choose>
          <xsl:when test="@EDCreationTime">
              <xsl:text>����� ����������� �� : </xsl:text>
              <xsl:value-of select="@EDCreationTime"/>
              <xsl:text>&#13;&#10;</xsl:text>
          </xsl:when>
          <xsl:otherwise>
              <xsl:text>&#13;&#10;</xsl:text>
          </xsl:otherwise>
      </xsl:choose>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:if test="count(ed:ED201)!=0 or count(ed:ED205)!=0">
          <xsl:text>�������      : </xsl:text>
          <xsl:if test="count(ed:ED205)!=0">
            <xsl:value-of select="count(ed:ED205[@StatusStateCode=19])"/>
          </xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>�����������  : </xsl:text>
          <xsl:value-of select="count(ed:ED201) + count(ed:ED205[@StatusStateCode=20])"/>
          <xsl:if test="count(ed:ED201) + count(ed:ED205[@StatusStateCode=20])!=0">
            <xsl:value-of select="concat(' ','(',' ')"/>
            <xsl:for-each select="node()[starts-with(local-name(),'ED')]">
              <xsl:if test="local-name()='ED201' or local-name()='ED205' and @StatusStateCode=20">
                <xsl:value-of select="concat(position(),' ')"/>
              </xsl:if>
            </xsl:for-each>
            <xsl:value-of select="string(')')"/>
          </xsl:if>
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:call-template name="Session"/>

      <xsl:if test="ed:InitialPacketED/@EDNo != ''">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>�������������� ��������� �� : </xsl:text>
          <xsl:text>� </xsl:text><xsl:value-of select="ed:InitialPacketED/@EDNo"/>
          <xsl:text> �� </xsl:text>
          <xsl:for-each select="ed:InitialPacketED/@EDDate">
              <xsl:call-template name="date"/>
          </xsl:for-each>
          <xsl:text> ��� : </xsl:text><xsl:value-of select="ed:InitialPacketED/@EDAuthor"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
<!--      <xsl:text>��������: </xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
-->
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:apply-templates select="*[starts-with(local-name(.),'ED')]"/>

  <!--       <xsl:apply-templates/>      -->
  </xsl:template>


</xsl:stylesheet>