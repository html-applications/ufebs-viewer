<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����� �� � ���) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates select="ed:ED408"/>
    <xsl:apply-templates select="ed:ED421"/>
    <xsl:apply-templates select="ed:ED422"/>
    <xsl:apply-templates select="ed:ED425"/>
    <xsl:apply-templates select="ed:ED428"/>
    <xsl:apply-templates select="ed:ED429"/>
    <xsl:apply-templates select="ed:ED431"/>
    <xsl:apply-templates select="ed:ED432"/>
    <xsl:apply-templates select="ed:ED433"/>
    <xsl:apply-templates select="ed:ED434"/>
    <xsl:apply-templates select="ed:ED435"/>
    <xsl:apply-templates select="ed:ED499"/>
  </xsl:template>

  <xsl:template match="*">
  </xsl:template>

  <xsl:include href="ED408.xslt" />
  <xsl:include href="ED421.xslt" />
  <xsl:include href="ED422.xslt" />
  <xsl:include href="ED425.xslt" />
  <xsl:include href="ED428.xslt" />
  <xsl:include href="ED429.xslt" />
  <xsl:include href="ED431.xslt" />
  <xsl:include href="ED432.xslt" />
  <xsl:include href="ED433.xslt" />
  <xsl:include href="ED434.xslt" />
  <xsl:include href="ED435.xslt" />
  <xsl:include href="ED499.xslt" />
  <xsl:include href="Deposit.xslt" />

  <xsl:include href="Common.xslt" />
</xsl:stylesheet>
