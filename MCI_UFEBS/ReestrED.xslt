<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������). -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ����� ������� ����������� ���������, �������������� � �������� -->
  <!--  �� ����� ���������� ������� AstraKBR  -->

  <xsl:template match="ed:ReestrED">
    <xsl:text>������ ����������� ���������, �������������� � �������� &#13;&#10;</xsl:text>
    <xsl:for-each select="@EDDate">
       <xsl:call-template name="date"/>
    </xsl:for-each>
    <xsl:text>    c </xsl:text><xsl:value-of select="@STime"/>
    <xsl:text>   �� </xsl:text><xsl:value-of select="@FTime"/>

    <xsl:text>&#13;&#10;===================================================================================&#13;&#10;</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>������� ������� ���������  : 02 (������ ���������� ��������)</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>���������� ����������      : </xsl:text><xsl:value-of select="ed:SystemCode_02/@EDQuantity"/>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>����� �����                : </xsl:text>
    <xsl:for-each select="ed:SystemCode_02/@Sum"><xsl:call-template name="ffsum"/></xsl:for-each>
    <xsl:text>&#13;&#10;-----------------------------------------------------------------------------------&#13;&#10;</xsl:text>
    <xsl:if test="count(ed:SystemCode_02/*) > 0">
      <xsl:call-template name="Reestr_SystemCode_Header"/>
      <xsl:for-each select="ed:SystemCode_02/*">
         <xsl:call-template name="ReestrStr">
         </xsl:call-template>
      </xsl:for-each>
      <xsl:text>&#13;&#10;===================================================================================&#13;&#10;</xsl:text>
    </xsl:if>

    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>������� ������� ���������  : 05 (������ �������� ��������)</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>���������� ����������      : </xsl:text><xsl:value-of select="ed:SystemCode_05/@EDQuantity"/>
    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>����� �����                : </xsl:text>
    <xsl:for-each select="ed:SystemCode_05/@Sum"><xsl:call-template name="ffsum"/></xsl:for-each>
    <xsl:text>&#13;&#10;-----------------------------------------------------------------------------------&#13;&#10;</xsl:text>
    <xsl:if test="count(ed:SystemCode_05/*) > 0">
      <xsl:call-template name="Reestr_SystemCode_Header"/>
      <xsl:for-each select="ed:SystemCode_05/*">
         <xsl:call-template name="ReestrStr">
         </xsl:call-template>
      </xsl:for-each>
      <xsl:text>&#13;&#10;===================================================================================&#13;&#10;</xsl:text>
    </xsl:if>

    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>����&#13;&#10;</xsl:text>
    <xsl:text>���������� ����������      : </xsl:text><xsl:value-of select="ed:ESID/@EDQuantity"/>
    <xsl:text>&#13;&#10;-----------------------------------------------------------------------------------&#13;&#10;</xsl:text>
    <xsl:if test="count(ed:ESID/*) > 0">
      <xsl:call-template name="Reestr_ESID_Header"/>
      <xsl:for-each select="ed:ESID/*">
         <xsl:call-template name="ReestrStrESID">
         </xsl:call-template>
      </xsl:for-each>
      <xsl:text>&#13;&#10;===================================================================================&#13;&#10;</xsl:text>
    </xsl:if>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!--  ������ ��� ������ ����� ������ ������� (ReestrED), ����������� � ��������.   -->

  <xsl:template name="Reestr_SystemCode_Header">
        <xsl:text> � �/�   ��� ��    ���-��      �����              �����               ����&#13;&#10;</xsl:text>
        <xsl:text>                    ���      ���������&#13;&#10;</xsl:text>
        <xsl:text>-----------------------------------------------------------------------------------&#13;&#10;</xsl:text>
  </xsl:template>

  <!--  ������ ��� ������ ����� ������ ������� (ReestrED), ����������� � ����.   -->

  <xsl:template name="Reestr_ESID_Header">
        <xsl:text> � �/�   ��� ��                �����            ����������            ����&#13;&#10;</xsl:text>
        <xsl:text>                             ���������&#13;&#10;</xsl:text>
        <xsl:text>-----------------------------------------------------------------------------------&#13;&#10;</xsl:text>
  </xsl:template>


  <!--  ������ ��� ������ ������ ������� ������� (ReestrED).   -->

  <xsl:template name="ReestrStr">
       <xsl:call-template name="LeftPad">
           <xsl:with-param name="input" select="position()"/>
           <xsl:with-param name="n" select="6"/>
       </xsl:call-template>
       <xsl:call-template name="Center">
           <xsl:with-param name="input" select="name(.)"/>
           <xsl:with-param name="n" select="12"/>
       </xsl:call-template>
       <xsl:call-template name="Center">
           <xsl:with-param name="input" select="@EDQuantity"/>
           <xsl:with-param name="n" select="9"/>
       </xsl:call-template>
       <xsl:call-template name="Center">
           <xsl:with-param name="input" select="@EDTime"/>
           <xsl:with-param name="n" select="12"/>
       </xsl:call-template>
       <xsl:value-of select="string('  ')"/>
       <xsl:for-each select="@Sum"><xsl:call-template name="fsum"/></xsl:for-each>
       <xsl:value-of select="string('  ')"/>
       <xsl:call-template name="Pad">
           <xsl:with-param name="input" select="@UbfName"/>
           <xsl:with-param name="n" select="35"/>
       </xsl:call-template>
<!--       <xsl:text>&#13;&#10;</xsl:text>
        <xsl:value-of select="string('                                                               ')"/>
-->
       <xsl:value-of select="string(' ')"/>
       <xsl:value-of select="@MessageID"/>
       <xsl:text>&#13;&#10;</xsl:text>

  </xsl:template>

  <!--  ������ ��� ������ ������ ���� ������� (ReestrED).   -->

  <xsl:template name="ReestrStrESID">
       <xsl:call-template name="LeftPad">
           <xsl:with-param name="input" select="position()"/>
           <xsl:with-param name="n" select="6"/>
       </xsl:call-template>
       <xsl:call-template name="Center">
           <xsl:with-param name="input" select="name(.)"/>
           <xsl:with-param name="n" select="12"/>
       </xsl:call-template>
       <xsl:value-of select="string('         ')"/>
       <xsl:call-template name="Center">
           <xsl:with-param name="input" select="@EDTime"/>
           <xsl:with-param name="n" select="12"/>
       </xsl:call-template>
       <xsl:value-of select="string('  ')"/>
       <xsl:call-template name="LeftPad">
           <xsl:with-param name="input" select="@EDReceiver"/>
           <xsl:with-param name="n" select="18"/>
       </xsl:call-template>
       <xsl:value-of select="string('  ')"/>
       <xsl:call-template name="Pad">
           <xsl:with-param name="input" select="@UbfName"/>
           <xsl:with-param name="n" select="35"/>
       </xsl:call-template>
<!--       <xsl:text>&#13;&#10;</xsl:text>
        <xsl:value-of select="string('                                                               ')"/>
-->
       <xsl:value-of select="string(' ')"/>
       <xsl:value-of select="@MessageID"/>
       <xsl:text>&#13;&#10;</xsl:text>

  </xsl:template>

</xsl:stylesheet>