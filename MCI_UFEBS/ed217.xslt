<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED217  -->
  <xsl:template match="ed:ED217">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:text>��������� � ������������� �� �������������� ������� /ED217/</xsl:text>
      <xsl:call-template name="PriznGr"/>
      <xsl:for-each select="@InfoDate">
          <xsl:text>���� ����������� ����������      : </xsl:text><xsl:call-template name="date"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>��� ����� :  </xsl:text>
      <xsl:for-each select="@BIC">
          <xsl:value-of select="."/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
          <xsl:text>����������������� ���� (�������) :  </xsl:text>
      <xsl:for-each select="@CorrespAcc">
          <xsl:value-of select="."/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:choose> <xsl:when test="ed:PartInfo">
          <xsl:apply-templates select="ed:PartInfo"/>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when> </xsl:choose>

      <xsl:if test="ed:InitialED/@EDNo != ''">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:apply-templates select="ed:InitialED"/>
      </xsl:if>

      <xsl:for-each select="ed:CreditInfo">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>----------------------------------------&#13;&#10;</xsl:text>
          <xsl:text>&#13;&#10;</xsl:text>

          <xsl:if test="@SessionID != '' and @SessionID != '0'">
              <xsl:text>����� ������������������ ����� :  </xsl:text>
              <xsl:for-each select="@SessionID"><xsl:value-of select="."/><xsl:text>&#13;&#10;</xsl:text></xsl:for-each>
          </xsl:if>
          <xsl:for-each select="@CreditOperation">
            <xsl:text>������� ��������������/��������� �������������� ������� :  </xsl:text>
            <xsl:value-of select="."/><xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:for-each select="@CreditLimit">
            <xsl:text>������� ������ ������ �������������� ������� � ������� �������� :  </xsl:text>
            <xsl:call-template name="summa"/>
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>
          <xsl:if test="@EndTime != ''">
              <xsl:text>����� ������� ������������ ������� :  </xsl:text>
              <xsl:for-each select="@EndTime">
                  <xsl:value-of select="."/>
                  <xsl:text>&#13;&#10;</xsl:text>
              </xsl:for-each>
          </xsl:if>
          <xsl:if test="@DebtBefore != ''">
              <xsl:text>����� ������������� �� ���������������� �������������� ������� �� ���������� ���������    :  </xsl:text>
              <xsl:for-each select="@DebtBefore"><xsl:call-template name="summa"/></xsl:for-each>
              <xsl:text>&#13;&#10;</xsl:text>
          </xsl:if>
          <xsl:if test="@DebtAfter != ''">
              <xsl:text>����� ������������� �� ���������������� �������������� ������� ����� ���������� ��������� :  </xsl:text>
              <xsl:for-each select="@DebtAfter"><xsl:call-template name="summa"/></xsl:for-each>
              <xsl:text>&#13;&#10;</xsl:text>
          </xsl:if>
          <xsl:for-each select="ed:AccDocInfo">
              <xsl:text>&#13;&#10;</xsl:text>
              <xsl:if test="@AccDocNo != ''">
                  <xsl:text>����� ���������� ���������        :  </xsl:text>
                  <xsl:for-each select="@AccDocNo"><xsl:value-of select="."/></xsl:for-each>
                  <xsl:text>&#13;&#10;</xsl:text>
              </xsl:if>

              <xsl:for-each select="@AccDocDate">
                <xsl:text>���� ������� ���������� ��������� :  </xsl:text>
                <xsl:call-template name="date"/>
                <xsl:text>&#13;&#10;</xsl:text>
              </xsl:for-each>

              <xsl:for-each select="@Sum">
                <xsl:text>����� ��� :  </xsl:text>
                <xsl:call-template name="summa"/>
                <xsl:text>&#13;&#10;</xsl:text>
              </xsl:for-each>

              <xsl:if test="@CashDocNo != ''">
                  <xsl:text>����� ��������� ���������         :  </xsl:text>
                  <xsl:for-each select="@CashDocNo">
                      <xsl:value-of select="."/>
                      <xsl:text>&#13;&#10;</xsl:text>
                  </xsl:for-each>
              </xsl:if>
          </xsl:for-each>
      </xsl:for-each>


      <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>