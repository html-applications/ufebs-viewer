<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI.
     ����� ��������� ������� � ���������� ���������� ���������
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

<!-- ======================================================================= -->
<!--  ����� ��������� ������� � ���������� ���������� ���������  -->
<!-- ======================================================================= -->
  <xsl:template name="ED_Single">
    <xsl:choose>
      <xsl:when test="local-name(.)='ED101'">
          <xsl:call-template name="Center">
            <xsl:with-param name="input" select="string('��������� ���������')"/>
            <xsl:with-param name="n" select="45"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:when>
      <xsl:when test="local-name(.)='ED103'">
          <xsl:call-template name="Center">
            <xsl:with-param name="input" select="string('��������� ����������')"/>
            <xsl:with-param name="n" select="45"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="local-name(.)='ED104'">
          <xsl:call-template name="Center">
            <xsl:with-param name="input" select="string('���������� ���������')"/>
            <xsl:with-param name="n" select="45"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="local-name(.)='ED105'">
          <xsl:call-template name="Center">
            <xsl:with-param name="input" select="string('��������� �����')"/>
            <xsl:with-param name="n" select="45"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="local-name(.)='ED107'">
        <xsl:call-template name="Center">
          <xsl:with-param name="input" select="string('��������� �����')"/>
          <xsl:with-param name="n" select="45"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="local-name(.)='ED108'">
          <xsl:call-template name="Center">
            <xsl:with-param name="input" select="string('��������� ��������� �� ����� ����� � ��������')"/>
            <xsl:with-param name="n" select="45"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>

      </xsl:when>
      <xsl:when test="local-name(.)='ED110'">
          <xsl:call-template name="Center">
            <xsl:with-param name="input" select="string('��� ������������ �������')"/>
            <xsl:with-param name="n" select="45"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="local-name(.)='ED111'">
          <xsl:call-template name="Center">
            <xsl:with-param name="input" select="string('������������ �����')"/>
            <xsl:with-param name="n" select="45"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="local-name(.)='ED113'">
          <xsl:call-template name="Center">
            <xsl:with-param name="input" select="string('��������� ����������')"/>
            <xsl:with-param name="n" select="45"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="local-name(.)='ED114'">
          <xsl:call-template name="Center">
            <xsl:with-param name="input" select="string('���������� ���������')"/>
            <xsl:with-param name="n" select="45"/>
          </xsl:call-template>
          <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
    <xsl:text>=============================================&#13;&#10;</xsl:text>

    <xsl:text>-----------------------------------------------------------------&#13;&#10;</xsl:text>
    <xsl:text>� � ������������ ���������      : �������� ���������&#13;&#10;</xsl:text>
    <xsl:text>-----------------------------------------------------------------&#13;&#10;</xsl:text>

    <xsl:text>200  ���������� ����� ���       : </xsl:text>
    <xsl:value-of select="@EDNo" />
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>201  ���� ����������� ���       : </xsl:text>
    <xsl:for-each select="@EDDate"> <xsl:call-template name="date"/> </xsl:for-each>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>202  ����.�����.����������� ��� : </xsl:text>
    <xsl:value-of select="@EDAuthor" />
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>     ����.�����.����������  ��� : </xsl:text>
    <xsl:value-of select="@EDReceiver" />
    <xsl:text>&#13;&#10;</xsl:text> <xsl:text>&#13;&#10;</xsl:text> <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>  7  ����� ���������            : </xsl:text>
    <xsl:for-each select="@Sum"> <xsl:call-template name="ffsum"/> </xsl:for-each>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:for-each select="@TransKind">
      <xsl:text> 18  ��� ��������               : </xsl:text> <xsl:value-of select="." />
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@Priority">
      <xsl:text> 21  ��� ����������� �������    : </xsl:text> <xsl:value-of select="." />
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@ PaymentPrecedence">
      <xsl:text>     ��������� �������          : </xsl:text> <xsl:value-of select="." />
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@PaytKind">
      <xsl:text>  5  ��� �������                : </xsl:text> <xsl:value-of select="." />
      <xsl:if test=".='4'"><xsl:text> (������)</xsl:text></xsl:if>
      <xsl:if test=".='5'"><xsl:text> (����������)</xsl:text></xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@TransContent">
      <xsl:text> 70  ���������� ��������        : </xsl:text> <xsl:value-of select="." />
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@PaytCondition">
      <xsl:text> 35  ������� ������             : </xsl:text> <xsl:value-of select="." />
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@AcptTerm">
      <xsl:text> 36  ���� ��� �������           : </xsl:text> <xsl:value-of select="." />
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@ReceiptDate">
      <xsl:text> 62  ��������� � ���� ����.     : </xsl:text> <xsl:call-template name="date"/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@FileDate">
      <xsl:text> 63  ���� ��������� � ��������� : </xsl:text> <xsl:call-template name="date"/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@ChargeOffDate">
      <xsl:text> 71  ������� �� ����� ����.     : </xsl:text> <xsl:call-template name="date"/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@SessionID">
      <xsl:text>241  ����� ����� (������)       : </xsl:text> <xsl:value-of select="."/>
        <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@SystemCode">
      <xsl:text>     ������� ���������          : </xsl:text> <xsl:value-of select="."/>
      <xsl:if test=".='02'"><xsl:text> (������ ���������� ��������)</xsl:text></xsl:if>
      <xsl:if test=".='05'"><xsl:text> (������ �������� ��������)</xsl:text></xsl:if>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="@PaymentID">
      <xsl:text> 22  ����.������������� ������� : </xsl:text> <xsl:value-of select="."/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:AccDoc">
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>     ��������� ��������� ������������&#13;&#10;</xsl:text>
      <xsl:text>---------------------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="@AccDocNo">
        <xsl:text>  3  ����� ������������         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@AccDocDate">
        <xsl:text>  4  ���� ����������� ��������. : </xsl:text>
        <xsl:for-each select=".">
            <xsl:call-template name="date"/>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:RelatedDoc">
      <xsl:text>     ��������� ������������, ���������� � &#13;&#10;</xsl:text>
      <xsl:text>     ����������� ������� ��������� �����&#13;&#10;</xsl:text>
      <xsl:text>---------------------------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="@RelatedDocNo">
        <xsl:text> 78  ����� ���������            : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@RelatedDocDate">
        <xsl:text> 79  ���� ���������             : </xsl:text>
        <xsl:for-each select=".">
            <xsl:call-template name="date"/>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:OrderingBank">
      <xsl:text> 81  ���������� � �����-�����������&#13;&#10;</xsl:text>
      <xsl:text>--------------------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="@BIC">
        <xsl:text> 81.2 ��� �����                 : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@SWBIC">
        <xsl:text> 81.3 ����������������� ���,&#13;&#10;</xsl:text>
        <xsl:text>      ����������� SWIFT         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@BankAccount">
        <xsl:text> 81.4 ����� ����� �����         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="Name">
        <xsl:text> 81.1 ������������ �����        : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="."/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:PrevInstrAgent">
      <xsl:text> 82  ���������� � ���������� ��������������� �����&#13;&#10;</xsl:text>
      <xsl:text>--------------------------------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="@BIC">
        <xsl:text> 82.2 ��� �����                 : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@SWBIC">
        <xsl:text> 82.3 ����������������� ���,&#13;&#10;</xsl:text>
        <xsl:text>      ����������� SWIFT         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@BankAccount">
        <xsl:text> 82.4 ����� ����� �����         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="Name">
        <xsl:text> 82.1 ������������ �����        : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="."/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:InstructingAgent">
      <xsl:text> 83  ���������� � �����-�����������&#13;&#10;</xsl:text>
      <xsl:text>-----------------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="@BIC">
        <xsl:text> 83.2 ��� �����                 : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@SWBIC">
        <xsl:text> 83.3 ����������������� ���,&#13;&#10;</xsl:text>
        <xsl:text>      ����������� SWIFT         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@CorrespAcc">
        <xsl:text> 83.4 ����� ����� �����         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:InstructedAgent">
      <xsl:text> 84 ���������� � �����-�����������&#13;&#10;</xsl:text>
      <xsl:text>-----------------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="@BIC">
        <xsl:text> 84.2 ��� �����                 : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@SWBIC">
        <xsl:text> 84.3 ����������������� ���,&#13;&#10;</xsl:text>
        <xsl:text>      ����������� SWIFT         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@CorrespAcc">
        <xsl:text> 84.4 ����� ����� �����         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:AcctWithInst">
      <xsl:text> 85  ���������� �� ������ �����-����������&#13;&#10;</xsl:text>
      <xsl:text>------------------------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="@BIC">
        <xsl:text> 85.2 ��� �����                 : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@SWBIC">
        <xsl:text> 85.3 ����������������� ���,&#13;&#10;</xsl:text>
        <xsl:text>      ����������� SWIFT         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@BankAccount">
        <xsl:text> 85.4 ����� ����� �����         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="Name">
        <xsl:text> 85.1 ������������ �����        : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="."/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:Beneficiary">
      <xsl:text> 86  ���������� � �����-����������&#13;&#10;</xsl:text>
      <xsl:text>----------------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="@BIC">
        <xsl:text> 86.2 ��� �����                 : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@SWBIC">
        <xsl:text> 86.3 ����������������� ���,&#13;&#10;</xsl:text>
        <xsl:text>      ����������� SWIFT         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@BankAccount">
        <xsl:text> 86.4 ����� ����� �����         : </xsl:text>
        <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="Name">
        <xsl:text> 86.1 ������������ �����        : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="."/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:Sndr2RecvrInfo">
      <xsl:text> 74  ���������� ����������      : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="."/>
        <xsl:with-param name="width" select="50"/>
        <xsl:with-param name="before" select="string('                                : ')"/>
      </xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:InitialAccDoc/@AccDocNo">
      <xsl:text>     ����� ��������� ���-��     : </xsl:text> <xsl:value-of select="." />
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:InitialAccDoc/@AccDocDate">
      <xsl:text>     ���� ��������� ���-��      : </xsl:text>
      <xsl:for-each select="."> <xsl:call-template name="date"/> </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:for-each select="ed:MemoOrderPayer">
      <xsl:for-each select="@Acc">
        <xsl:text>     ���.����/������� �� ������ : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@BIC">
        <xsl:text>     ��� ���/�� �� ������       : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="ed:AccName">
        <xsl:text>     ������. ����� �� ������    : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="."/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
    </xsl:for-each>

    <xsl:for-each select="ed:MemoOrderPayee">
      <xsl:for-each select="@Acc">
        <xsl:text>     ���.����/������� �� �������: </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="@BIC">
        <xsl:text>     ��� ���/�� �� �������      : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:for-each select="ed:AccName">
        <xsl:text>     ������. ����� �� �������   : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="."/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                : ')"/>
        </xsl:call-template>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
    </xsl:for-each>

    <xsl:for-each select="ed:RefDocInfo">
      <xsl:text>     ���������� ��������        : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="."/>
        <xsl:with-param name="width" select="50"/>
        <xsl:with-param name="before" select="string('                                : ')"/>
      </xsl:call-template>
    </xsl:for-each>

    <xsl:for-each select="ed:InitialED">
      <xsl:text>     �������������� ��������� ���&#13;&#10;</xsl:text>
      <xsl:text>-----------------------------------&#13;&#10;</xsl:text>
      <xsl:text>203  ����� �� � ������� ������� : </xsl:text>
      <xsl:value-of select="@EDNo"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>204  ���� ������������� ��      : </xsl:text>
      <xsl:for-each select="@EDDate">
        <xsl:call-template name="date"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>205  ��� ��                     : </xsl:text>
      <xsl:value-of select="@EDAuthor"/>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:Payer">
      <xsl:text>��������� �����������           :&#13;&#10;</xsl:text>
      <xsl:text>--------------------------------:&#13;&#10;</xsl:text>
      <xsl:for-each select="@PersonalAcc">
        <xsl:text>  9  ������� ����               : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@INN">
        <xsl:text> 60  ���                        : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@KPP">
        <xsl:text>102  ���                        : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="ed:Name">
        <xsl:text>  8  ������������               : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="."/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                : ')"/>
        </xsl:call-template>
      </xsl:for-each>

      <xsl:for-each select="ed:Bank/@BIC">
        <xsl:text> 11  ��� ��, ������� �� ��� ��� : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="ed:Bank/@CorrespAcc">
        <xsl:text> 12  ����� ����.����� ��,       :&#13;&#10;</xsl:text>
        <xsl:text>     ������� �� � ���           : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:Payee">
      <xsl:text>��������� ����������            :&#13;&#10;</xsl:text>
      <xsl:text>--------------------------------:&#13;&#10;</xsl:text>
      <xsl:for-each select="@PersonalAcc">
        <xsl:text> 17  ������� ����               : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@INN">
        <xsl:text> 61  ���                        : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@KPP">
        <xsl:text>103  ���                        : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="ed:Name">
        <xsl:text> 16  ������������               : </xsl:text>
        <xsl:call-template name="text_wrapper">
          <xsl:with-param name="text" select="."/>
          <xsl:with-param name="width" select="50"/>
          <xsl:with-param name="before" select="string('                                : ')"/>
        </xsl:call-template>
      </xsl:for-each>

      <xsl:for-each select="ed:Bank/@BIC">
        <xsl:text> 14  ��� ��, ������� �� ��� ��� : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="ed:Bank/@CorrespAcc">
        <xsl:text> 15  ����� ����.����� ��,       :&#13;&#10;</xsl:text>
        <xsl:text>     ������� �� � ���           : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:Purpose">
      <xsl:text> 24  ���������� �������         : </xsl:text>
      <xsl:call-template name="text_wrapper">
        <xsl:with-param name="text" select="."/>
        <xsl:with-param name="width" select="50"/>
        <xsl:with-param name="before" select="string('                                : ')"/>
      </xsl:call-template>
    </xsl:for-each>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:for-each select="ed:DepartmentalInfo">
      <xsl:text>������������� ����������        :&#13;&#10;</xsl:text>
      <xsl:text>--------------------------------:&#13;&#10;</xsl:text>

      <xsl:for-each select="@DrawerStatus">
        <xsl:text>101  ������ �����������         : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@CBC">
        <xsl:text>104  ���                        : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@OKATO">
        <xsl:text>105  ��� �����                  : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@PaytReason">
        <xsl:text>106  ��������� �����.�������    : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@TaxPeriod">
        <xsl:text>107  ��������� ������           : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@DocNo">
        <xsl:text>108  ����� ���������� ��������� : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@DocDate">
        <xsl:text>109  ���� ���������� ���������  : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>

      <xsl:for-each select="@TaxPaytKind">
        <xsl:text>110  ��� ���������� �������     : </xsl:text> <xsl:value-of select="." />
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:for-each select="ed:PartialPayt">
      <xsl:text>������. � ��������� �������     :&#13;&#10;</xsl:text>
      <xsl:text>--------------------------------:&#13;&#10;</xsl:text>
      <xsl:text> 38  ����� ���������� �������   : </xsl:text> <xsl:value-of select="@PaytNo" />
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text> 39  ���� ���������� ���������  : </xsl:text> <xsl:value-of select="@TransKind" />
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text> 40  ����� ���������� ��������� : </xsl:text> <xsl:value-of select="ed:AccDoc/@AccDocNo" />
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text> 41  ���� ���������� ���������  : </xsl:text>
      <xsl:for-each select="ed:AccDoc/@AccDocDate"> <xsl:call-template name="date"/> </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text> 42  ����� ������� �������      : </xsl:text>
      <xsl:for-each select="@SumResidualPayt"> <xsl:call-template name="ffsum"/> </xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

    <xsl:if test="count(ed:CreditTransferTransactionInfo) != 0">
      <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
      <xsl:text> ������ �������� � ���������� ������������&#13;&#10;</xsl:text>
      <xsl:text>------------------------------------------&#13;&#10;</xsl:text>
      <xsl:for-each select="ed:CreditTransferTransactionInfo">
        <xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
        <xsl:text>����� ������ � ������� : </xsl:text> <xsl:value-of select="@TransactionID" />
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:if test="count(@PayerDocNo) != 0 or count(@PayerDocDate) != 0">
          <xsl:text>� � ���� ������������  : </xsl:text> <xsl:value-of select="@PayerDocNo" />
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>�����������            : </xsl:text>
          <xsl:for-each select="@PayerDocDate"> <xsl:call-template name="date"/> </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>

        <xsl:text>���� � ����� ��������  : </xsl:text>
        <xsl:for-each select="@TransactionDate">
            <xsl:call-template name="date"/> <xsl:value-of select="string('   ')" />
        </xsl:for-each>
        <xsl:for-each select="@TransactionSum"> <xsl:call-template name="ffsum"/> </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:if test="count(@OperationID) !=0">
          <xsl:text>����.����� ��������    : </xsl:text> <xsl:value-of select="@OperationID" />
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:if>


        <xsl:for-each select="@PaymentID">
          <xsl:text>����.�����.�������     : </xsl:text>   <xsl:value-of select="." />
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:for-each select="ed:TransactionPayerInfo">
          <xsl:text>���������� � �����������        &#13;&#10;</xsl:text>
          <xsl:text>-----------------------------&#13;&#10;</xsl:text>

          <xsl:for-each select="@PersonalID">
            <xsl:text>�������������    : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="@Acc">
            <xsl:text>����� ����.����� : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="@INN">
            <xsl:text>���              : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:PersonName">
            <xsl:text>��� ���.����     : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:PersonAddress">
            <xsl:text>����� ���.����   : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:TradeName">
            <xsl:text>������������     : </xsl:text>
            <xsl:call-template name="text_wrapper">
              <xsl:with-param name="text" select="."/>
              <xsl:with-param name="width" select="60"/>
              <xsl:with-param name="before" select="string('                 : ')"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:text>&#13;&#10;</xsl:text>

        </xsl:for-each>

        <xsl:for-each select="ed:TransactionPayeeInfo">
          <xsl:text>���������� � ���������� &#13;&#10;</xsl:text>
          <xsl:text>-----------------------------&#13;&#10;</xsl:text>

          <xsl:for-each select="@PersonalID">
            <xsl:text>�������������    : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="@Acc">
            <xsl:text>����� ����.����� : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="@INN">
            <xsl:text>���              : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:PersonName">
            <xsl:text>��� ���.����     : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:PersonAddress">
            <xsl:text>����� ���.����   : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:TradeName">
            <xsl:text>������������     : </xsl:text>
            <xsl:call-template name="text_wrapper">
              <xsl:with-param name="text" select="."/>
              <xsl:with-param name="width" select="60"/>
              <xsl:with-param name="before" select="string('                 : ')"/>
            </xsl:call-template>
          </xsl:for-each>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:for-each select="ed:TransactionBeneficiaryInfo">
          <xsl:text>���������� � ����, ��� ����������� �� ������ �������� ������� ����������� &#13;&#10;</xsl:text>
          <xsl:text>--------------------------------------------------------------------------&#13;&#10;</xsl:text>

          <xsl:for-each select="@PersonalID">
            <xsl:text>�������������    : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="@INN">
            <xsl:text>���              : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:PersonName">
            <xsl:text>��� ���.����     : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:PersonAddress">
            <xsl:text>����� ���.����   : </xsl:text> <xsl:value-of select="." />
            <xsl:text>&#13;&#10;</xsl:text>
          </xsl:for-each>

          <xsl:for-each select="ed:TradeName">
            <xsl:text>������������     : </xsl:text>
            <xsl:call-template name="text_wrapper">
              <xsl:with-param name="text" select="."/>
              <xsl:with-param name="width" select="60"/>
              <xsl:with-param name="before" select="string('                 : ')"/>
            </xsl:call-template>
          </xsl:for-each>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:for-each select="ed:TransactionPurpose">
          <xsl:text>���������� �������     : </xsl:text>
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="."/>
            <xsl:with-param name="width" select="60"/>
            <xsl:with-param name="before" select="string('                       : ')"/>
           </xsl:call-template>
        </xsl:for-each>

        <xsl:for-each select="ed:RemittanceInfo">
          <xsl:text>���������� � ��������  : </xsl:text>
          <xsl:call-template name="text_wrapper">
            <xsl:with-param name="text" select="."/>
            <xsl:with-param name="width" select="60"/>
            <xsl:with-param name="before" select="string('                       : ')"/>
           </xsl:call-template>
        </xsl:for-each>

        <xsl:for-each select="ed:CTTDepartmentalInfo">
          <xsl:text>&#13;&#10;</xsl:text>
          <xsl:text>������������� ���������� &#13;&#10;</xsl:text>

          <xsl:text>-------------------------------------------------------------------------------------------------------------------&#13;&#10;</xsl:text>
          <xsl:text>  ������    :���������   : ���������: � ����������  :���� �����.: ��� �����.:����� ���.�����, : ����� ���.����� �� &#13;&#10;</xsl:text>
          <xsl:text>����������� :���.������� :  ������  :  ���������    : ��������� :  �������  :����.� ������ �� :  � ���.������      &#13;&#10;</xsl:text>
          <xsl:text>            :            :          :               :           :           :                 :                    &#13;&#10;</xsl:text>
          <xsl:text>   101�     :   106�     :  107�    :     108�      :  109�     :   110�    :     111�        :       112�&#13;&#10;</xsl:text>
          <xsl:text>-------------------------------------------------------------------------------------------------------------------&#13;&#10;</xsl:text>
          <xsl:choose>
            <xsl:when test="count(@F101R) !=0">
              <xsl:for-each select="@F101R">
                <xsl:call-template name="Center">
                  <xsl:with-param name="input" select="."/>
                  <xsl:with-param name="n" select="13"/>
                </xsl:call-template>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise> <xsl:value-of select="string('             ')" /> </xsl:otherwise>
          </xsl:choose>

          <xsl:choose>
            <xsl:when test="count(@F106R) !=0">
              <xsl:for-each select="@F106R">
                <xsl:call-template name="Center">
                  <xsl:with-param name="input" select="."/>
                  <xsl:with-param name="n" select="13"/>
                </xsl:call-template>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise> <xsl:value-of select="string('             ')" /> </xsl:otherwise>
          </xsl:choose>

          <xsl:choose>
            <xsl:when test="count(@F107R) !=0">
              <xsl:for-each select="@F107R">
                <xsl:call-template name="Center">
                  <xsl:with-param name="input" select="."/>
                  <xsl:with-param name="n" select="11"/>
                </xsl:call-template>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise> <xsl:value-of select="string('           ')" /> </xsl:otherwise>
          </xsl:choose>

          <xsl:choose>
            <xsl:when test="count(@F108R) !=0">
              <xsl:for-each select="@F108R">
                <xsl:call-template name="Center">
                  <xsl:with-param name="input" select="."/>
                  <xsl:with-param name="n" select="16"/>
                </xsl:call-template>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise> <xsl:value-of select="string('                ')" /> </xsl:otherwise>
          </xsl:choose>

          <xsl:choose>
            <xsl:when test="count(@F109R) !=0">
              <xsl:for-each select="@F109R">
                <xsl:call-template name="Center">
                  <xsl:with-param name="input" select="."/>
                  <xsl:with-param name="n" select="12"/>
                </xsl:call-template>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise> <xsl:value-of select="string('            ')" /> </xsl:otherwise>
          </xsl:choose>

          <xsl:choose>
            <xsl:when test="count(@F110R) !=0">
              <xsl:for-each select="@F110R">
                <xsl:call-template name="Center">
                  <xsl:with-param name="input" select="."/>
                  <xsl:with-param name="n" select="12"/>
                </xsl:call-template>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise> <xsl:value-of select="string('            ')" /> </xsl:otherwise>
          </xsl:choose>

          <xsl:choose>
            <xsl:when test="count(@F111R) !=0">
              <xsl:for-each select="@F111R">
                <xsl:call-template name="Center">
                  <xsl:with-param name="input" select="."/>
                  <xsl:with-param name="n" select="18"/>
                </xsl:call-template>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise> <xsl:value-of select="string('                  ')" /> </xsl:otherwise>
          </xsl:choose>

          <xsl:choose>
            <xsl:when test="count(@F112R) !=0">
              <xsl:for-each select="@F112R">
                <xsl:call-template name="Center">
                  <xsl:with-param name="input" select="."/>
                  <xsl:with-param name="n" select="20"/>
                </xsl:call-template>
              </xsl:for-each>
            </xsl:when>
            <xsl:otherwise> <xsl:value-of select="string('                    ')" /> </xsl:otherwise>
          </xsl:choose>
          <xsl:text>&#13;&#10;</xsl:text>
        </xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="starts-with(local-name(.),'ED10')">
      <xsl:call-template name="PPS"><xsl:with-param name="EDName" select="local-name(.)"/></xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>
    <xsl:if test="starts-with(local-name(.),'ED111')">
      <xsl:call-template name="PPS"><xsl:with-param name="EDName" select="local-name(.)"/></xsl:call-template>
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:if>

  </xsl:template>
</xsl:stylesheet>
