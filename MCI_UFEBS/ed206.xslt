<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- XSLT ��� ������ ������� � ���������� AstraKBR � AstraKBR_GUI (����������,�������).
          20-05-2015 ������ ����� 2.6.8 �� 29-06-2015
          25-03-2015 ������ ����� 2.6.7 �� 30-03-2015
          07-12-2014 ������ ����� 2.6.5 �� 03-01-2015

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ed="urn:cbr-ru:ed:v2.0"
  xmlns:env="http://www.w3.org/2003/05/soap-envelope"  xmlns:props="urn:cbr-ru:msg:props:v1.3"
  xmlns:sen="urn:cbr-ru:dsig:env:v1.1"
  xmlns:dsig="urn:cbr-ru:dsig:v1.1"
  xmlns:xf="http://www.w3.org/2002/08/xquery-functions">
  <xsl:output method="text" media-type="text/plain" encoding="WINDOWS-1251" indent="yes"/>

  <!--  ED206  -->

  <xsl:template match="ed:ED206">
      <xsl:text>------------------------------------------------------------------------------------------------------------------</xsl:text>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:value-of select="concat(position()-$modif_pos,'.  ')"/>
      <xsl:if test="@DC = 1"><xsl:text>������������� ������/ED206/    </xsl:text></xsl:if>
      <xsl:if test="@DC = 3"><xsl:text>������������� ������ � �������/ED206/    </xsl:text></xsl:if>
      <xsl:if test="@DC = 2"><xsl:text>������������� �������/ED206/   </xsl:text></xsl:if>
      <xsl:value-of select="@Acc"/>
      <xsl:call-template name="PriznGr"/>

      <xsl:text>������� ������/�������     : </xsl:text><xsl:value-of select="@DC"/>
      <xsl:if test="@DC = 1"><xsl:text> (�����)&#13;&#10;</xsl:text></xsl:if>
      <xsl:if test="@DC = 2"><xsl:text> (������)&#13;&#10;</xsl:text></xsl:if>
      <xsl:if test="@DC = 3"><xsl:text> (����� � ������)&#13;&#10;</xsl:text></xsl:if>

      <xsl:text>���� ���������� ��������   : </xsl:text>
      <xsl:for-each select="@TransDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>����� ���������� ��������  : </xsl:text><xsl:value-of select="@TransTime"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:text>��� ����� ��������������   : </xsl:text><xsl:value-of select="@BICCorr"/>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:if test="@CorrAcc != ''">
        <xsl:text>����������������� ����     : </xsl:text><xsl:value-of select="@CorrAcc"/>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:text>����� ���                  : </xsl:text><xsl:for-each select="@Sum"><xsl:call-template name="summa"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:if test="@PaytKind != ''">
        <xsl:text>��� �������                : </xsl:text><xsl:value-of select="@PaytKind"/>
        <xsl:if test="@PaytKind = 2"><xsl:text> (������)</xsl:text></xsl:if>
        <xsl:if test="@PaytKind = 3"><xsl:text> (����������)</xsl:text></xsl:if>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
      <xsl:call-template name="SystemCode"/>
      <xsl:if test="@SessionID != '' and @SessionID != '0'">
          <xsl:text>����� ����� (������)       : </xsl:text>
          <xsl:value-of select="@SessionID"/> <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>

      <xsl:call-template name="ProcessingDetails"/>

      <xsl:text>&#13;&#10;</xsl:text>

      <xsl:text>��������� ���.������������   : </xsl:text><xsl:text>� </xsl:text>
      <xsl:value-of select="ed:AccDoc/@AccDocNo"/>
      <xsl:text>  ��  </xsl:text>
      <xsl:for-each select="ed:AccDoc/@AccDocDate"><xsl:call-template name="date"/></xsl:for-each>
      <xsl:text>&#13;&#10;</xsl:text>
      <xsl:if test="ed:EDRefID/@EDNo != ''">
          <xsl:apply-templates select="ed:EDRefID"/>
      </xsl:if>
  </xsl:template>

</xsl:stylesheet>